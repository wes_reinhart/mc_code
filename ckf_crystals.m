% close all
% clc

plot_gofr = false;
num_patches = 2; % should the xml have patches?
orientations = true; % should the config have orientations?

cell  = 'ct';
label = '0.50';
aniso_rho = 0; % stretch the box in x to reach this density

ncell = [8, 8, 8];

max_rnd = 0.00;

write_aniso_xml = false;
write_config = true;

write_xml = true;
write_xyz = false;

sigma = 1.000;
rho = str2double(label);

if( strcmp(cell,'fcc3') )
    % fcc
    c = 4;
    boxsize = [1 1 1];
    
    xCell = [0.25, 0.75, 0.75, 0.25];
    yCell = [0.25, 0.75, 0.25, 0.75];
    zCell = [0.25, 0.25, 0.75, 0.75];
    
    % 3 bonds per patch
    xOrient = sqrt(3)/3 * ones(size(xCell));
    yOrient = sqrt(3)/3 * ones(size(yCell));
    zOrient = sqrt(3)/3 * ones(size(zCell));
    
elseif( strcmp(cell,'fcc4') )
    % fcc
    c = 4;
    boxsize = [1 1 1];
    
    xCell = [0.25, 0.75, 0.75, 0.25];
    yCell = [0.25, 0.75, 0.25, 0.75];
    zCell = [0.25, 0.25, 0.75, 0.75];
    
    % 4 bonds per patch
    xOrient = 1 * ones(size(xCell));
    yOrient = 0 * ones(size(yCell));
    zOrient = 0 * ones(size(zCell));

elseif( strcmp(cell,'bct') )
    % bcc
    c = 2;
    boxsize = [1/2 1/2 sqrt(1/2)];

    xCell = [0.25, 0.75]*boxsize(1);
    yCell = [0.25, 0.75]*boxsize(2);
    zCell = [0.25, 0.75]*boxsize(3);
    
    xOrient = 0 * (size(xCell));
    yOrient = 0 * ones(size(yCell));
    zOrient = 1 * ones(size(zCell));

elseif( strcmp(cell,'bcc') )
    % bcc
    c = 2;
    boxsize = [1 1 1];

    xCell = [0.25, 0.75];
    yCell = [0.25, 0.75];
    zCell = [0.25, 0.75];
    
    xOrient = ones(size(xCell));
    yOrient = 0 * ones(size(yCell));
    zOrient = 0 * ones(size(zCell));
    
elseif( strcmp(cell,'sc') )
    % sc
    c = 1;
    boxsize = [1 1 1];
    
    xCell = 0.5;
    yCell = 0.5;
    zCell = 0.5;
    
    xOrient = 0;
    yOrient = 0;
    zOrient = 1;
    
elseif( strcmp(cell,'hcp3') )
    c = 4;
    boxsize = [1,sqrt(3),2*sqrt(2/3)];
    
    xCell = [1/2, 0, 0, 1/2];
    yCell = [0,sqrt(3)/2,1/(2*sqrt(3)),1/2*(sqrt(3)+1/sqrt(3))];
    zCell = [0,0,sqrt(2/3),sqrt(2/3)];
    
    % xOrient = [ 0.04188,-0.01706, 0.04730, 0.03240];
    % yOrient = [-0.94989, 0.94654, 0.95637, 0.94087];
    % zOrient = [-0.30977, 0.32212, 0.28828, 0.33721];
    
    % sheets? 2.5 bonds per patch
%     facing = [sqrt((1-cos(2*pi/5)^2)/4097), ...
%         64 * sqrt((1-cos(2*pi/5)^2)/4097), ...
%         cos(2*pi/5)];

    % 3 bonds per patch at sufficient X and delta
    facing = [0 0 1];

    facing = facing / sqrt(sum(facing.^2));
    xOrient = facing(1) * ones(size(xCell));
    yOrient = facing(2) * ones(size(yCell));
    zOrient = facing(3) * ones(size(zCell));
    
elseif( strcmp(cell,'hcp4') )
    c = 4;
    boxsize = [1,sqrt(3),2*sqrt(2/3)];
    
    xCell = [1/2, 0, 0, 1/2];
    yCell = [0,sqrt(3)/2,1/(2*sqrt(3)),1/2*(sqrt(3)+1/sqrt(3))];
    zCell = [0,0,sqrt(2/3),sqrt(2/3)];

    % this gives 4 bonds per patch at sufficient X
    facing = [0.0054, 0.8181, 0.5751];

    facing = facing / sqrt(sum(facing.^2));
    xOrient = facing(1) * ones(size(xCell));
    yOrient = facing(2) * ones(size(yCell));
    zOrient = facing(3) * ones(size(zCell));    
    
elseif( strcmp(cell,'hs') )
    c = 4;
    boxsize = [1,sqrt(3),2*sqrt(2/3)];
    
    xCell = [1/2, 0, 0, 1/2];
    yCell = [0,sqrt(3)/2,1/(2*sqrt(3)),1/2*(sqrt(3)+1/sqrt(3))];
    zCell = [0,0,sqrt(2/3),sqrt(2/3)];
    
    %{
        ri = [xCell(1),yCell(1),zCell(1)]/(rho/c*prod(boxsize))^(1/3);
        rj = [xCell(2),yCell(2),zCell(2)]/(rho/c*prod(boxsize))^(1/3);
        rij = sqrt(sum((ri-rj).^2));
        
        f = rij;
        boxsize = boxsize .* [f f 1.0];
        xCell = xCell * f;
        yCell = yCell * f;
    %}
    
    xOrient = 0 * ones(size(xCell));
    yOrient = 1 * ones(size(yCell));
    zOrient = 0 * ones(size(zCell));
    
elseif( strcmp(cell,'ht') )
    c = 16;
    boxsize = [2,2*sqrt(3),4*sqrt(2/3)];
    
    % positions
    % dense layer of A bilayer
    AIx = [1,1/2,3/2,0,1/2,3/2];
    AIy = [0,sqrt(3)/2,sqrt(3)/2,sqrt(3),3/2*sqrt(3),3/2*sqrt(3)];
    AIz = zeros(1,6);
    % sparse layer of A bilayer
    AIIx = [0,1];
    AIIy = [1/2*(sqrt(3)+1/sqrt(3)),1/2*(3*sqrt(3)+1/sqrt(3))];
    AIIz = zeros(1,2) + sqrt(2/3);
    % A and B dense layers (x,y) are the same for hexagonal tetrastack
    BIx = AIx;
    BIy = AIy;
    BIz = zeros(1,6) + 2*sqrt(2/3);
    % sparse layer of B bilayer
    BIIx = [1,0];
    BIIy = [1/2*(sqrt(3)-1/sqrt(3)),1/2*(3*sqrt(3)-1/sqrt(3))];
    BIIz = zeros(1,2) + 3*sqrt(2/3);
    % collate the different layers
    xCell = [AIx,AIIx,BIx,BIIx];
    yCell = [AIy,AIIy,BIy,BIIy];
    zCell = [AIz,AIIz,BIz,BIIz];
    
    % orientations
    % each of the sparse layers point in the z-direction
    % A sparse
    AIIx = [0,0];
    AIIy = [0,0];
    AIIz = [1,1];
    % B sparse
    BIIx = [0,0];
    BIIy = [0,0];
    BIIz = [1,1];
    % each of the dense layers point to the CoM of their tetrahedron
    % set up convenient naming system
    com   = [1/2,1/(2*sqrt(3)),1/4*sqrt(2/3)];
    north = (com - [1/2,1/2*sqrt(3),0]);
    dist = sqrt( sum( north.^2 ) );
    north = transpose( north/dist );
    west  = transpose( (com-[0,0,0])/dist );
    east  = transpose( (com-[1,0,0])/dist );
    % A dense
    AI = [north,east,west,north,west,east];
    % B dense
    BI = AI;
    BI(3,:) = -BI(3,:);
    % collate the different layers
    xOrient = [AI(1,:),AIIx,BI(1,:),BIIx];
    yOrient = [AI(2,:),AIIy,BI(2,:),BIIy];
    zOrient = [AI(3,:),AIIz,BI(3,:),BIIz];
    
elseif( strcmp(cell,'ct') )
    c = 24;
    boxsize = [2,2*sqrt(3),6*sqrt(2/3)];
    
    % positions
    xyz = [-0.750000000000000,1.29903810567666,1.83711730708738;-0.750000000000000,-1.58771324027147,-2.24536559755125;-0.750000000000000,-1.01036297108185,-1.42886901662352;-0.750000000000000,-0.433012701892220,-0.612372435695794;-0.750000000000000,0.144337567297406,0.204124145231932;-0.750000000000000,0.721687836487032,1.02062072615966;-0.250000000000000,0.433012701892219,1.83711730708738;-0.250000000000000,-1.29903810567666,1.83711730708738;-0.250000000000000,1.58771324027147,-1.42886901662352;-0.250000000000000,-0.144337567297407,-1.42886901662352;-0.250000000000000,-0.721687836487033,0.204124145231932;-0.250000000000000,1.01036297108185,0.204124145231932;0.250000000000000,-0.433012701892220,1.83711730708738;0.250000000000000,0.144337567297406,-2.24536559755125;0.250000000000000,0.721687836487032,-1.42886901662352;0.250000000000000,1.29903810567666,-0.612372435695794;0.250000000000000,-1.58771324027147,0.204124145231932;0.250000000000000,-1.01036297108185,1.02062072615966;0.750000000000000,0.433012701892219,1.83711730708738;0.750000000000000,-1.29903810567666,1.83711730708738;0.750000000000000,1.58771324027147,-1.42886901662352;0.750000000000000,-0.144337567297407,-1.42886901662352;0.750000000000000,-0.721687836487033,0.204124145231932;0.750000000000000,1.01036297108185,0.204124145231932];
    xCell = transpose(xyz(:,1));
    yCell = transpose(xyz(:,2));
    zCell = transpose(xyz(:,3));
    
    % orientations
    q = [0,-0.942809041582063,-0.333333333333334;0,0,1;0,0.942809041582063,0.333333333333334;0,0,1;0,-0.942809041582063,-0.333333333333334;0,0,1;-0.816496580927726,0.471404520791033,-0.333333333333333;0.816496580927726,0.471404520791033,-0.333333333333333;0.816496580927726,-0.471404520791031,0.333333333333334;0.816496580927726,0.471404520791031,-0.333333333333333;0.816496580927726,-0.471404520791031,0.333333333333334;0.816496580927726,0.471404520791031,-0.333333333333333;0,-0.942809041582063,-0.333333333333334;0,0,1;0,-0.942809041582063,-0.333333333333333;0,0,1;0,-0.942809041582063,-0.333333333333334;0,0,1;0.816496580927726,0.471404520791033,-0.333333333333333;-0.816496580927726,0.471404520791033,-0.333333333333333;-0.816496580927726,-0.471404520791031,0.333333333333334;-0.816496580927726,0.471404520791031,-0.333333333333333;-0.816496580927726,-0.471404520791031,0.333333333333334;-0.816496580927726,0.471404520791031,-0.333333333333333];
    xOrient = transpose(q(:,1));
    yOrient = transpose(q(:,2));
    zOrient = transpose(q(:,3));
    
elseif( strcmp(cell,'p') )
    c = 3;
    boxsize = [1 1 1]*sqrt(2);
    
    % positions
    xCell = [ 1/2 1 1]*boxsize(1);
    yCell = [ 0   0 1/2]*boxsize(2);
    zCell = [ 1/2 1 1/2]*boxsize(3);
    
    % orientations
    xOrient = [ 1  0  0];
    yOrient = [ 0  0  1];
    zOrient = [ 0  1  0];
    % xOrient = [ 1  1  1]/sqrt(3);
    % yOrient = [ 1  1  1]/sqrt(3);
    % zOrient = [ 1  1  1]/sqrt(3);
    
elseif( strcmp(cell,'fp') )
    c = 6;
    boxsize = [1 1 1]*(1+sqrt(2));
    
    % positions
    xCell = [ 1 -1  0  0  0  0]*sqrt(2)/2;
    yCell = [ 0  0  1 -1  0  0]*sqrt(2)/2;
    zCell = [ 0  0  0  0 -1  1]*sqrt(2)/2;
    
    % orientations
    xOrient = [ 1 -1  0  0  0  0];
    yOrient = [ 0  0  1 -1  0  0];
    zOrient = [ 0  0  0  0 -1  1];
    
else
    
    fprintf('Unrecognized unit cell.\n')
    return
    
end

% if( ~strcmp(cell,'p') )
% center the unit cell in the box
b = (boxsize - [max(xCell),max(yCell),max(zCell)]);
a = [min(xCell),min(yCell),min(zCell)];
shift = (b-a)/2;
xCell = xCell + shift(1);
yCell = yCell + shift(2);
zCell = zCell + shift(3);
% end

N = prod(ncell)*c;

% L = (N/rho)^(1/3) * boxsize; % box length in units of sigma
if( strcmp(cell,'hs') )
    a_factor = ( (N/rho) / prod(boxsize.*ncell) );
    a = [a_factor^(1/8) a_factor^(1/8) a_factor^(3/4)];
else
    a = ( (N/rho) / prod(boxsize.*ncell) )^(1/3) * ones(1,3);
end

if( aniso_rho > 0 && aniso_rho ~= rho )
    % anisotropic box for direct coexistence
    a_aniso = rho / aniso_rho;
    a(3) = a(3) * a_aniso;
    label = num2str(aniso_rho);
end

L = boxsize.*ncell.*a;

filename = ['ckf_N',num2str(N),'_',cell,'_',label];

deltag = 0.01;
nbins = ceil( (min(L)/2)/deltag );
g_hist = zeros(nbins,1);

r = zeros(N,3);
p = zeros(N,3);

n = 1;

for x = 0:ncell(1)-1
    for y = 0:ncell(2)-1
        for z = 0:ncell(3)-1
            for k = 1:c
                if( n <= N )
                    % positions
                    r(n,1) = (x*boxsize(1) + xCell(k))*a(1) - L(1)/2;
                    r(n,2) = (y*boxsize(2) + yCell(k))*a(2) - L(2)/2;
                    r(n,3) = (z*boxsize(3) + zCell(k))*a(3) - L(3)/2;
                    for q = 1:num_patches
                        % orientations
                        p(n,1) = xOrient(k);
                        p(n,2) = yOrient(k);
                        p(n,3) = zOrient(k);
                    end
                    n = n +1;
                end
            end
        end
    end
end

r = r + normrnd(0,max_rnd,size(r));

if( plot_gofr )
    rij2 = zeros(N,N);
    for i=1:N-1
        for j=i+1:N
            dx = r(i,1)-r(j,1);
            dx = dx - L(1)*round(dx/L(1));
            dy = r(i,2)-r(j,2);
            dy = dy - L(2)*round(dy/L(2));
            dz = r(i,3)-r(j,3);
            dz = dz - L(3)*round(dz/L(3));
            rij2 = dx*dx + dy*dy + dz*dz;
            rij = sqrt(rij2);
            if( rij < min(L)/2 )
                gid = ceil(rij/deltag);
                g_hist(gid) = g_hist(gid) + 2;
            end
        end
    end
    
    x = zeros(nbins,2);
    for i = 1:nbins
        rij = deltag*(i-0.5);
        vg = 4/3 * pi * ( (i+0.5)^3 - (i-0.5)^3 ) * deltag^3;
        g_i = g_hist(i) / ( N * rho * vg );
        x(i,:) = [rij, g_i];
    end
    
    peak_id = find(x(:,2));
    first_peak = x(peak_id(1),1);
    
    fprintf('first peak at %f\n',first_peak)
end

if( plot_gofr )
    figure;
    hold on
    plot(x(:,1),x(:,2),'k-')
    title(cell)
    %{
    figure;
    hold on
    plot3(r(:,1),r(:,2),r(:,3),'k.','markersize',4)
    box_x = ([0 1 1 1 1 1 1 0 0 0 0 0 1 1 0 0]-1/2)*L(1);
    box_y = ([0 0 0 0 1 1 1 1 1 1 0 0 0 1 1 0]-1/2)*L(2);
    box_z = ([1 1 0 1 1 0 1 1 0 1 1 0 0 0 0 0]-1/2)*L(3);
    plot3(box_x,box_y,box_z,'k-')
    axis([-1,1,-1,1,-1,1]*max(L/2))
    axis square
    %}
end

L = L * sigma;
r = r * sigma;

natom = N*(1+num_patches);

if( write_xml )
    % generate hoomd xml configuration
    fid = fopen([filename,'.xml'],'w');
    fprintf(fid,'<?xml version="1.0" encoding="UTF-8"?>\n');
    fprintf(fid,'<hoomd_xml version="1.5">\n');
    fprintf(fid,'<configuration time_step="0" dimensions="3" natoms="%d" >\n',natom);
    fprintf(fid,'<box lx="%f" ly="%f" lz="%f" xy="0" xz="0" yz="0"/>\n',L(1),L(2),L(3));
    fprintf(fid,'<position num="%d">\n',natom);
    for i = 1:N
        fprintf(fid,'%f %f %f\n',r(i,1),r(i,2),r(i,3));
        if( num_patches == 1 )
            fprintf(fid,'%f %f %f\n',...
                r(i,1)+p(i,1)*0.5, r(i,2)+p(i,2)*0.5, r(i,3)+p(i,3)*0.5);
        elseif( num_patches == 2 )
            fprintf(fid,'%f %f %f\n',...
                r(i,1)+p(i,1)*0.5, r(i,2)+p(i,2)*0.5, r(i,3)+p(i,3)*0.5);
            fprintf(fid,'%f %f %f\n',...
                r(i,1)-p(i,1)*0.5, r(i,2)-p(i,2)*0.5, r(i,3)-p(i,3)*0.5);
        elseif( num_patches == 4 )
            % write tetrahedral patch geometry
            q = zeros(4,3);
            q(1,:) = [1 0 -1/sqrt(2)]/sqrt(1.5);
            q(2,:) = [-1 0 -1/sqrt(2)]/sqrt(1.5);
            q(3,:) = [0 1 1/sqrt(2)]/sqrt(1.5);
            q(4,:) = [0 -1 1/sqrt(2)]/sqrt(1.5);
            % construct the rotation matrix
            th_rot = (-1)^mod(i,2)*pi/4;
            r_dir = [0 0 1];
            r_cross = [0,-r_dir(3),r_dir(2); r_dir(3),0,-r_dir(1); -r_dir(2),r_dir(1),0];
            rxr = zeros(3);
            for j = 1:3
                for k = 1:3
                    rxr(j,k) = r_dir(j)*r_dir(k);
                end
            end
            R = cos(th_rot)*eye(3) + sin(th_rot)*r_cross + (1-cos(th_rot))*rxr;
            for j = 1:4
                q(j,:) = q(j,:) * R;
                fprintf(fid,'%f %f %f\n',r(i,:) + q(j,:)*0.5);
            end
        elseif( num_patches == 6 )
            % write octahedral patch geometry
            q = zeros(6,3);
            q(1,:) = [ 0  0  1];
            q(2,:) = [ 1  0  0];
            q(3,:) = [-1  0  0];
            q(4,:) = [ 0  1  0];
            q(5,:) = [ 0 -1  0];
            q(6,:) = [ 0  0 -1];
            for j = 1:6
                fprintf(fid,'%f %f %f\n',r(i,:) + q(j,:)*0.5);
            end
        end
    end
    fprintf(fid,'</position>\n');
    fprintf(fid,'<type num="%d">\n',natom);
    for i = 1:N
        fprintf(fid,'C\n');
        for q = 1:num_patches
            fprintf(fid,'P\n');
        end
    end
    fprintf(fid,'</type>\n');
    fprintf(fid,'<diameter num="%d">\n',natom);
    for i = 1:N
        fprintf(fid,'1.0\n');
        for q = 1:num_patches
            fprintf(fid,'0.2\n');
        end
    end
    fprintf(fid,'</diameter>\n');
    fprintf(fid,'</configuration>\n');
    fprintf(fid,'</hoomd_xml>\n');
end

if( write_aniso_xml )
    % generate hoomd xml configuration
    fid = fopen([filename,'.xml'],'w');
    fprintf(fid,'<?xml version="1.0" encoding="UTF-8"?>\n');
    fprintf(fid,'<hoomd_xml version="1.5">\n');
    fprintf(fid,'<configuration time_step="0" dimensions="3" natoms="%d" >\n',N);
    fprintf(fid,'<box lx="%f" ly="%f" lz="%f" xy="0" xz="0" yz="0"/>\n',L(1),L(2),L(3));
    fprintf(fid,'<position num="%d">\n',N);
    for i = 1:N
        fprintf(fid,'%f %f %f\n',r(i,1),r(i,2),r(i,3));
    end
    fprintf(fid,'</position>\n');

    fprintf(fid,'<orientation num="%d">\n',N);
    up = [1, 0, 0];
    for i = 1:N  
        q = quaternion.rotateutov(up,p(i,:));
        a = q.e';
        fprintf(fid,'%f %f %f %f\n',a(1),a(2),a(3),a(4));
    end
    fprintf(fid,'</orientation>\n');
    
    fprintf(fid,'<type num="%d">\n',N);
    for i = 1:N
        fprintf(fid,'C\n');
    end
    fprintf(fid,'</type>\n');
    
    fprintf(fid,'<diameter num="%d">\n',N);
    for i = 1:N
        fprintf(fid,'1.0\n');
    end
    fprintf(fid,'</diameter>\n');
    
    fprintf(fid,'</configuration>\n');
    fprintf(fid,'</hoomd_xml>\n');
end

if( write_config )
    fid = fopen([filename,'.lattice'],'w');
    % number of particles and box size
    fprintf(fid,'%15d%15.10f%15.10f%15.10f\n',N,L);
    % positions
    for i = 1:length(r)
        fprintf(fid,'%15.10f%15.10f%15.10f\n',r(i,:));
    end
    if( orientations )
        % orientations
        for i = 1:length(p)
            if( num_patches == 1 )
                fprintf(fid,'%15.10f%15.10f%15.10f\n',p(i,:));
            elseif( num_patches == 2 )
                fprintf(fid,'%15.10f%15.10f%15.10f\n',p(i,:));
                fprintf(fid,'%15.10f%15.10f%15.10f\n',-p(i,:));
            elseif( num_patches == 4 )
                % write patch geometry
                q = zeros(4,3);
                q(1,:) = [1 0 -1/sqrt(2)]/sqrt(1.5);
                q(2,:) = [-1 0 -1/sqrt(2)]/sqrt(1.5);
                q(3,:) = [0 1 1/sqrt(2)]/sqrt(1.5);
                q(4,:) = [0 -1 1/sqrt(2)]/sqrt(1.5);
                % construct the rotation matrix
                th_rot = (-1)^mod(i,2)*pi/4;
                r_dir = [0 0 1];
                r_cross = [0,-r_dir(3),r_dir(2); r_dir(3),0,-r_dir(1); -r_dir(2),r_dir(1),0];
                rxr = zeros(3);
                for j = 1:3
                    for k = 1:3
                        rxr(j,k) = r_dir(j)*r_dir(k);
                    end
                end
                R = cos(th_rot)*eye(3) + sin(th_rot)*r_cross + (1-cos(th_rot))*rxr;
                for j = 1:4
                    q(j,:) = q(j,:) * R;
                    fprintf(fid,'%15.10f%15.10f%15.10f\n',q(j,:));
                end
            elseif( num_patches == 6 )
                % write octahedral patch geometry
                q = zeros(6,3);
                q(1,:) = [ 0  0  1];
                q(2,:) = [ 1  0  0];
                q(3,:) = [-1  0  0];
                q(4,:) = [ 0  1  0];
                q(5,:) = [ 0 -1  0];
                q(6,:) = [ 0  0 -1];
                % write patches to file
                for j = 1:6
                    fprintf(fid,'%15.10f%15.10f%15.10f\n',q(j,:));
                end
            end
        end
    end
    fclose(fid);
end

if( write_xyz )
    fid = fopen([filename,'.xyz'],'w');
    % number of particles
    fprintf(fid,'%10d\n',N);
    % each particle has a weird format
    for i = 1:length(r)
        fprintf(fid,'Na%20.15f%20.15f%20.15f\n',r(i,:));
    end
    fclose(fid);
end