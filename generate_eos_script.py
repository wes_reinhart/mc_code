from __future__ import print_function
import sys
import os
import shutil
import numpy as np
import commands

def log_glq(N):
    filename = 'N'+str(N)+'.glq'
    [x,w] = glq = np.polynomial.legendre.leggauss(int(N))

    # c = float(1e-6)
    c = float(1e-3)
    y = ( x + 1 ) / 2.0 * ( np.log(c+1.0) - np.log(c) ) + np.log(c)
    z = 1 - ( np.exp(y) - c )
    x = ( z * 2.0 ) - 1.0

    # this is the exponential part AND the change of interval part
    w2 = ( np.log(c+1.0) - np.log(c) ) / 2.0 * np.exp(y)

    fid = open(filename,'w+')
    print('%           weight                    point',file=fid)
    for i in range(int(N)):
        print('%.16f     %.16f     %.16f' % (w[i],x[i],w2[i]),file=fid)
    fid.close()
    print('generation of %s complete' % filename)

    # only need abscissa during this script
    #      weights are saved to file
    return x

#==================================================
# set parameters for the run
#--------------------------------------------------
run_name   = "eos_fluid_d1.06_a0.45_T0.15"

# read from file
# configfile = "/home/wfr/Data/mc_ckf/relax_hcp-0/relax.config.xml"
# rescale_mode = "xyz"

# generate randomly (fluid)
configfile = False
Lx = Ly = Lz = 8.6
rescale_mode = "all"

# system parameters
N = 192;
kT = 0.15;

# ckf potential parameters
rcut  = 1.6;
Md    = 1.8341;
Mr    = 0.0302;
req   = 1.0043;
omega = 50;
alpha = 0.45;

# quadrature points
P = np.logspace(0.001,0.5,41)

# run parameters
equil  = 1e5;
sweeps = 1e6;
reset  = 1e2;
adjust = 10;
logs = 1e3;
snapshots = 1e2;

#==================================================
# create a run directory
#--------------------------------------------------
data_dir = "/home/wfr/Data/mc_ckf/"

# assign an unused label
label = -1
for x in next(os.walk(data_dir))[1]:
    if x.find(run_name+"-") == 0:
        dash = x.rfind('-') + 1
        n = int( x[dash:len(x)] )
        if n > label:
            label = n
            
print( "Last run was %d" % label )

# make the directory
run_dir = data_dir + run_name + "-" + str(label+1) + "/"
os.mkdir(run_dir)

# copy the job script there
shutil.copy2("run_mc.sh",run_dir)

# copy this script there
shutil.copy2("generate_eos_script.py",run_dir)

# copy the analysis scripts there
shutil.copy2("analyze_eos.py",run_dir)
shutil.copy2("analyze_eos.sh",run_dir)

# copy the initial configuration there
if configfile:
    shutil.copy2(configfile,run_dir+"reference.config.xml")

# move to the run directory so slurm.out is written there
os.chdir(run_dir)

# save the pressures to file
np.savetxt("eos_P.dat",P,fmt='%e')

slurm_job_ids = []

#==================================================
# eos runs
#--------------------------------------------------
for i in range(len(P)):
    # open script
    scriptname = "input_%d.mc" % i;
    script = open(run_dir+scriptname,'w+');
    # initial setup
    if configfile:
        print("config.read(configfile=%s,npatch=2)" % configfile,file=script);
    else:
        print("config.generate(N=%d,L=(%f,%f,%f),npatch=2,grid=0.1)" % (N,Lx,Ly,Lz),file=script);
    print("mc.npt(kT=%f,P=%f)" % ( kT, P[i] ),file=script);
    # equilibrate with all potentials now
    print("potential.pair_ckf(rcut=%f,Md=%f,Mr=%f,req=%f,omega=%f,alpha=%f)" % (rcut,Md,Mr,req,omega,alpha),file=script);
    # moves might need to be very small
    print("move.displace(freq=%d,mag=1e-1,max=1e0,min=1e-3)" % N,file=script);
    print("move.rotate(freq=%d,mag=0.33,max=1.57,min=1e-3)" % N,file=script);
    print("move.rescale(freq=2,mag=0.5,max=10.0,min=1e-3,dims=%s)" % rescale_mode,file=script);
    # equilibrate moves (and configuration)
    print("mc.run(sweeps=%d,reset=%d,adjust=%d)" % (equil, reset, adjust),file=script);
    # log all the quantities
    print("mc.log(prefix=eos_%d,period=%d,potential=True,pressure=True,lx=True,ly=True,lz=True)" % (i,sweeps/logs) ,file=script);
    # set up some snapshots (for diagnostics)
    print("mc.snapshot(prefix=eos_%d.config,period=%d,overwrite=True)" % (i,sweeps/snapshots) ,file=script);
    # production run
    print("mc.run(sweeps=%d,reset=%d)" % (sweeps, reset),file=script);
    script.close()

    #==================================================
    # queue the run
    #--------------------------------------------------
    # sbatch the job script (must be bash)
    this_proc_name = run_name + "-" + str(label+1) + ":" + str(i)
    cmd = "sbatch --job-name=%s run_mc.sh %s %s %s" % (this_proc_name,scriptname,run_name,run_dir)
    # print("submitting eos job with command: %s" % cmd)
    status, output = commands.getstatusoutput(cmd)
    if status != 0:
        print("Error! %s" % output)
    jobnum = output.replace("Submitted batch job ","")
    slurm_job_ids.append(jobnum)

#==================================================
# calculate necessary values
#--------------------------------------------------
print("list of all jobs:")
print(slurm_job_ids)

cmd = "sbatch --depend=afterany"
for job in slurm_job_ids:
    cmd += ":" + job
cmd += " analyze_eos.sh"
# print("submitting analysis job with command: %s" % cmd)
status, output = commands.getstatusoutput(cmd)
if status != 0:
    print("Error! %s" % output)
