from __future__ import print_function
import numpy as np
import os
import sys
import glob
import commands

def log_glq(N):
    filename = 'N'+str(N)+'.glq'
    [x,w] = glq = np.polynomial.legendre.leggauss(int(N))

    # c = float(1e-6)
    c = float(1e-3)
    y = ( x + 1 ) / 2.0 * ( np.log(c+1.0) - np.log(c) ) + np.log(c)
    z = 1 - ( np.exp(y) - c )
    x = ( z * 2.0 ) - 1.0

    # this is the exponential part AND the change of interval part
    w2 = ( np.log(c+1.0) - np.log(c) ) / 2.0 * np.exp(y)

    # only need abscissa during this script
    #      weights are saved to file
    return x

#==================================================
# find parameters for the run
#--------------------------------------------------
logdata = np.loadtxt("dA1_0_prod.log",comments='%')
logs    = logdata.shape[0]

num_A1 = len(glob.glob('dA1_*.config.xml'))
num_A2 = len(glob.glob('dA2_*.config.xml'))

cmd = "grep '^kT = ' generate_ti_script.py"
status, output = commands.getstatusoutput(cmd)
kT = float( output.replace("kT","").replace("=","").replace(";","") )

cmd = "grep '^lambda_tr = ' generate_ti_script.py"
status, output = commands.getstatusoutput(cmd)
lambda_tr = float( output.replace("lambda_tr","").replace("=","").replace("float(","").replace(")","") )

cmd = "grep '^lambda_or = ' generate_ti_script.py"
status, output = commands.getstatusoutput(cmd)
lambda_or = float( output.replace("lambda_or","").replace("=","").replace("float(","").replace(")","") )

#==================================================
# preprocess some parameters
#--------------------------------------------------
nblocks = 10
equil_cutoff = 0

beta = 1.0/kT
blocksize = int( logs / nblocks )
U_solid = np.zeros((logs,num_A1))
tr_disp = np.zeros((logs,num_A2))
or_disp = np.zeros((logs,num_A2))
sol_arr = np.zeros((nblocks+1,num_A1)) * np.nan # need an extra row for U_lattice
tr_arr = np.zeros((nblocks,num_A2)) * np.nan
or_arr = np.zeros((nblocks,num_A2)) * np.nan

# compute the abscissa and weights
x = log_glq(num_A2)

#==================================================
# process dA1
#--------------------------------------------------
for i in range(num_A1):
    logdata = np.loadtxt("dA1_"+str(i)+"_equil.log",comments='%')
    U_lattice = logdata[0,2]    
    # load ckf potential
    logdata = np.loadtxt("dA1_"+str(i)+"_prod.log",comments='%')
    U_solid[:,i] = logdata[:,2]
    sol_arr[0,0] = U_lattice
    # calculate block averages
    for j in range(nblocks):
        lo = equil_cutoff + j*blocksize
        hi = equil_cutoff + (j+1)*blocksize
        sol_arr[j+1,i] = np.mean(np.exp(-beta*(U_solid[lo:hi,i]-U_lattice)))
# write to file
np.savetxt("sol.dat",sol_arr,fmt='%25.15e',header="% block averages <exp[-B*(U_solid-U_lattice)]>")

#==================================================
# process dA2
#--------------------------------------------------
for i in range(num_A2):
    local_lambda = (x[i] + 1.0) * 0.50
    local_lambda_tr = lambda_tr * (1 - local_lambda)
    local_lambda_or = lambda_or * (1 - local_lambda)
    # load ckf potential
    logdata = np.loadtxt("dA2_"+str(i)+".log",comments='%')
    tr_disp[:,i] = logdata[:,3] / local_lambda_tr
    or_disp[:,i] = logdata[:,4] / local_lambda_or
    # calculate block averages
    for j in range(nblocks):
        lo = equil_cutoff + j*blocksize
        hi = equil_cutoff + (j+1)*blocksize
        tr_arr[j,i] = np.mean(tr_disp[lo:hi,i])
        or_arr[j,i] = np.mean(or_disp[lo:hi,i])

# write to file
np.savetxt("tr.dat",tr_arr,fmt='%25.15e',header="% block averages <restraint_position_energy / lambda_tr>")
np.savetxt("or.dat",or_arr,fmt='%25.15e',header="% block averages <restraint_orientation_energy / lambda_or>")
