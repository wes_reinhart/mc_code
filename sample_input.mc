# generate a configuration
config.generate(N=192,L=(10,10,10),npatch=2,grid=0.1,diameter=(1.0,0.0))

# set up displacements and rotations
move.displace(freq=192,mag=0.1,max=1.0,min=0.001)
move.rotate(freq=192,mag=1.57,max=3.14,min=0.001)

# set up rescale moves
move.rescale(freq=4,dims=all,mag=1.0)

# set up pair potential(s)
potential.pair_wca(sigma=0.95)
potential.pair_kf(delta=0.06,alpha=0.68)

# set up biasing potential
potential.bias(v_min=100,v_max=2000,n_bins=191)

# choose ensemble
mc.npt(kT=0.25,P=0.50)

# set up logger
mc.log(prefix=run,period=1e2,potential=True,volume=True)

# save result
mc.snapshot(prefix=snapshot,period=1e3,overwrite=False)
mc.snapshot(prefix=restart,period=1e2,overwrite=True)

# equilibration run
mc.run(sweeps=1e4,reset=1e3,adjust=1e2)
