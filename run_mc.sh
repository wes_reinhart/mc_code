#!/bin/bash
#SBATCH -N 1
#SBATCH --ntasks-per-node=1
#SBATCH -t 24:00:00
#SBATCH -p cpu

# print diagnostic information
host=`hostname`
echo "running job ${run_name} on node ${host} (job ${SLURM_JOB_ID}):"

# get run name and directory from command line
job_script=$1
run_name=$2
run_dir=$3
this_location=`pwd`

# cp stuff from the run directory
#   to the temporary job directory
# cp ${this_location}/* /data/${SLURM_JOB_ID}/
echo ""
echo "rsyncing from /home/ filesystem to temporary /data/ directory"
rsync -az ${this_location}/* /data/${SLURM_JOB_ID}/ --exclude="slurm-*.out"
cd /data/${SLURM_JOB_ID}/

echo ""
pwd

# run the script from inside the run directory
echo ""
echo "running MC script: /home/wfr/mc_code/bin/testmc ${job_script}"
/home/wfr/mc_code/bin/testmc ${job_script}

# cp /data/${SLURM_JOB_ID}/* ${this_location}/
echo ""
echo "rsyncing from temporary /data/ directory to /home/ filesystem"
rsync -az /data/${SLURM_JOB_ID}/* ${this_location}/ --exclude="slurm-*.out"

# create an archive
echo ""
echo "creating archive..."
data_dir='/home/wfr/Data/'
backup_dir='/home/wfr/backups/'
main_dir='/home/wfr/Data/mc_ckf/'
upper_dir=`echo "${main_dir}" | sed s:${data_dir}::g`
upper_dir=`echo "${upper_dir}" | sed s:/::g`
lower_dir=`echo "${run_dir}" | sed s:${main_dir}::g`
lower_dir=`echo "${lower_dir}" | sed s:/::g`
echo ">> cd ${data_dir}/${upper_dir}/"
echo ">> tar -czvf ${lower_dir}.tar.gz ${lower_dir}"
echo ">> mv ${lower_dir}.tar.gz ${backup_dir}/${upper_dir}/"
cd ${data_dir}/${upper_dir}/
tar -czf ${lower_dir}.tar.gz ${lower_dir}
mv ${lower_dir}.tar.gz ${backup_dir}/${upper_dir}/

echo ""
echo "cleaning up temporary /data/ directory..."
rm /data/${SLURM_JOB_ID}/*
