# read a configuration
config.read(configfile=/Users/wfr/code/mc_code/bin/swaps.config.xml,npatch=2)

# set up diameter swaps
move.swap_diameters(freq=192)

# set up displacements and rotations
# move.displace(freq=1,mag=0.1,max=1.0,min=0.001)
# move.rotate(freq=1,mag=1.57,max=3.14,min=0.001)

# set up rescale moves
move.rescale(freq=1,dims=all,mag=1.0)

# set up pair potential(s)
potential.pair_wca(sigma=0.95)
potential.pair_kf(delta=0.10,alpha=0.67)

# choose ensemble
mc.npt(kT=0.15,P=0.15)
# mc.nvt(kT=0.15)

# set up logger
mc.log(prefix=swaps,period=1e2,potential=True,volume=True)

# save result
mc.snapshot(prefix=swaps,period=1e2,overwrite=False)
mc.snapshot(prefix=swaps.config,period=1e2,overwrite=True)

# production run
mc.run(sweeps=1e4,reset=1e2,adjust=1e1)
