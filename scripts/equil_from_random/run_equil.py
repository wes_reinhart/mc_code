from __future__ import print_function
import numpy as np
import commands
import os
import shutil
import glob

cmd = "./testmc generate_config.mc"
status, output = commands.getstatusoutput(cmd)
shutil.copy( "swaps.config.xml", "orig.config.xml" )

trials = 0
energy = 1
while energy > 0:
    # generate the config
    cmd = "./testmc swaps.mc"
    status, output = commands.getstatusoutput(cmd)
    # compute hard sphere energy
    cmd = "./testmc compute_hs_energy.mc"
    status, output = commands.getstatusoutput(cmd)
    # get the energy from the log
    log = np.loadtxt("hs_energy.log",comments="%")
    energy = log[1]
    print("energy = %e" % energy)
    # create a folder for this run
    try:
        os.mkdir( "trial_%d/" % trials )
    except:
        files = glob.glob( "trial_%d/*" % trials )
        [os.remove( f ) for f in files]
    # move the swaps data there
    files = glob.glob( "swaps.*[x,l]*" )
    [shutil.move( f, "./trial_%d/" % trials ) for f in files]
    # copy the swaps.config file back
    shutil.copy2( "trial_%d/swaps.config.xml" % trials, "./" ) 
    # move the hs data there
    shutil.move( "hs_energy.log", "trial_%d/" % trials )
    # increment count
    trials += 1

# run with real KF potential (hard spheres)
cmd = "./testmc equil_hs.mc"
status, output = commands.getstatusoutput(cmd)

# get the energy from the log
log = np.loadtxt("hs.log",comments="%")
energy = log[-1,1]
print("final energy = %e" % energy)
