# read a configuration
config.read(configfile=/Users/wfr/code/mc_code/bin/swaps.config.xml,npatch=2)

# set up pair potential(s)
potential.pair_hs()
potential.pair_kf(delta=0.06,alpha=0.67)

# choose ensemble
mc.nvt(kT=0.15)

# set up trial move(s)
move.displace(freq=192,mag=0.1,max=1.0,min=0.001)
move.rotate(freq=192,mag=1.57,max=3.14,min=0.001)
# move.swap_diameters(freq=10)

# set up logger and snapshots
mc.log(prefix=hs,period=1e2,potential=True,lx=True,ly=True,lz=True)
mc.snapshot(prefix=hs,period=1e3,overwrite=False)
mc.snapshot(prefix=hs.config,period=1e3,overwrite=True)

# equilibrate
mc.run(sweeps=1e4,reset=1e2,adjust=1e1)
