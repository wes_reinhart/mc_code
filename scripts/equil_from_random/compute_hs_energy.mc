# read a configuration
config.read(configfile=/Users/wfr/code/mc_code/bin/swaps.config.xml,npatch=2)

# set up diameter swaps
move.swap_diameters(freq=1)

# set up pair potential(s)
potential.pair_hs()
potential.pair_kf(delta=0.06,alpha=0.67)

# choose ensemble
mc.nvt(kT=0.15)

# set up logger
mc.log(prefix=hs_energy,period=1,potential=True,volume=True)

# compute the energy without running
mc.run(sweeps=0)
