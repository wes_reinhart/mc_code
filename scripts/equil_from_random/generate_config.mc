# read a lattice configuration
config.read(configfile=/Users/wfr/code/mc_code/bin/ckf_N192_ct_0.65.xml,diameter=(1.0,0.01),npatch=2)

# write the configuration with random diameters
mc.snapshot(prefix=swaps.config,overwrite=True)
