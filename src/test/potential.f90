module potential_class
  implicit none

  type, public :: potential
   contains
     procedure :: pair => evaluate
  end type potential
  
contains

  function evaluate(self) result()
    class(potential), intent(in) :: self
    
  end function evaluate
  
end module potential_class
