module class_Cylinder
  use class_Circle
  implicit none

  type, extends(Circle), public :: Cylinder
     type (Circle), private :: end_circ
     real :: height
   contains

     interface construct
        module procedure cylinder_construct
     end interface construct
     
     procedure, public :: volume => cylinder_volume
     procedure, public :: print => cylinder_print
  end type Cylinder
  
contains

  subroutine cylinder_construct(self)
    type (Cylinder), intent(out) :: self
    type (Circle) :: end_circ
    construct(end_circ)
    self%end_circ = end_circ
  end subroutine cylinder_construct
  
  function cylinder_volume(self) result(volume)
    class(Cylinder), intent(in) :: self
    real :: area, volume
    area = self%end_circ%area()
    volume = self%height * area
  end function cylinder_volume

  subroutine cylinder_print(self)
    class(Cylinder), intent(in) :: self
    real :: volume
    volume = self%volume()
    print *, 'Cylinder: r = ', self%radius,&
             ', height = ', self%height,&
             ', volume = ', volume
  end subroutine cylinder_print
  
end module class_Cylinder
