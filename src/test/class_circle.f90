module class_Circle
  implicit none
  private
  real :: pi = 3.1415926535897931d0

  type, public :: Circle
     real :: radius
   contains
     
     interface construct
        module procedure circle_construct
     end interface construct
     
     procedure, public :: area => circle_area
     procedure, public :: print => circle_print

  end type Circle

  ! type, extends(Circle), public :: Cylinder
  !    real :: height
  !  contains
  !    procedure :: volume => cylinder_volume
  !    procedure :: print => cylinder_print
  ! end type Cylinder
  
contains

  subroutine circle_construct(self)
    type (Circle), intent(out) :: self
  end subroutine circle_construct
  
  function circle_area(self) result(area)
    class(Circle), intent(in) :: self
    real :: area
    area = pi * self%radius**2
  end function circle_area

  subroutine circle_print(self)
    class(Circle), intent(in) :: self
    real :: area
    area = self%area()
    print *, 'Circle: r = ', self%radius, ' area = ', area
  end subroutine circle_print
  
  ! function cylinder_volume(self) result(volume)
  !   class(Cylinder), intent(in) :: self
  !   real :: volume
  !   volume = self%height * circle_area(self)
  ! end function cylinder_volume

  ! subroutine cylinder_print(self)
  !   class(Cylinder), intent(in) :: self
  !   real :: volume
  !   volume = self%volume()
  !   print *, 'Cylinder: r = ', self%radius,&
  !        ', height = ', self%height,&
  !        ', volume = ', volume
  ! end subroutine cylinder_print

end module class_Circle
