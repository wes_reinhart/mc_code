program main
  use globals
  use datatypes
  use interpreter_util
  use interpreter
  use util_time
  use util_rand
  implicit none
  integer, parameter :: fid = 99
  integer :: ierr
  type (context_struct) :: context
  character(len=1000) :: fline

  ! initialize rng before any commands are run
  call initialize_rng()

  ! read name of input file
  call getarg(1,fline)
  
  ! open the input file
  open(unit=fid,file=trim(fline),action="read",status="unknown",iostat=ierr)

  if( ierr .ne. 0 ) then
     write(*,'("Error: could not read specified input file: ",A)') trim(fline)
     stop
  end if
  
  ! feed each line to the interpreter
  read(fid,'(A)',iostat=ierr) fline
  line_number = 1
  do while( ierr .eq. 0 )
     call interpret_generic_command(context,fline)
     read(fid,'(A)',iostat=ierr) fline
     line_number = line_number + 1
  end do
  
  close(fid)
  
end program main
