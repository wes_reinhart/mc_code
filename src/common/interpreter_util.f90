module interpreter_util
  use globals
  implicit none

contains

  !========================================================!
  !     return an error if the quantity is nonpositive     !
  !========================================================!
  subroutine generic_gt_one_error(key,val)
    implicit none
    ! external variables
    character(len=*), intent(in) :: key
    real(P), intent(in) :: val
    if( val .gt. 1.0_P ) then
       write(*,'("Error (",I0,"): value of """,A,""" must not exceed one")') &
               line_number, trim(key)
    end if
  end subroutine generic_gt_one_error
  
  !========================================================!
  !     return an error if the quantity is nonpositive     !
  !========================================================!
  subroutine generic_nonpositive_error(key,val)
    implicit none
    ! external variables
    character(len=*), intent(in) :: key
    real(P), intent(in) :: val
    if( val .le. 0.0_P ) then
       write(*,'("Error (",I0,"): value of """,A,""" must be greater than zero")') &
               line_number, trim(key)
    end if
  end subroutine generic_nonpositive_error

  !=====================================================!
  !     return an error if the quantity is negative     !
  !=====================================================!
  subroutine generic_negative_error(key,val)
    implicit none
    ! external variables
    character(len=*), intent(in) :: key
    real(P), intent(in) :: val
    if( val .lt. 0.0_P ) then
       write(*,'("Error (",I0,"): value of """,A,""" must be non-negative")') &
               line_number, trim(key)
    end if
  end subroutine generic_negative_error
  
  !=====================================================!
  !     interpret a REAL3 parameters via key search     !
  !=====================================================!
  function interpret_real3(key,fline,required,default)
    implicit none
    real(P), dimension(3) :: interpret_real3
    ! external variables
    character(len=*), intent(in) :: key
    character(len=*), intent(in) :: fline
    logical, intent(in) :: required
    real(P), intent(in) :: default
    ! internal variables
    character(len=100) :: val_str
    character(len=:), allocatable :: key_equals
    integer :: ierr, s, e
    real(P) :: val_real

    allocate( character(len=len(key)+2) :: key_equals )
    write(key_equals,'(A,"=(")') trim(key)

    ! extract FIRST value from fline
    s = index(fline,key_equals) + len(key_equals)
    e = scan(fline(s:),",") - 2 + s
    val_str = trim(fline(s:e))
    read(val_str,*,iostat=ierr) val_real
    if( ierr .eq. 0 ) then
       interpret_real3(1) = val_real
    else
       if( required ) then
          write(*,'("Error (",I0,"): required value """,A,""" should be 3 elements long")') &
               line_number, trim(key)
          stop
       else
          interpret_real3(1) = default
       end if
    end if

    ! extract SECOND value from fline
    s = e + 2
    e = scan(fline(s:),",") - 2 + s
    val_str = trim(fline(s:e))
    read(val_str,*,iostat=ierr) val_real
    if( ierr .eq. 0 ) then
       interpret_real3(2) = val_real
    else
       if( required ) then
          write(*,'("Error (",I0,"): required value """,A,""" should be 3 elements long")') &
               line_number, trim(key)
          stop
       else
          interpret_real3(2) = default
       end if
    end if

    ! extract THIRD value from fline
    s = e + 2
    e = scan(fline(s:),")") - 2 + s
    val_str = trim(fline(s:e))
    read(val_str,*,iostat=ierr) val_real
    if( ierr .eq. 0 ) then
       interpret_real3(3) = val_real
    else
       if( required ) then
          write(*,'("Error (",I0,"): required value """,A,""" should be 3 elements long")') &
               line_number, trim(key)
          stop
       else
          interpret_real3(3) = default
       end if
    end if
    
    write(*,'(A," = ",3F10.5)') trim(key), interpret_real3
    
  end function interpret_real3

  !=====================================================!
  !     interpret a REAL2 parameters via key search     !
  !=====================================================!
  function interpret_real2(key,fline,required,default)
    implicit none
    real(P), dimension(2) :: interpret_real2
    ! external variables
    character(len=*), intent(in) :: key
    character(len=*), intent(in) :: fline
    logical, intent(in) :: required
    real(P), dimension(2), intent(in) :: default
    ! internal variables
    character(len=100) :: val_str
    character(len=:), allocatable :: key_equals
    integer :: ierr, s, e
    real(P) :: val_real

    allocate( character(len=len(key)+2) :: key_equals )
    write(key_equals,'(A,"=(")') trim(key)

    ! extract FIRST value from fline
    s = index(fline,key_equals) + len(key_equals)
    e = scan(fline(s:),",") - 2 + s
    val_str = trim(fline(s:e))
    read(val_str,*,iostat=ierr) val_real
    if( ierr .eq. 0 ) then
       interpret_real2(1) = val_real
    else
       if( required ) then
          write(*,'("Error (",I0,"): required value """,A,""" not found")') &
               line_number, trim(key)
          stop
       else
          interpret_real2(1) = default(1)
       end if
    end if

    ! extract SECOND value from fline
    s = e + 2
    e = scan(fline(s:),")") - 2 + s
    val_str = trim(fline(s:e))
    read(val_str,*,iostat=ierr) val_real
    if( ierr .eq. 0 ) then
       interpret_real2(2) = val_real
    else
       if( required ) then
          write(*,'("Error (",I0,"): required value """,A,""" should be 3 elements long")') &
               line_number, trim(key)
          stop
       else
          interpret_real2(2) = default(2)
       end if
    end if
    
    write(*,'(A," = ",2F10.5)') trim(key), interpret_real2
    
  end function interpret_real2
  
  !====================================================!
  !     interpret a REAL parameters via key search     !
  !====================================================!
  real(P) function interpret_real(key,fline,required,default)
    implicit none
    ! external variables
    character(len=*), intent(in) :: key
    character(len=*), intent(in) :: fline
    logical, intent(in) :: required
    real(P), intent(in) :: default
    ! internal variables
    character(len=100) :: val_str
    character(len=:), allocatable :: key_equals
    integer :: ierr, s, e
    real(P) :: val_real

    allocate( character(len=len(key)+1) :: key_equals )
    write(key_equals,'(A,"=")') trim(key)

    ! extract value from fline
    s = index(fline,key_equals) + len(key_equals)
    e = scan(fline(s:),",)") - 2 + s
    val_str = trim(fline(s:e))
    read(val_str,*,iostat=ierr) val_real
    if( ierr .eq. 0 ) then
       interpret_real = val_real
    else
       if( required ) then
          write(*,'("Error (",I0,"): required value """,A,""" not found")') &
               line_number, trim(key)
          stop
       else
          interpret_real = default
       end if
    end if
    
  end function interpret_real

  !================================================!
  !     wrap the REAL version to return a REAL     !
  !================================================!
  real(P) function interpret_generic_real(key,fline,required,default)
    implicit none
    ! external variables
    character(len=*), intent(in) :: key
    character(len=*), intent(in) :: fline
    logical, intent(in) :: required
    real(P), intent(in) :: default
    ! dummy variables to pass to interpret_real
    character(len=len(key))   :: d_key
    character(len=len(fline)) :: d_fline
    logical :: d_required
    real(P) :: d_default
    ! copy input to dummy variables
    d_key = key
    d_fline = fline
    d_required = required
    d_default = default
    interpret_generic_real = interpret_real(trim(d_key),trim(d_fline),d_required,d_default)
    write(*,'(A," = ",F10.5)') trim(key), interpret_generic_real
  end function interpret_generic_real

  !====================================================!
  !     wrap the REAL version to return an INTEGER     !
  !====================================================!
  integer function interpret_generic_integer(key,fline,required,default)
    implicit none
    ! external variables
    character(len=*), intent(in) :: key
    character(len=*), intent(in) :: fline
    logical, intent(in) :: required
    integer, intent(in) :: default
    ! dummy variables to pass to interpret_real
    character(len=len(key))   :: d_key
    character(len=len(fline)) :: d_fline
    logical :: d_required
    real(P) :: d_default
    ! copy input to dummy variables
    d_key = key
    d_fline = fline
    d_required = required
    d_default = real(default,P)
    interpret_generic_integer = int( interpret_real(trim(d_key),trim(d_fline),d_required,d_default) )
    write(*,'(A," = ",I0)') trim(key), interpret_generic_integer
  end function interpret_generic_integer

  !========================================================!
  !     interpret a CHARACTER parameter via key search     !
  !========================================================!
  function interpret_generic_character(key,fline,required,default)
    implicit none
    character(len=:), allocatable :: interpret_generic_character
    ! external variables
    character(len=*), intent(in) :: key
    character(len=*), intent(in) :: fline
    logical,          intent(in) :: required
    character(len=*), intent(in) :: default
    ! internal variables
    character(len=1000) :: val_str
    character(len=:), allocatable :: key_equals
    integer :: s, e

    allocate( character(len=len(key)+1) :: key_equals )
    write(key_equals,'(A,"=")') trim(key)

    ! extract value from fline
    s = index(fline,key_equals) + len(key_equals)
    if( s .gt. len(key_equals) ) then
       e = scan(fline(s:),",)") - 2 + s
       val_str = trim(fline(s:e))
       if( len(trim(val_str)) .gt. 0 ) then
          allocate( character(len=len(trim(val_str))) :: interpret_generic_character )
          interpret_generic_character = trim(val_str)
       else
          write(*,'("Error (",I0,"): invalid value """,A,""" supplied for """,A,""" (must be CHARACTER)")') &
               line_number, trim(val_str), trim(key)
          stop
       end if
    else
       if( required ) then
          write(*,'("Error (",I0,"): required parameter """,A,""" not found")') &
               line_number, trim(key)
          stop
       else
          allocate( character(len=len(trim(default))) :: interpret_generic_character )
          interpret_generic_character = trim(default)
       end if
    end if

    write(*,'(A," = ",A)') trim(key), interpret_generic_character
  end function interpret_generic_character

  !======================================================!
  !     interpret a LOGICAL parameter via key search     !
  !======================================================!
  logical function interpret_generic_logical(key,fline,required,default)
    implicit none
    ! external variables
    character(len=*), intent(in) :: key
    character(len=*), intent(in) :: fline
    logical,          intent(in) :: required
    logical,          intent(in) :: default
    ! internal variables
    character(len=1000) :: val_str
    character(len=:), allocatable :: key_equals
    integer :: s, e

    allocate( character(len=len(key)+1) :: key_equals )
    write(key_equals,'(A,"=")') trim(key)

    ! extract value from fline
    s = index(fline,key_equals) + len(key_equals)
    if( s .gt. len(key_equals) ) then
       e = scan(fline(s:),",)") - 2 + s
       val_str = trim(fline(s:e))
       if( trim(val_str) .eq. "true" .or. trim(val_str) .eq. "True" &
            .or. trim(val_str) .eq. "false" .or. trim(val_str) .eq. "False" ) then
          if( trim(val_str) .eq. "true" .or. trim(val_str) .eq. "True" ) then
             interpret_generic_logical = .true.
          elseif( trim(val_str) .eq. "false" .or. trim(val_str) .eq. "False" ) then
             interpret_generic_logical = .false.
          else
             write(*,'("error in interpret_generic_logical")')
          end if
       else
          write(*,'("Error (",I0,"): invalid value """,A,""" supplied for """,A,""" (must be LOGICAL)")') &
               line_number, trim(val_str), trim(key)
          stop
       end if
    else
       if( required ) then
          write(*,'("Error (",I0,"): required parameter """,A,""" not found")') &
               line_number, trim(key)
          stop
       else
          interpret_generic_logical = default
       end if
    end if

    write(*,'(A," = ",L1)') trim(key), interpret_generic_logical
  end function interpret_generic_logical

end module interpreter_util
