module moves
  use datatypes
  use util_rand
  use util_math
  use celllist
  implicit none

contains

  !===========================================!
  !     wrapper for all monte carlo moves     !
  !===========================================!
  subroutine perform_move(context)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    ! internal variables
    integer :: move_type
    real(P) :: a

    ! pick a move type
    call random_number(a)
    move_type = minloc(context%system%move_prob, 1, context%system%move_prob .gt. a)
    context%trial_system%trial%move_type = move_type

    ! call the appropriate move routine
    if( context%moves(move_type)%name .eq. "displace" ) then
       call shift_particle(context)
    else if( context%moves(move_type)%name .eq. "rotate" ) then
       call rotate_particle(context)
    else if( context%moves(move_type)%name .eq. "rescale" ) then
       call rescale_box(context)
    else if( context%moves(move_type)%name .eq. "swap_diameters" ) then
       call swap_diameters(context)
    else
       write(*,'("move type ",A," not recognized")') trim(context%moves(move_type)%name)
       stop
    end if

  end subroutine perform_move
  
  !=========================================================================!
  !     applies a random displacement to a previously selected particle     !
  !=========================================================================!  
  subroutine shift_particle(context)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    ! internal variables
    real(P), dimension(3) :: shift_vec, pos
    integer :: particle_id
    real(P) :: a

    context%trial_system%trial%num_trial = 1
    
    call random_number(a)
    particle_id = floor( a * real(context%system%num_particles,P) ) + 1
    context%trial_system%trial%particle_ids(1) = particle_id
    
    ! obtain a random displacement vector
    call random_number(shift_vec)
    shift_vec = ( shift_vec - 0.5_P ) * 2.0_P * context%moves(context%trial_system%trial%move_type)%magnitude
    pos = context%system%particles%particle(particle_id)%position + shift_vec

    where( abs( pos ) .gt. context%system%box_length*0.5_P )
       pos = pos - sign( context%system%box_length, pos )
    end where
    
    context%trial_system%trial%particle(1)%position = pos

    ! set trial particle diameter and orientation even though they do not change
    context%trial_system%trial%particle(1)%diameter = context%system%particles%particle(particle_id)%diameter
    context%trial_system%trial%particle(1)%orientation = context%system%particles%particle(particle_id)%orientation(:,:)

    context%trial_system%trial%full_recompute = .false.
    !context%trial_system%trial%full_recompute = .true.
    !context%trial_system%trial%use_cell_list  = .false.
    
  end subroutine shift_particle

  !=====================================================================!
  !     applies a random rotation to a previously selected particle     !
  !=====================================================================!  
  subroutine rotate_particle(context)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    ! internal variables
    real(P), dimension(3) :: facing, direction, current_orientation
    real(P), dimension(3,3) :: r_mat
    real(P) :: angle, a
    integer :: particle_id, j

    
   context%trial_system%trial%num_trial = 1

    call random_number(a)
    particle_id = floor( a * real(context%system%num_particles,P) ) + 1
    context%trial_system%trial%particle_ids(1) = particle_id
    
    ! obtain a random orientation vector and angle
    call random_sphere(facing)
    call random_number(angle)
    angle = ( angle - 0.5_P ) * 2.0_P * context%moves(context%trial_system%trial%move_type)%magnitude
    current_orientation = context%system%particles%particle(particle_id)%orientation(:,1)
    direction = normalize( cross( facing, current_orientation ) )
    ! construct rotation matrix
    r_mat = rotation_matrix(direction,angle)
    do j = 1, context%system%particles%num_patches
       ! apply the rotation
       context%trial_system%trial%particle(1)%orientation(:,j) = &
            matmul( context%system%particles%particle(particle_id)%orientation(:,j), r_mat )
    end do
    
    ! set trial particle diameter and position even though they do not change
    context%trial_system%trial%particle(1)%position = context%system%particles%particle(particle_id)%position(:)
    context%trial_system%trial%particle(1)%diameter = context%system%particles%particle(particle_id)%diameter
    
    context%trial_system%trial%full_recompute = .false.
    !context%trial_system%trial%full_recompute = .true.
    !context%trial_system%trial%use_cell_list  = .false.
    
  end subroutine rotate_particle

  !==============================================!
  !     swap two random particles' diameters     !
  !==============================================!
  subroutine swap_diameters(context)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    ! internal variables
    integer :: particle_a, particle_b
    real(P) :: a, temp_diam
    type (particle_data_struct) :: temp_particle

    context%trial_system%trial%num_trial = 2

    ! does this set each value in particle_id to 0?
    !context%trial_system%trial%particle_ids = 0 ! flag this as an all-atom move
    context%trial_system%trial%full_recompute = .false.
    
    call random_number(a)
    particle_a = floor( a * real(context%system%num_particles,P) ) + 1
    call random_number(a)
    particle_b = floor( a * real(context%system%num_particles,P) ) + 1
    
    ! store trial particle information in the trial stuct
    context%trial_system%trial%particle_ids(1) = particle_a
    context%trial_system%trial%particle_ids(2) = particle_b

    ! maintain position and orientation
    context%trial_system%trial%particle(1)%position = context%trial_system%particles%particle(particle_a)%position
    context%trial_system%trial%particle(2)%position = context%trial_system%particles%particle(particle_b)%position

    context%trial_system%trial%particle(1)%orientation = context%trial_system%particles%particle(particle_a)%orientation
    context%trial_system%trial%particle(2)%orientation = context%trial_system%particles%particle(particle_b)%orientation

    ! swap diameters
    context%trial_system%trial%particle(1)%diameter = context%trial_system%particles%particle(particle_b)%diameter
    context%trial_system%trial%particle(2)%diameter = context%trial_system%particles%particle(particle_a)%diameter

    ! do I need to update the cell list here?
    !call update_cell_list(context%trial_system)

    ! switch entire particles (including orientations)
    !temp_particle = context%trial_system%particles%particle(particle_a)
    !context%trial_system%particles%particle(particle_a) = context%trial_system%particles%particle(particle_b)
    !context%trial_system%particles%particle(particle_b) = temp_particle

    ! only switch particle diameters
    ! store diameter a
    !temp_diam = context%system%particles%particle(particle_a)%diameter
    ! set diameter a to diameter b
    !context%trial_system%particles%particle(particle_a)%diameter = &
    !     context%system%particles%particle(particle_b)%diameter
    ! set diameter b to temp
    !context%trial_system%particles%particle(particle_b)%diameter = temp_diam
    
  end subroutine swap_diameters
  
  !===========================================================!
  !     applies a random adjustment to the box dimensions     !
  !===========================================================!
  subroutine rescale_box(context)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    ! internal variables
    real(P) :: a, volume, volume_change, scaling, rescale_magnitude
    integer :: dim, rescale_move_id, i

    ! does this set each value in particle_id to 0?
    context%trial_system%trial%particle_ids = 0 ! flag this as an all-atom move
    context%trial_system%trial%full_recompute = .true.

    do i = 1, size( context%moves )
       if( trim(context%moves(i)%name) .eq. "rescale" ) then
          rescale_magnitude = context%moves(i)%magnitude
          rescale_move_id = i
       end if
    end do    
    
    ! currently using variable ORTHORHOMBIC box
    !   (implement floppy box later)
    
    ! obtain a random rescaling factor
    call random_number(a)
    volume_change = (a - 0.5_P) * 2.0_P * rescale_magnitude

    ! calculate the scaling factor for box length and particle positions
    volume = product(context%system%box_length)
    if( context%moves(context%trial_system%trial%move_type)%uniform_rescale ) then
       ! UNIFORM SCALING
       scaling = ( volume + volume_change )**(1.0_P/3.0_P) / ( volume )**(1.0_P/3.0_P)
       dim = 1
    else
       ! INDEPENDENT x/y/z
       scaling = ( volume + volume_change ) / volume
       ! pick a dimension to rescale (1..3)
       call random_number(a)
       dim = minloc(context%moves(rescale_move_id)%rescale_prob, &
            1, context%moves(rescale_move_id)%rescale_prob .gt. a)
    end if

    ! box needs to stay larger than interaction range
    ! so that particles can never interact with their own image
    if( context%system%box_length(dim) * scaling .le. 2.0_P * context%potentials%max_range ) then
       scaling = ( context%potentials%max_range * 2.10_P ) / context%system%box_length(dim)
       write(*,'("Warning: forcing simulation box to stay larger than twice interaction range")')
    end if
    
    ! scale the trial box and particle positions
    !   (assumes the trial_system is up to date!)
    if( context%moves(context%trial_system%trial%move_type)%uniform_rescale ) then
       ! UNIFORM SCALING
       context%trial_system%box_length         = context%system%box_length         * scaling
       do i = 1, context%trial_system%num_particles
          context%trial_system%particles%particle(i)%position = context%system%particles%particle(i)%position * scaling
       end do
    else
       ! INDEPENDENT x/y/z
       context%trial_system%box_length(dim)           = context%system%box_length(dim)  * scaling
       do i = 1, context%trial_system%num_particles
          context%trial_system%particles%particle(i)%position(dim) = context%system%particles%particle(i)%position(dim) * scaling
       end do
    end if

    ! check if cell list must be updated
    call query_cell_list_rebuild(context,context%trial_system)
    
  end subroutine rescale_box
  
end module moves
