module interpreter_config
  use globals
  use interpreter_util
  use datatypes
  use config
  implicit none
  
contains

  !===========================================================!
  !     interpret a config command and send to subroutine     !
  !===========================================================!
  subroutine interpret_config_command(context,fline)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    character(len=*),      intent(inout) :: fline
    ! internal variables
    character(len=100) :: cmd_key, cmd_type
    integer :: s, e

    ! extract move type from fline
    cmd_key = "config."
    s = index(fline,trim(cmd_key)) + len(trim(cmd_key))
    e = index(fline(s:),"(") - 2 + s
    cmd_type = trim(fline(s:e))

    ! switch between different potential types
    if( trim(cmd_type) .eq. "read" ) then
       call interpret_config_read(context,fline)
    else if( trim(cmd_type) .eq. "generate" ) then
       call interpret_config_generate(context,fline)
    else if( trim(cmd_type) .eq. "destroy" ) then
       ! call interpret_config_destroy(context,fline)
    else
       write(*,'("Error (",I0,"): config command ",A," not recognized")') &
            line_number, trim(cmd_type)
       stop
    end if

  end subroutine interpret_config_command

  !========================================!
  !     interpret config.read commands     !
  !========================================!
  subroutine interpret_config_read(context,fline)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    character(len=*),      intent(inout) :: fline
    ! internal variables
    logical :: config_exists
    write(*,'("config.read:")')
    ! required
    context%parameters%config_file = interpret_generic_character("configfile",fline,.true.,"none")
    ! optional
    context%system%particles%num_patches  = interpret_generic_integer("npatch",fline,.false.,0)
    context%system%particles%diameter_mean_var = interpret_real2("diameter",fline,.false.,(/1.0_P,0.0_P/))
    ! check that the specified file exists
    inquire(file=trim(context%parameters%config_file), exist=config_exists)
    if( config_exists ) then
       ! read the file into the context data struct
       call read_context_xml(context)
    else
       write(*,'("Error (",I0,"): file could not be read")') line_number
       stop
    end if
  print*,'I read the config file'
    
  end subroutine interpret_config_read

  !============================================!
  !     interpret config.generate commands     !
  !============================================!
  subroutine interpret_config_generate(context,fline)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    character(len=*),      intent(inout) :: fline
    ! internal variables
    real(P) :: volume
    real(P), dimension(2) :: default_diameters
    write(*,'("config.generate:")')
    default_diameters(1) = 1.0_P
    default_diameters(2) = 0.0_P
    ! required
    context%system%num_particles = interpret_generic_integer("N",fline,.true.,0)
    volume = interpret_generic_real("V",fline,.false.,0.0_P)
    if( volume .le. 0.0_P ) then
       context%system%box_length = interpret_real3("L",fline,.false.,0.0_P)
       volume = product(context%system%box_length)
    end if
    if( volume .le. 0.0_P ) then
       write(*,'("Error (",I0,"): volume must be a non-negative number &
            specified through ""V"" (cubic) or ""L"" (orthorhombic)")') line_number
    end if
    if( context%system%num_particles .lt. 0 ) then
       write(*,'("Error (",I0,"): number of particles must be non-negative")') line_number
       stop
    end if
    ! optional
    context%system%particles%num_patches  = interpret_generic_integer("npatch",fline,.false.,0)
    context%system%nominal_lattice_spacing  = interpret_generic_real("grid",fline,.false.,0.1_P)
    context%system%particles%diameter_mean_var = interpret_real2("diameter",fline,.false.,default_diameters)
    ! generate the context by reading specified file
    call generate_lattice_config(context)
    
  end subroutine interpret_config_generate
  
end module interpreter_config
