module util_math
  use globals
  implicit none

contains

  !=============================================!
  !     compute cross product of 3x3 matrix     !
  !=============================================!
  function cross(a,b)
    implicit none
    real(P), dimension(3) :: cross
    real(P), dimension(3), intent(in) :: a, b
    cross(1) = a(2) * b(3) - a(3) * b(2)
    cross(2) = a(3) * b(1) - a(1) * b(3)
    cross(3) = a(1) * b(2) - a(2) * b(1)
  end function cross

  !===================================!
  !     construct identity matrix     !
  !===================================!
  function eye(rank)
    implicit none
    ! external variables
    integer, intent(in) :: rank
    real(P), dimension(rank,rank) :: eye
    ! internal variables
    integer :: i    
    eye = 0.0_P
    do i = 1, rank
       eye(i,i) = 1.0_P
    end do
  end function eye

  !===================================!
  !     construct rotation matrix     !
  !===================================!
  function rotation_matrix(direction,angle)
    implicit none
    real(P), dimension(3,3) :: rotation_matrix
    ! external variables
    real(P), dimension(3) :: direction
    real(P) :: angle
    ! internal variables
    real(P), dimension(3,3) :: rxr, cross
    integer :: i, j
    ! build cross product matrix
    cross(1,:) = (/0.0_P,          -direction(3), direction(2)/)
    cross(2,:) = (/direction(3),  0.0_P,         -direction(1)/)
    cross(3,:) = (/-direction(2), direction(1), 0.0_P/)
    ! build tensor product
    rxr = 0.0_P
    do i = 1,3
       do j = 1,3
          rxr(i,j) = direction(i)*direction(j)
       end do
    end do
    ! build rotation matrix
    rotation_matrix = cos(angle)*eye(3) + sin(angle)*cross + (1.0_P-cos(angle))*rxr
  end function rotation_matrix

  !======================================!
  !     normalize a 3 element vector     !
  !======================================!
  function normalize(a)
    implicit none
    real(P), dimension(3) :: normalize
    real(P), dimension(3), intent(in) :: a
    normalize = a / sqrt( sum( a**2.0_P ) )
  end function normalize

  !======================================!
  !     convert quaternion to vector     !
  !======================================!
  function quat_to_vec(q)
    implicit none
    real(P), dimension(3) :: quat_to_vec
    ! external variables
    real(P), dimension(4) :: q
    ! internal variables
    real(P), dimension(3) :: b
    b = (/1.0_P,0.0_P,0.0_P/)
    quat_to_vec = ( q(1)*q(1) - dot_product(q(2:4),q(2:4)) ) * b &
         + 2.0_P * q(1) * cross(q(2:4),b) &
         + 2.0_P * dot_product(q(2:4),b) * q(2:4)
  end function quat_to_vec

end module util_math
