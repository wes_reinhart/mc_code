module snapshot
  use datatypes
  implicit none

contains

  !===================================!
  !     setup a snapshot instance     !
  !===================================!
  subroutine open_snapshot(context,snapshot_id)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    integer, intent(in) :: snapshot_id
    ! increment global fid tracker
    last_fid = last_fid + 1
    ! assign a fild ID that has not been used
    context%snapshots(snapshot_id)%fid = last_fid
  end subroutine open_snapshot

  !=============================================!
  !     cleanly close the snapshot instance     !
  !=============================================!
  subroutine close_snapshot(context,snapshot_id)
    ! external variables
    type (context_struct),intent(inout) :: context
    integer, intent(in) :: snapshot_id
    ! close the snapshot instance
    close(context%snapshots(snapshot_id)%fid)
  end subroutine close_snapshot

  !============================================!
  !     returns t padded by up to 10 zeros     !
  !============================================!
  character(len=10) function zero_pad_timestep(timestep)
    implicit none
    ! external variables
    integer, intent(in) :: timestep
    ! internal variables
    character(len=100) :: temp
    integer :: places, zeros

    if( timestep .lt. 0 ) then
       write(zero_pad_timestep,'("**********")')
    elseif( timestep .eq. 0 ) then
       write(zero_pad_timestep,'("0000000000")')
    else
       write(temp,'(I0)') timestep
       places = len(trim(temp))
       zeros  = 10 - places
       if( zeros .lt. 0 ) then
          write(zero_pad_timestep,'("**********")')
       else if( zeros .eq. 0 ) then
          write(zero_pad_timestep,'(I0)') timestep
       else if( zeros .gt. 0 .and. zeros .lt. 10 ) then
          write(zero_pad_timestep,'("0000000000")')
          write(zero_pad_timestep((zeros+1):10),'(I0)') timestep
       end if
    end if

  end function zero_pad_timestep

  !===========================================!
  !     returns t padded by up to 4 zeros     !
  !===========================================!
  character(len=4) function zero_pad_bin(bin)
    implicit none
    ! external variables
    integer, intent(in) :: bin
    ! internal variables
    character(len=100) :: temp
    integer :: places, zeros
    if( bin .lt. 0 ) then
       write(zero_pad_bin,'("**********")')
    elseif( bin .eq. 0 ) then
       write(zero_pad_bin,'("0000")')
    else
       write(temp,'(I0)') bin
       places = len(trim(temp))
       zeros  = 4 - places
       if( zeros .lt. 0 ) then
          write(zero_pad_bin,'("**********")')
       else if( zeros .eq. 0 ) then
          write(zero_pad_bin,'(I0)') bin
       else if( zeros .gt. 0 .and. zeros .lt. 4 ) then
          write(zero_pad_bin,'("0000")')
          write(zero_pad_bin((zeros+1):4),'(I0)') bin
       end if
    end if
  end function zero_pad_bin

  !==========================!
  !     write a snapshot     !
  !==========================!
  subroutine write_snapshot(context,snapshot_id)
    ! external variables
    type (context_struct),intent(inout) :: context
    integer, intent(inout) :: snapshot_id
    ! internal variables
    character(len=1000) :: filename
    integer :: fid

    fid = context%snapshots(snapshot_id)%fid

    if( context%snapshots(snapshot_id)%overwrite ) then
       write(filename,'(A,".xml")') trim(context%snapshots(snapshot_id)%prefix)
    else
       write(filename,'(A,".",A,".xml")') &
            trim(context%snapshots(snapshot_id)%prefix), &
            zero_pad_timestep(context%parameters%timestep)
    end if

    call write_snapshot_by_name(context,filename,fid,snapshot_id)

  end subroutine write_snapshot

  !=========================================================!
  !     write a configuration for the biasing potential     !
  !=========================================================!
  subroutine write_bias_snapshot(context,filename)
    ! external variables
    type (context_struct),intent(inout) :: context
    character(len=*), intent(inout) :: filename
    ! internal variables
    integer :: fid, snapshot_id
    fid = last_fid + 1
    snapshot_id = -1
    call write_snapshot_by_name(context,filename,fid,snapshot_id)
  end subroutine write_bias_snapshot

  !======================================!
  !     write a snapshot by filename     !
  !======================================!
  subroutine write_snapshot_by_name(context,filename,fid,snapshot_id)
    ! external variables
    type (context_struct),intent(inout) :: context
    character(len=1000), intent(in) :: filename
    integer, intent(in) :: fid, snapshot_id
    ! internal variables
    integer :: num_atoms, num_patches, sum_patches, i, j
    real(P) :: avg_npatches
    real(P), dimension(context%system%num_particles) :: particle_potentials

    ! open the file
    open(unit=fid,file=trim(filename),action="write",status="replace")

    ! each particle has the colloid and some number of patches
    num_atoms = context%system%num_particles * (context%system%particles%num_patches + 1)

    ! write HOOMD-style headers
    write(fid,'(A)') "<?xml version=""1.0"" encoding=""UTF-8""?>"
    write(fid,'(A)') "<hoomd_xml version=""1.5"">"
    write(fid,'(A,I0,A,I0,A,I0,A)') &
         '<configuration time_step="',context%parameters%timestep, &
         '" dimensions="3" natoms="', num_atoms, &
         '" npatches="', context%system%particles%num_patches, '" >'

    write(fid,'(A,F0.10,A,F0.10,A,F0.10,A)') &
         "<box lx=""", context%system%box_length(1), &
         """ ly=""",   context%system%box_length(2), &
         """ lz=""",   context%system%box_length(3), &
         """ xy=""0"" xz=""0"" yz=""0""/>"

    ! write positions of patches
    write(fid,'(A,I0,A)') &
         "<position num=""", num_atoms, """>"
    do i = 1, context%system%num_particles
       write(fid,'(F0.10," ",F0.10," ",F0.10)') &
            context%system%particles%particle(i)%position(:)
       do j = 1, context%system%particles%num_patches
          write(fid,'(F0.10," ",F0.10," ",F0.10)') &
               context%system%particles%particle(i)%position(:) &
               + context%system%particles%particle(i)%orientation(:,j) &
               * context%system%particles%particle(i)%diameter*0.5_P
       end do
    end do
    write(fid,'(A,I0,A)') "</position>"

    ! write patch facing vectors
    num_patches = context%system%num_particles * context%system%particles%num_patches
    write(fid,'(A,I0,A)') &
         "<patch num=""", num_patches, """>"
    do i = 1, context%system%num_particles
       do j = 1, context%system%particles%num_patches
          write(fid,'(F0.10," ",F0.10," ",F0.10)') &
               context%system%particles%particle(i)%orientation(:,j)
       end do
    end do
    write(fid,'(A,I0,A)') "</patch>"

    ! write diameters
    write(fid,'(A,I0,A)') &
         "<diameter num=""", num_atoms, """>"
    do i = 1, context%system%num_particles
       write(fid,'(F0.10)') context%system%particles%particle(i)%diameter
       do j = 1, context%system%particles%num_patches
          write(fid,'("0.2")')
       end do
    end do
    write(fid,'(A,I0,A)') "</diameter>"

    ! write types
    write(fid,'(A,I0,A)') &
         "<type num=""", num_atoms, """>"
    do i = 1, context%system%num_particles
       write(fid,'("C")')
       do j = 1, context%system%particles%num_patches
          write(fid,'("P")')
       end do
    end do
    write(fid,'(A,I0,A)') "</type>"

    if( snapshot_id .gt. 0 ) then
       if( context%snapshots(snapshot_id)%potential ) then
          particle_potentials = sum(context%system%potentials%potential_pair,1) * 0.5_P
          write(fid,'(A,I0,A)') &
               "<potentials num=""", context%system%num_particles, """>"
          do i = 1, context%system%num_particles
             write(fid,'(F15.10)') particle_potentials(i)
          end do
          write(fid,'(A,I0,A)') "</potentials>"
       end if
    end if

    ! end the tags
    write(fid,'(A,I0,A)') "</configuration>"
    write(fid,'(A,I0,A)') "</hoomd_xml>"

    ! close the file
    close(fid)

  end subroutine write_snapshot_by_name

end module snapshot
