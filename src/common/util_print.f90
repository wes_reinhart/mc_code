module util_print
  use globals
  use datatypes
  implicit none

contains

  !=================================================!
  !     choose format based on allowable digits     !
  !=================================================!
  function var_real_fmt(val,prec,places)
    implicit none
    character(len=:), allocatable :: var_real_fmt
    ! external variables
    real(P), intent(in) :: val
    integer, intent(in) :: prec
    integer, intent(in) :: places
    ! internal variables
    character(len=100) :: fmt
    integer :: nfmt

    fmt = var_float(val,prec)
    nfmt = len(trim(fmt))
    if( nfmt .gt. places ) then
       fmt = var_exp(val,prec)
       nfmt = len(trim(fmt))
       allocate( character(len=nfmt) :: var_real_fmt )
       var_real_fmt = trim(fmt)
    else
       ! write(*,'("floating point okay: ",A)') trim(fmt)
       ! floating point is short enough
       allocate( character(len=nfmt) :: var_real_fmt )
       var_real_fmt = trim(fmt)
    end if
  end function var_real_fmt

  !==================================================!
  !     write variable width scientific notation     !
  !==================================================!
  function var_exp(val,prec)
    implicit none
    character(len=:), allocatable :: var_exp
    ! external variables
    real(P), intent(in) :: val
    integer, intent(in) :: prec
    ! internal variables
    integer :: tot_places
    character(len=100) :: preproc, preproc_fmt
    character(len=100) :: exp_char, pre_dec, post_dec
    integer :: l, s, d, exp
    write(preproc_fmt,'("(E100.",I0,")")') prec + 1
    write(preproc,trim(preproc_fmt)) val
    preproc = trim(preproc)
    l = len(preproc)
    s = index(preproc,"E")
    exp_char = preproc((s+1):l)
    read(exp_char,*) exp
    exp = exp - 1
    if( exp .ge. 0 ) then
       write(exp_char,'("+",I0)') abs(exp)
    else
       write(exp_char,'("-",I0)') abs(exp)
    end if
    d = index(preproc,".")
    pre_dec = preproc((d+1):(d+1))
    post_dec = preproc((d+2):(s-1))
    tot_places = len(trim(pre_dec)) + 1 + len(trim(post_dec)) + 1 + len(trim(exp_char))
    allocate( character(len=tot_places) :: var_exp )
    write(var_exp,'(A,A,A,A,A)') trim(pre_dec),".",trim(post_dec),"E",trim(exp_char)
  end function var_exp

  !=============================================!
  !     write variable width floating point     !
  !=============================================!
  function var_float(val,prec)
    implicit none
    character(len=:), allocatable :: var_float
    ! external variables
    real(P), intent(in) :: val
    integer, intent(in) :: prec
    ! internal variables
    integer(16) :: pre_decimal
    integer(16) :: pre_places, tot_places
    character(len=100) :: fmt
    pre_decimal = int( sign( real(floor(abs(val),kind(pre_decimal)),P), val ), kind(pre_decimal))
    if( abs(pre_decimal) .lt. 1 ) then
       pre_places = 1
    else
       pre_places  = floor( log( real(abs(pre_decimal),P)) / log(10.0_P), kind(pre_decimal)  ) + 1
    end if
    tot_places = pre_places + prec + 2 ! accounts for sign and decimal
    write(fmt,'("(F",I0,".",I0,")")') tot_places, prec
    allocate( character(len=tot_places) :: var_float )
    write(var_float,trim(fmt)) val
  end function var_float

end module util_print
