module interpreter_potential
  use globals
  use interpreter_util
  use datatypes
  use potential_bias
  implicit none

contains

  !==============================================================!
  !     interpret a potential command and send to subroutine     !
  !==============================================================!
  subroutine interpret_potential_command(context,fline)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    character(len=*), intent(inout) :: fline
    ! internal variables
    character(len=100) :: cmd_key, cmd_type
    integer :: s, e

    ! extract move type from fline
    cmd_key = "potential."
    s = index(fline,trim(cmd_key)) + len(trim(cmd_key))
    e = index(fline(s:),"(") - 2 + s
    cmd_type = trim(fline(s:e))

    ! switch between different potential types
    if( trim(cmd_type) .eq. "pair_kf" ) then
       call interpret_potential_pair_kf(context,fline)
    else if( trim(cmd_type) .eq. "pair_ckf" ) then
       call interpret_potential_pair_ckf(context,fline)
    else if( trim(cmd_type) .eq. "pair_lj" ) then
       call interpret_potential_pair_lj(context,fline)
    else if( trim(cmd_type) .eq. "pair_wca" ) then
       call interpret_potential_pair_wca(context,fline)
    else if( trim(cmd_type) .eq. "pair_hs" ) then
       call interpret_potential_pair_hs(context,fline)
    else if( trim(cmd_type) .eq. "restrain_tr" ) then
       call interpret_potential_restrain_tr(context,fline)
    else if( trim(cmd_type) .eq. "restrain_or" ) then
       call interpret_potential_restrain_or(context,fline)
    else if( trim(cmd_type) .eq. "bias" ) then
       call interpret_potential_bias(context,fline)
    else
       write(*,'("Error (",I0,"): potential type ",A," not recognized")') &
            line_number, trim(cmd_type)
       stop
    end if

  end subroutine interpret_potential_command

  !====================================!
  !     interpret pair_kf commands     !
  !====================================!
  subroutine interpret_potential_pair_kf(context,fline)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    character(len=*),      intent(inout) :: fline
    write(*,'("potential.pair_kf:")')
    context%potentials%kf%enabled      = interpret_generic_logical("enabled",fline,.false.,.true.)
    context%potentials%kf%log          = interpret_generic_logical("log",fline,.false.,.false.)
    ! only continue reading if potential is enabled
    if( context%potentials%kf%enabled .or. context%potentials%kf%log ) then
       context%potentials%kf%delta = interpret_generic_real("delta",fline,.true.,0.0_P)
       context%potentials%kf%align_cutoff  = interpret_generic_real("alpha",fline,.true.,0.0_P)
       context%potentials%kf%radial_cutoff = 1.0_P
    end if
  end subroutine interpret_potential_pair_kf

  !=====================================!
  !     interpret pair_ckf commands     !
  !=====================================!
  subroutine interpret_potential_pair_ckf(context,fline)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    character(len=*),      intent(inout) :: fline
    write(*,'("potential.pair_ckf:")')
    ! CKF potential relies on WCA cutoff
    context%potentials%wca%radial_cutoff = 2.0_P**(1.0_P/6.0_P)
    ! read parameters from fline
    context%potentials%ckf%enabled      = interpret_generic_logical("enabled",fline,.false.,.true.)
    context%potentials%ckf%log          = interpret_generic_logical("log",fline,.false.,.false.)
    ! only continue reading if potential is enabled
    if( context%potentials%ckf%enabled .or. context%potentials%ckf%log ) then
       context%potentials%ckf%radial_cutoff = interpret_generic_real("rcut",fline,.true.,0.0_P)
       context%potentials%ckf%r_eq          = interpret_generic_real("req",fline,.true.,0.0_P)
       context%potentials%ckf%Mr            = interpret_generic_real("Mr",fline,.true.,0.0_P)
       context%potentials%ckf%Md            = interpret_generic_real("Md",fline,.true.,0.0_P)
       context%potentials%ckf%alpha         = interpret_generic_real("alpha",fline,.true.,0.0_P)
       context%potentials%ckf%omega         = interpret_generic_real("omega",fline,.true.,0.0_P)
       context%potentials%ckf%shift         = interpret_generic_logical("shift",fline,.false.,.false.)
       if( context%potentials%ckf%shift ) then
          write(*,'("Error (",I0,"): shift is not currently supported for CKF potential.")') line_number
          stop
       end if
    end if
  end subroutine interpret_potential_pair_ckf

  !====================================!
  !     interpret pair_lj commands     !
  !====================================!
  subroutine interpret_potential_pair_lj(context,fline)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    character(len=*),      intent(inout) :: fline
    real(P) :: r_ij_m6, r_ij_p3, rho
    write(*,'("potential.pair_lj:")')
    ! read parameters from fline
    context%potentials%lj%enabled = interpret_generic_logical("enabled",fline,.false.,.true.)
    context%potentials%lj%log     = interpret_generic_logical("log",fline,.false.,.false.)
    context%potentials%lj%correction = 0.0_P
    ! only continue reading if potential is enabled
    if( context%potentials%lj%enabled .or. context%potentials%lj%log ) then
       context%potentials%lj%radial_cutoff = interpret_generic_real("rcut",fline,.true.,0.0_P)
       context%potentials%lj%shift    = interpret_generic_logical("shift",fline,.false.,.false.)
       ! calculate energy shift
       if( context%potentials%lj%shift ) then
          r_ij_m6 = context%potentials%lj%radial_cutoff ** (-6.0_P)
          context%potentials%lj%correction = 4.0_P * ( r_ij_m6*r_ij_m6 - r_ij_m6 )
       end if
       context%potentials%lj%lrc      = interpret_generic_logical("lrc",fline,.false.,.false.)
       ! calculate long-range corrections
       if( context%potentials%lj%lrc ) then
          if( context%potentials%lj%shift ) then
             write(*,'("Warning (",I0,"): both shift and lrc requested; using only shift.")') line_number
          else
             write(*,'("Warning (",I0,"): lrc currently assumes constant density.")') line_number
             rho = context%system%num_particles/product(context%system%box_length)
             r_ij_p3 = context%potentials%lj%radial_cutoff ** (3.0_P)
             context%potentials%lj%correction = 2.0_P * pi * rho &
                  * (4.0_P - 12.0_P * r_ij_p3 * r_ij_p3 ) &
                  / ( 9.0_P * r_ij_p3 * r_ij_p3 * r_ij_p3 )
          end if
       end if
    end if
  end subroutine interpret_potential_pair_lj

  !=====================================!
  !     interpret pair_wca commands     !
  !=====================================!
  subroutine interpret_potential_pair_wca(context,fline)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    character(len=*),      intent(inout) :: fline
    write(*,'("potential.pair_wca:")')
    ! read parameters from fline
    context%potentials%wca%sigma = interpret_generic_real("sigma",fline,.false.,1.0_P)
    context%potentials%wca%enabled = interpret_generic_logical("enabled",fline,.false.,.true.)
    context%potentials%wca%log     = interpret_generic_logical("log",fline,.false.,&
         context%potentials%wca%enabled)
    ! WCA has a defined cutoff
    context%potentials%wca%radial_cutoff = 2.0_P**(1.0_P/6.0_P)*context%potentials%wca%sigma ! WCA cutoff
  end subroutine interpret_potential_pair_wca

  !====================================!
  !     interpret pair_hs commands     !
  !====================================!
  subroutine interpret_potential_pair_hs(context,fline)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    character(len=*),      intent(inout) :: fline
    write(*,'("potential.pair_hs:")')
    ! HS has a defined cutoff
    ! no need to worry about pointers
    context%potentials%hs%radial_cutoff = 1.0_P
    ! read parameters from fline
    context%potentials%hs%enabled = interpret_generic_logical("enabled",fline,.false.,.true.)
    context%potentials%hs%log     = interpret_generic_logical("log",fline,.false.,&
         context%potentials%hs%enabled)
  end subroutine interpret_potential_pair_hs

  !========================================!
  !     interpret restrain_tr commands     !
  !========================================!
  subroutine interpret_potential_restrain_tr(context,fline)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    character(len=*),      intent(inout) :: fline
    write(*,'("potential.restrain_tr:")')
    ! read parameters from fline
    context%potentials%restrain_tr%enabled = interpret_generic_logical("enabled",fline,.false.,.true.)
    context%potentials%restrain_tr%log     = interpret_generic_logical("log",fline,.false.,.false.)
    ! only continue reading if potential is enabled
    if( context%potentials%restrain_tr%enabled .or. context%potentials%restrain_tr%log ) then
       context%potentials%restrain_tr%lambda   = interpret_generic_real("lambda",fline,.true.,0.0_P)
    end if
  end subroutine interpret_potential_restrain_tr

  !========================================!
  !     interpret restrain_or commands     !
  !========================================!
  subroutine interpret_potential_restrain_or(context,fline)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    character(len=*),      intent(inout) :: fline
    write(*,'("potential.restrain_or:")')
    ! read parameters from fline
    context%potentials%restrain_or%enabled = interpret_generic_logical("enabled",fline,.false.,.true.)
    context%potentials%restrain_or%log     = interpret_generic_logical("log",fline,.false.,.false.)
    ! only continue reading if potential is enabled
    if( context%potentials%restrain_or%enabled .or. context%potentials%restrain_or%log ) then
       context%potentials%restrain_or%lambda   = interpret_generic_real("lambda",fline,.true.,0.0_P)
    end if
  end subroutine interpret_potential_restrain_or

  !=================================!
  !     interpret bias commands     !
  !=================================!
  subroutine interpret_potential_bias(context,fline)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    character(len=*),      intent(inout) :: fline
    ! internal variables
    logical :: file_exists
    real(P) :: delta_v
    integer :: i
    write(*,'("potential.bias:")')
    ! read parameters from fline
    context%potentials%bias%enabled = interpret_generic_logical("enabled",fline,.false.,.true.)
    context%potentials%bias%log     = interpret_generic_logical("log",fline,.false.,.false.)
    ! only continue reading if potential is enabled
    if( context%potentials%bias%enabled .or. context%potentials%bias%log ) then
       context%potentials%bias%prefix  = interpret_generic_character("prefix",fline,.true.,"")
       context%potentials%bias%f         = interpret_generic_real("f",fline,.false.,1.0_P)
       context%potentials%bias%period    = interpret_generic_integer("period",fline,.false.,0)
       inquire(file=trim(context%potentials%bias%prefix)//"_potential.dat", exist=file_exists)
       if( file_exists ) then
          write(*,'("Loading biasing potential from file")')
          call load_bias(context)
       else
          write(*,'("Error (",I0,"): potential file could not be read")') line_number
          stop
       end if
    end if
  end subroutine interpret_potential_bias

end module interpreter_potential
