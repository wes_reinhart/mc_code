module potential_pair_ckf
  use globals
  use datatypes_potentials
  use datatypes
  implicit none

contains

  !=========================================================!
  !     computes continuous kern-frenkel pair potential     !
  !=========================================================!
  real(P) function eval_ckf(potentials,particle_i,particle_j,r_ij,r_ij_length)
    implicit none
    ! external variables
    type (potentials_struct), intent(inout) :: potentials
    type (particle_data_struct), intent(inout) :: particle_i, particle_j
    real(P), dimension(3), intent(in) :: r_ij
    real(P), intent(in) :: r_ij_length
    ! internal variables
    real(P) :: r_ij_m6
    real(P) :: ckf_aligned, U_WCA, U_Morse
    real(P) :: Md, Mr, r_eq

    ! assume no interaction
    eval_ckf = 0.0_P

    ! continuous patchy potential
    if( r_ij_length .lt. potentials%ckf%radial_cutoff ) then
       if( r_ij_length .lt. potentials%WCA%radial_cutoff ) then
          r_ij_m6 = r_ij_length**(-6)
          U_WCA = 4.0_P * ( r_ij_m6*r_ij_m6 - r_ij_m6 + 0.25_P )

       else

          U_WCA = 0.0_P

       end if

       Md   = potentials%ckf%Md
       r_eq = potentials%ckf%r_eq
       Mr   = potentials%ckf%Mr

       U_Morse = Md * ( ( 1.0_P - exp(-(r_ij_length - r_eq)/Mr))**2 - 1.0_P )

       ckf_aligned = ckf_alignment(potentials,r_ij/r_ij_length,particle_i,particle_j)

       eval_ckf = U_WCA + U_Morse * ckf_aligned

    end if

  end function eval_ckf

  !===============================================================!
  !     return alignment status of i and j on interval [-1,1]     !
  !===============================================================!
  real(P) function ckf_alignment(potentials,r_ij,particle_i,particle_j)
    implicit none
    ! external variables
    type (potentials_struct), intent(inout) :: potentials
    type (particle_data_struct), intent(inout) :: particle_i, particle_j
    real(P), dimension(:),    intent(in) :: r_ij
    ! internal variables
    real(P), dimension(:,:), allocatable :: i_orient, j_orient
    real(P) :: gamma_i, gamma_j, i_align, j_align
    integer :: np

    i_orient = particle_i%orientation(:,:)
    j_orient = particle_j%orientation(:,:)

    np = size(i_orient,2)
    
    gamma_i = dot_product( i_orient(:,1), r_ij )
    gamma_j = dot_product( j_orient(:,1), r_ij )

    if( np .eq. 1 .or. np .eq. 2 ) then
       i_align = 1.0_P / ( 1.0_P + exp( -potentials%ckf%omega * &
            (gamma_i**real(np,P) - potentials%ckf%alpha) ) )
       j_align = 1.0_P / ( 1.0_P + exp( -potentials%ckf%omega * &
            (gamma_j**real(np,P) - potentials%ckf%alpha) ) )
    else
       write(*,'("Error: CKF potential only defined for one or two patches")')
       stop
    end if

    ckf_alignment = i_align * j_align

  end function ckf_alignment

end module potential_pair_ckf
