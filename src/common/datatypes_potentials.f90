module datatypes_potentials
  use globals
  implicit none

  ! do the radial_cutoff need to be related to diameter?

  !==========================================!
  !     parameters for generic potential     !
  !==========================================!
  type :: potential_struct
     ! system variables
     logical :: enabled = .false. ! should the potential contribute to the Hamiltonian
     logical :: log     = .false. ! should the logger evaluate the potential
     real(P) :: tot_potential
  end type potential_struct

  !===============================================!
  !     parameters for generic pair potential     !
  !===============================================!
  type, extends(potential_struct) :: potential_pair_struct
     real(P) :: radial_cutoff ! 'diameter' between two particles
     ! truncation corrections
     logical :: shift = .false. ! should the potential be shifted at its cutoff?
     logical :: lrc   = .false. ! should long-range corrections be applied?
     real(P) :: correction = 0  ! for energy shift
  end type potential_pair_struct

  !===============================================!
  !     parameters for WCA potential     !
  !===============================================!
  type, extends( potential_pair_struct ) :: potential_pair_wca_struct
     real(P) :: sigma
  end type potential_pair_wca_struct

  !===============================================!
  !     parameters for Kern-Frenkel potential     !
  !===============================================!
  type, extends( potential_pair_struct ) :: potential_pair_kf_struct
     real(P) :: align_cutoff  ! patch extends this far from pole, in radians
     real(P) :: delta ! distance from particle surface to surface of patch
  end type potential_pair_kf_struct

  !==========================================================!
  !     parameters for continuous Kern-Frenkel potential     !
  !==========================================================!
  type, extends( potential_pair_struct) :: potential_pair_ckf_struct
     ! potential parameters
     real(P) :: r_eq          ! equilibrium bond length
     real(P) :: Mr            !
     real(P) :: Md            !
     real(P) :: alpha         ! width of orientation dependence
     real(P) :: omega         ! steepness of orientation dependence
  end type potential_pair_ckf_struct

  !================================================!
  !     parameters for generic restraint field     !
  !================================================!
  type, extends( potential_struct ) :: potential_restrain_struct
     ! potential parameters
     real(P) :: lambda ! strength of field
  end type potential_restrain_struct

  !====================================================!
  !     parameters for translation restraint field     !
  !====================================================!
  type, extends( potential_restrain_struct ) :: potential_restrain_tr_struct
     real(P), dimension(:,:), allocatable :: ref ! store reference positions
  end type potential_restrain_tr_struct

  !====================================================!
  !     parameters for orientation restraint field     !
  !====================================================!
  type, extends( potential_restrain_struct ) :: potential_restrain_or_struct
     real(P), dimension(:,:,:), allocatable :: ref ! store reference orientations
  end type potential_restrain_or_struct

  !==========================================!
  !     parameters for biasing potential     !
  !==========================================!
  type, extends( potential_struct ) :: potential_bias_struct
     ! potential parameters
     character(len=1000) :: prefix
     integer :: period
     real(P) :: f ! bias adjust weight
     integer, dimension(2) :: n_bins ! number of bins for histogram
     real(P), dimension(:),   allocatable :: v_bins, u_bins ! store histogram bins
     real(P), dimension(:,:), allocatable :: bias ! store biasing potential
     integer, dimension(:,:), allocatable :: hist ! store biasing potential histogram
  end type potential_bias_struct

  !=========================================!
  !     holds parameters for potentials     !
  !=========================================!
  type :: potentials_struct
     ! global cutoff
     real(P) :: max_range = 0.0_P

     ! PAIR POTENTIALS
     ! (1) Kern-Frenkel (KF)
     type (potential_pair_kf_struct) :: kf
     ! (2) "continuous Kern-Frenkel" (CKF)
     type (potential_pair_ckf_struct) :: ckf
     ! (3) Lennard-Jones (LJ)
     type (potential_pair_struct) :: lj
     ! (4) Weeks-Chandler-Andersen (WCA)
     type (potential_pair_wca_struct) :: wca
     ! (5) hard spheres (HS)
     type (potential_pair_struct) :: hs

     ! OTHER POTENTIALS
     ! (6) translational restraints (harmonic springs)
     type (potential_restrain_tr_struct) :: restrain_tr
     ! (7) rotational restraints
     type (potential_restrain_or_struct) :: restrain_or
     ! (8) biasing potential
     type (potential_bias_struct) :: bias
  end type potentials_struct

contains

  !=======================================================!
  !     update max_range of all enabled potential_pair     !
  !=======================================================!
  subroutine update_potential_pair_max_range(potentials,d_max)
    implicit none
    ! external variables
    type (potentials_struct), intent(inout) :: potentials
    real(P), intent(in) :: d_max
    potentials%max_range = 0.0_P
    if( potentials%kf%enabled ) then
       if( potentials%kf%radial_cutoff .gt. potentials%max_range ) then
          potentials%max_range = potentials%kf%radial_cutoff * d_max + potentials%kf%delta*2.0_P
       end if
    end if
    if( potentials%ckf%enabled ) then
       if( potentials%ckf%radial_cutoff .gt. potentials%max_range ) then
          potentials%max_range = potentials%ckf%radial_cutoff
       end if
    end if
    if( potentials%lj%enabled ) then
       if( potentials%lj%radial_cutoff .gt. potentials%max_range ) then
          potentials%max_range = potentials%lj%radial_cutoff * d_max
       end if
    end if
    if( potentials%wca%enabled ) then
       if( potentials%wca%radial_cutoff .gt. potentials%max_range ) then
          potentials%max_range = potentials%wca%radial_cutoff * d_max
       end if
    end if
    if( potentials%hs%enabled ) then
       if( potentials%hs%radial_cutoff .gt. potentials%max_range ) then
          potentials%max_range = potentials%hs%radial_cutoff * d_max
       end if
    end if
  end subroutine update_potential_pair_max_range
  ! max range is largest diameter plus delta (interaction distance)

end module datatypes_potentials
