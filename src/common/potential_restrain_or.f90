module potential_restrain_or
  use globals
  use datatypes_potentials
  use datatypes
  implicit none

contains
  
  !===================================================!
  !     return orientation part of Einstein field     !
  !===================================================!
  real(P) function eval_restrain_or(potentials,n_i,n_i_ref)
    implicit none
    ! external variables
    type (potentials_struct), intent(in) :: potentials
    real(P), dimension(:,:),  intent(in) :: n_i, n_i_ref
    ! internal variables
    real(P) :: sin_sq, min_sin_sq, dot
    integer :: i, j, min_patch, np

    eval_restrain_or = 0.0_P

    np = size(n_i,2)
    
    if( np .le. 2 ) then

       ! use only one minimum angle due to symmetry
       min_sin_sq = infinity
       do j = 1, np
          dot = dot_product( n_i(:,j), n_i_ref(:,1) )
          sin_sq = 1.0_P - sign(dot**2.0_P,dot)
          ! catch finite-precision errors
          if( sin_sq .lt. 0.0_P ) sin_sq = 0.0_P
          if( sin_sq .lt. min_sin_sq ) min_sin_sq = sin_sq
       end do
       
       eval_restrain_or = potentials%restrain_or%lambda * min_sin_sq
       
    else if( np .le. 6 ) then
       
       ! use two patches for extra rotational dof
       ! use minimum angles since patches are not 'labelled'
       eval_restrain_or = 0.0_P
       min_patch = 0
       
       do i = 1, 2
          min_sin_sq = infinity
          do j = 1, np
             dot = dot_product( n_i(:,j), n_i_ref(:,i) )**2.0_P
             sin_sq = 1.0_P - sign(dot**2.0_P,dot)
             ! catch finite-precision errors
             if( sin_sq .lt. 0.0_P ) sin_sq = 0.0_P
             if( sin_sq .lt. min_sin_sq .and. min_patch .ne. j ) then
                min_sin_sq = sin_sq
                min_patch = j
             end if
          end do
          
          eval_restrain_or = eval_restrain_or &
               + potentials%restrain_or%lambda * min_sin_sq
          
       end do
       
    end if
    
  end function eval_restrain_or

end module potential_restrain_or
