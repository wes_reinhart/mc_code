module potential_pair_lj
  use globals
  use datatypes_potentials
  use datatypes
  implicit none

contains

  !===============================================!
  !     computes Lennard-Jones pair potential     !
  !===============================================!
  real(P) function eval_lj(potentials,particle_i,particle_j,r_ij_length)
    implicit none
    ! external variables
    type (potentials_struct), intent(inout) :: potentials
    type (particle_data_struct), intent(inout) :: particle_i, particle_j
    real(P), intent(in) :: r_ij_length
    ! internal variables
    real(P) :: r_ij_m6
    
    ! assume no interaction
    eval_lj = 0.0_P

    ! Lennard-Jones potential
    ! no need for diameter
    if( r_ij_length .le. potentials%lj%radial_cutoff ) then
       r_ij_m6 = r_ij_length**(-6)
       eval_lj = 4.0_P * ( r_ij_m6*r_ij_m6 - r_ij_m6 ) - potentials%lj%correction
    end if

  end function eval_lj

end module potential_pair_lj
