module compute
  use globals
  use datatypes_potentials
  use datatypes
  use util_mc
  use celllist
  use potential_pair_lj
  use potential_pair_wca
  use potential_pair_hs
  use potential_pair_kf
  use potential_pair_ckf
  use potential_restrain_tr
  use potential_restrain_or
  use potential_bias
  implicit none

contains

  !=======================================================!
  !     wrapper for all potential energy calculations     !
  !=======================================================!
  subroutine reset_potential(context)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    ! internal variables
    real(P) :: old_potential, old_pair, old_restrain_tr, old_restrain_or, old_bias, diff
    logical :: reset_flag

    reset_flag = .false.

    ! store current values of the potentials
    old_potential   = context%system%potentials%tot_potential
    old_pair        = context%system%potentials%tot_potential_pair
    if( context%potentials%restrain_tr%enabled ) &
         old_restrain_tr = context%system%potentials%tot_potential_restrain_tr
    if( context%potentials%restrain_or%enabled ) &
         old_restrain_or = context%system%potentials%tot_potential_restrain_or
    if( context%potentials%bias%enabled ) &
         old_bias = context%system%potentials%tot_potential_bias

    ! set flags for full recompute without cell list
    !   (no chance for mistakes in the algorithm)
    context%system%trial%full_recompute = .true.
    context%system%trial%use_cell_list  = .false.

    ! perform an all-atom, complete reset of the potentials
    call update_potential(context%system,context%potentials)

    ! reset cell list preference
    if( any( context%system%cell_list%cell_dim .gt. 3 ) ) then
       context%system%trial%use_cell_list = .true.
    else
       context%system%trial%use_cell_list = .false.
    end if

    ! write(*,'("RESET : total : ",A," -> ",A)') &
    !         var_real_fmt(old_potential,3,10), &
    !         var_real_fmt(context%system%potentials%tot_potential,3,10)
    ! diff = abs(old_potential - context%system%potentials%tot_potential)
    ! if( diff .gt. almost_zero ) reset_flag = .true.
    ! write(*,'("RESET : pair : ",A," -> ",A)') &
    !         var_real_fmt(old_pair,3,10), &
    !         var_real_fmt(context%system%potentials%tot_potential_pair,3,10)

    diff = abs(old_potential - context%system%potentials%tot_potential)
    if( diff .gt. almost_zero ) then
      reset_flag = .true.
      write(*,'("RESET : total : ",A," -> ",A)') &
           var_real_fmt(old_potential,3,10), &
           var_real_fmt(context%system%potentials%tot_potential,3,10)
    end if

    diff = abs(old_pair - context%system%potentials%tot_potential_pair)
    if( diff .gt. almost_zero ) then
      write(*,'("RESET : pair : ",A," -> ",A)') &
           var_real_fmt(old_pair,3,10), &
           var_real_fmt(context%system%potentials%tot_potential_pair,3,10)
    end if

    if( context%potentials%restrain_tr%enabled ) then
       diff = abs(old_restrain_tr - context%system%potentials%tot_potential_restrain_tr)
       if( diff .gt. almost_zero )  then
          reset_flag = .true.
          write(*,'("RESET : restrain_tr : ",A," -> ",A)') &
               var_real_fmt(old_restrain_tr,3,10), &
               var_real_fmt(context%system%potentials%tot_potential_restrain_tr,3,10)
       end if
    end if

    if( context%potentials%restrain_or%enabled ) then
       diff = abs(old_restrain_or - context%system%potentials%tot_potential_restrain_or)
       if( diff .gt. almost_zero )  then
          reset_flag = .true.
          write(*,'("RESET : restrain_or : ",A," -> ",A)') &
               var_real_fmt(old_restrain_or,3,10), &
               var_real_fmt(context%system%potentials%tot_potential_restrain_or,3,10)
       end if
    end if

    if( context%potentials%bias%enabled ) then
       diff = abs(old_bias - context%system%potentials%tot_potential_bias)
       if( diff .gt. almost_zero ) then
          reset_flag = .true.
          write(*,'("RESET : bias : ",A," -> ",A)') &
               var_real_fmt(old_bias,3,10), &
               var_real_fmt(context%system%potentials%tot_potential_bias,3,10)
       end if
    end if

    if( reset_flag ) &
         write(*,'("RESET: ",5F10.5)') &
         context%system%potentials%tot_potential, &
         context%system%potentials%tot_potential_pair, &
         context%system%potentials%tot_potential_restrain_tr, &
         context%system%potentials%tot_potential_restrain_or, &
         context%system%potentials%tot_potential_bias

  end subroutine reset_potential

  !=======================================================!
  !     wrapper for all potential energy calculations     !
  !=======================================================!
  subroutine update_potential(system,potentials)
    implicit none
    ! external variables
    type (system_struct),     intent(inout) :: system
    type (potentials_struct), intent(inout) :: potentials

    if( system%trial%full_recompute ) then
       system%potentials%tot_potential = 0.0_P
       system%potentials%tot_potential_pair = 0.0_P
       system%potentials%tot_potential_restrain_tr = 0.0_P
       system%potentials%tot_potential_restrain_or = 0.0_P
       system%potentials%tot_potential_bias = 0.0_P
    end if

    ! pair potentials
    if( potentials%kf%enabled .or. &
         potentials%ckf%enabled .or. &
         potentials%lj%enabled .or. &
         potentials%wca%enabled .or. &
         potentials%hs%enabled ) then

       if( system%trial%full_recompute ) then

          system%potentials%tot_potential_pair = 0.0_P

          if( system%trial%use_cell_list ) then
             ! full recompute with cell list
             call calculate_all_pair_potentials_cell_list(system,potentials)
          else
             ! full recompute without cell list
             call calculate_all_pair_potentials(system,potentials)
          end if

          call calculate_all_bias_potentials(system,potentials)

       else ! (full_recompute .ne. .true.)

          if( system%trial%use_cell_list ) then
             ! trial state update with cell list
             call calculate_trial_pair_potentials_cell_list(system,potentials)
          else
             ! trial state update without cell list
             call calculate_trial_pair_potentials(system,potentials)
          end if

          call calculate_trial_bias_potentials(system,potentials)

       end if

    end if

    if( system%trial%full_recompute ) then
       ! full recompute
       call calculate_all_restraint_potentials(system,potentials)
    else
       ! trial state update
       call calculate_trial_restraint_potentials(system,potentials)
    end if

  end subroutine update_potential

  !===========================================================!
  !     compute all pair potentials without the cell list     !
  !===========================================================!
  subroutine calculate_all_pair_potentials(system,potentials)
    implicit none
    ! external variables
    type (system_struct),     intent(inout) :: system
    type (potentials_struct), intent(inout) :: potentials
    ! internal variables
    integer :: i, j
    real(P) :: u_ij

    system%potentials%potential_pair = 0.0_P

    ! loop through particle pairs
    do i = 1, (system%num_particles-1)
       do j = i+1, system%num_particles
          ! compute pair potentials
          u_ij = pair_potential(system,potentials,system%particles%particle(i),system%particles%particle(j))
          ! assign to memory
          system%potentials%potential_pair(i,j) = u_ij
          system%potentials%potential_pair(j,i) = u_ij
       end do
    end do

    ! avoid double counting
    system%potentials%tot_potential_pair = sum( system%potentials%potential_pair ) * 0.5_P

    ! re-sum
    system%potentials%tot_potential = &
         system%potentials%tot_potential_pair + &
         system%potentials%tot_potential_restrain_tr + &
         system%potentials%tot_potential_restrain_or + &
         system%potentials%tot_potential_bias

  end subroutine calculate_all_pair_potentials

  !========================================================!
  !     compute all pair potentials with the cell list     !
  !========================================================!
  subroutine calculate_all_pair_potentials_cell_list(system,potentials)
    implicit none
    ! external variables
    type (system_struct),     intent(inout) :: system
    type (potentials_struct), intent(inout) :: potentials
    ! internal variables
    integer :: particle_id, neighbor_id, local_id
    integer :: cell_id, neighbor_cell_id, cell_counter
    real(P) :: u_ij

    system%potentials%potential_pair = 0.0_P

    ! loop through particle pairs
    do particle_id = 1, system%num_particles
       ! find the cell id from the particle data
       cell_id = which_cell(system,system%particles%particle(particle_id)%position(:))
       ! loop over neighbor cells (includes itself)
       do cell_counter = 1, 27
          ! retrieve neighbor cell id
          neighbor_cell_id = system%cell_list%neighbors(cell_counter,cell_id)
          ! loop through particles in the chosen cell
          do local_id = 1, system%cell_list%cell_count(neighbor_cell_id)
             ! find its identity from the map
             neighbor_id = system%cell_list%cell_map(local_id,neighbor_cell_id)
             ! skip lower triangular interactions
             if( neighbor_id .gt. particle_id ) then
                ! compute pair potentials
                u_ij = pair_potential(system,potentials, system%particles%particle(particle_id), &
                     system%particles%particle(neighbor_id))
                ! assign to memory
                system%potentials%potential_pair(particle_id,neighbor_id) = u_ij
                system%potentials%potential_pair(neighbor_id,particle_id) = u_ij
             end if
          end do
       end do
    end do

    ! avoid double counting
    system%potentials%tot_potential_pair = sum( system%potentials%potential_pair ) * 0.5_P

    ! re-sum
    system%potentials%tot_potential = &
         system%potentials%tot_potential_pair + &
         system%potentials%tot_potential_restrain_tr + &
         system%potentials%tot_potential_restrain_or + &
         system%potentials%tot_potential_bias

  end subroutine calculate_all_pair_potentials_cell_list

  !======================================================================================================!
  !    compute all pair potentials involving the last moved particle(s) and those in neighboring cells   !
  !======================================================================================================!
  subroutine calculate_trial_pair_potentials_cell_list(system,potentials)
    implicit none
    ! external variables
    type (system_struct),     intent(inout) :: system
    type (potentials_struct), intent(inout) :: potentials
    ! internal variables
    integer :: neighbor_id, local_id, nt, n, i
    integer, dimension(1) :: j
    integer, dimension(:), allocatable :: particle_ids
    integer :: cell_counter
    integer, dimension(:), allocatable :: cell_ids, neighbor_cell_ids
    integer, dimension(:), allocatable :: num_delta
    integer, dimension(:,:), allocatable :: delta_ids
    real(P) :: u_ij, delta_u

    ! reset the trial potentials since not all are visited when using cell list
    system%trial%potential_pairs = 0.0_P
    nt = system%trial%num_trial ! number of trial particles
    n = system%num_particles ! number of particles

    allocate(particle_ids(nt))
    do i = 1, nt
       particle_ids(i) = system%trial%particle_ids(i)
    end do

    ! find the cell id from the particle data
    allocate(cell_ids(nt))
    allocate(neighbor_cell_ids(nt))
    do i = 1, nt
       cell_ids(i) = which_cell(system,system%trial%particle(i)%position)
    end do

    allocate(num_delta(nt))
    allocate(delta_ids(n,nt))
    num_delta = 0
    delta_ids = 0

    delta_u = 0.0_P

    do i = 1, nt
       ! loop over neighbor cells (includes itself)
       do cell_counter = 1, 27
          ! retrieve neighbor cell id
          neighbor_cell_ids(i) = system%cell_list%neighbors(cell_counter,cell_ids(i))
          ! loop through particles in the chosen cell
          do local_id = 1, system%cell_list%cell_count(neighbor_cell_ids(i))
             ! find its identity from the map
             neighbor_id = system%cell_list%cell_map(local_id,neighbor_cell_ids(i))
             ! skip i-i interaction
             if( neighbor_id .ne. particle_ids(i) ) then
                ! note that this pair was evaluated
                num_delta(i) = num_delta(i) + 1
                delta_ids(num_delta(i),i) = neighbor_id
                ! check if other particle is also a trial particle
                if( any(neighbor_id .eq. particle_ids) ) then
                   j = minloc(abs(particle_ids - neighbor_id))
                   u_ij = pair_potential(system,potentials,system%trial%particle(i), &
                        system%trial%particle(j(1)))
                else
                   ! compute pair potentials
                   u_ij = pair_potential(system,potentials,system%trial%particle(i), &
                        system%particles%particle(neighbor_id))
                end if
             else
                u_ij = 0.0_P
             end if
             ! assign to memory
             system%trial%potential_pairs(neighbor_id,i) = u_ij
             !system%trial%potential_pairs(neighbor_id,particle_ids(i)) = u_ij
             ! remove the old value and add the new value
             delta_u = delta_u + u_ij - system%potentials%potential_pair(neighbor_id,particle_ids(i))
          end do
       end do
    end do

    ! apply the change to the total(s)
    system%potentials%tot_potential      = system%potentials%tot_potential      + delta_u
    system%potentials%tot_potential_pair = system%potentials%tot_potential_pair + delta_u

    system%trial%num_delta(1:nt) = num_delta(1:nt)
    system%trial%delta_ids(:,1:nt) = delta_ids(:,1:nt)

  end subroutine calculate_trial_pair_potentials_cell_list

  !=======================================================================!
  !     compute all pair potentials involving the last moved particle     !
  !=======================================================================!
  subroutine calculate_trial_pair_potentials(system,potentials)
    implicit none
    ! external variables
    type (system_struct),     intent(inout) :: system
    type (potentials_struct), intent(inout) :: potentials
    ! internal variables
    integer, dimension(:), allocatable :: particle_ids
    integer :: neighbor_id, i, nt, n
    integer, dimension(1) :: j
    integer, dimension(:), allocatable  :: num_delta
    integer, dimension(:,:),allocatable :: delta_ids
    real(P) :: u_ij, delta_u

    nt = system%trial%num_trial ! number of trial particles
    n = system%num_particles ! number of particles

    allocate(num_delta(nt))
    allocate(delta_ids(n,nt))
    num_delta = 0
    delta_ids = 0

    delta_u = 0.0_P

    allocate(particle_ids(nt))
    do i = 1, nt
       particle_ids(i) = system%trial%particle_ids(i)
    end do

    do i = 1, nt
       ! loop over all other particles
       do neighbor_id = 1, system%num_particles
          ! skip i-i interaction
          if( neighbor_id .ne. particle_ids(i) ) then
             ! note that this pair was evaluated
             num_delta(i) = num_delta(i) + 1
             delta_ids(num_delta(i),i) = neighbor_id
             ! check if other particle is also a trial particle
             if( any(neighbor_id .eq. particle_ids) ) then
                j = minloc(abs(particle_ids - neighbor_id))
                u_ij = pair_potential(system,potentials,system%trial%particle(i), &
                     system%trial%particle(j(1)))
             else
                ! compute pair potentials
                u_ij = pair_potential(system,potentials,system%trial%particle(i), &
                     system%particles%particle(neighbor_id))
             end if
          else
             u_ij = 0.0_P
          end if
          ! assign to memory
          system%trial%potential_pairs(neighbor_id,i) = u_ij
          !system%trial%potential_pairs(neighbor_id,particle_ids(i)) = u_ij
          ! remove the old value and add the new value
          delta_u = delta_u + u_ij - system%potentials%potential_pair(neighbor_id,particle_ids(i))
       end do
    end do

    ! apply the change to the total(s)
    system%potentials%tot_potential      = system%potentials%tot_potential      + delta_u
    system%potentials%tot_potential_pair = system%potentials%tot_potential_pair + delta_u

    system%trial%num_delta(1:nt) = num_delta(1:nt)
    system%trial%delta_ids(:,1:nt) = delta_ids(:,1:nt)

  end subroutine calculate_trial_pair_potentials

  !==========================================!
  !     compute all restraint potentials     !
  !==========================================!
  subroutine calculate_all_restraint_potentials(system,potentials)
    implicit none
    ! external variables
    type (system_struct),     intent(inout) :: system
    type (potentials_struct), intent(inout) :: potentials
    ! internal variables
    integer :: i

    ! reference translation field
    if( potentials%restrain_tr%enabled ) then

       ! loop through reference positions
       do i = 1, system%num_particles
          if( potentials%restrain_tr%enabled ) &
               system%potentials%potential_restrain_tr(i) = &
               eval_restrain_tr(potentials,system%particles%particle(i)%position(:), &
               potentials%restrain_tr%ref(:,i))
       end do

       system%potentials%tot_potential_restrain_tr = sum( system%potentials%potential_restrain_tr )
    else
       system%potentials%tot_potential_restrain_tr = 0.0_P
    end if

    ! reference orientation field
    if( potentials%restrain_or%enabled ) then

       ! loop through reference orientations
       do i = 1, system%num_particles
          if( potentials%restrain_or%enabled ) &
               system%potentials%potential_restrain_or(i) = &
               eval_restrain_or(potentials,system%particles%particle(i)%orientation(:,:), &
               potentials%restrain_or%ref(:,:,i))
       end do

       system%potentials%tot_potential_restrain_or = sum( system%potentials%potential_restrain_or )
    else
       system%potentials%tot_potential_restrain_or = 0.0_P
    end if

    ! re-sum
    system%potentials%tot_potential = &
         system%potentials%tot_potential_pair + &
         system%potentials%tot_potential_restrain_tr + &
         system%potentials%tot_potential_restrain_or + &
         system%potentials%tot_potential_bias

  end subroutine calculate_all_restraint_potentials

  !========================================================================!
  !     compute restraint potentials involving the last moved particle     !
  !========================================================================!
  subroutine calculate_trial_restraint_potentials(system,potentials)
    implicit none
    ! external variables
    type (system_struct),     intent(inout) :: system
    type (potentials_struct), intent(inout) :: potentials
    ! internal variables
    integer, dimension(:), allocatable :: particle_ids
    integer :: nt, i
    real(P) :: u_ij, delta_u

    nt = system%trial%num_trial

    allocate(particle_ids(nt))
    do i = 1, nt
       particle_ids(i) = system%trial%particle_ids(i)
    end do

    ! reference translation field
    if( potentials%restrain_tr%enabled ) then
       do i = 1, nt
          ! only change the restraint potential for the last moved particle(s)
          u_ij = eval_restrain_tr(potentials,system%trial%particle(i)%position, &
               potentials%restrain_tr%ref(:,particle_ids(i)))
          system%trial%potential_restrain_tr = u_ij

          ! calculate the difference
          delta_u = u_ij - system%potentials%potential_restrain_tr(particle_ids(i))

          ! apply the change to the total(s)
          system%potentials%tot_potential = system%potentials%tot_potential + delta_u
          system%potentials%tot_potential_restrain_tr = system%potentials%tot_potential_restrain_tr + delta_u
       end do
    end if

    ! reference orientation field
    if( potentials%restrain_or%enabled ) then
       do i = 1, nt
          ! only change the restraint potential for the last moved particle
          u_ij = eval_restrain_or(potentials,system%trial%particle(i)%orientation, &
               potentials%restrain_or%ref(:,:,particle_ids(i)))
          system%trial%potential_restrain_or = u_ij

          ! calculate the difference
          delta_u = u_ij - system%potentials%potential_restrain_or(particle_ids(i))

          ! apply the change to the total(s)
          system%potentials%tot_potential = system%potentials%tot_potential + delta_u
          system%potentials%tot_potential_restrain_or = system%potentials%tot_potential_restrain_or + delta_u
       end do
    end if

  end subroutine calculate_trial_restraint_potentials

  !=====================================!
  !     compute all bias potentials     !
  !=====================================!
  subroutine calculate_all_bias_potentials(system,potentials)
    implicit none
    ! external variables
    type (system_struct),     intent(inout) :: system
    type (potentials_struct), intent(inout) :: potentials

    ! compute
    if( potentials%bias%enabled ) then
       system%potentials%tot_potential_bias = eval_bias(system,potentials)
    else
       system%potentials%tot_potential_bias = 0.0_P
    end if

    ! re-sum
    system%potentials%tot_potential = &
         system%potentials%tot_potential_pair + &
         system%potentials%tot_potential_restrain_tr + &
         system%potentials%tot_potential_restrain_or + &
         system%potentials%tot_potential_bias

  end subroutine calculate_all_bias_potentials

  !===================================================================!
  !     compute bias potentials involving the last moved particle     !
  !===================================================================!
  subroutine calculate_trial_bias_potentials(system,potentials)
    implicit none
    ! external variables
    type (system_struct),     intent(inout) :: system
    type (potentials_struct), intent(inout) :: potentials
    ! internal variables
    real(P) :: delta_u

    ! compute only
    if( potentials%bias%enabled ) then
       delta_u = eval_bias(system,potentials) - system%potentials%tot_potential_bias
       system%potentials%tot_potential_bias = system%potentials%tot_potential_bias + delta_u
       system%potentials%tot_potential      = system%potentials%tot_potential      + delta_u
    else
       system%potentials%tot_potential_bias = 0.0_P
    end if

  end subroutine calculate_trial_bias_potentials

  !============================================!
  !     driver for all the pair potentials     !
  !============================================!
  real(P) function pair_potential(system,potentials,particle_i,particle_j)
    implicit none
    ! external variables
    type (system_struct),     intent(inout) :: system
    type (potentials_struct), intent(inout) :: potentials
    type (particle_data_struct), intent(inout) :: particle_i, particle_j
    ! internal variables
    real(P) :: kf, ckf, lj, wca, hs, r_ij_length
    real(P), dimension(3) :: r_ij

    r_ij = displacement(system,particle_i%position,particle_j%position)
    r_ij_length = sqrt(sum(r_ij**2.0_P))

    pair_potential = 0.0_P

    if( potentials%kf%enabled ) then
       kf = eval_kf(potentials,particle_i,particle_j,r_ij,r_ij_length)
       pair_potential = pair_potential + kf
    end if

    if( potentials%ckf%enabled ) then
       ckf = eval_ckf(potentials,particle_i,particle_j,r_ij,r_ij_length)
       pair_potential = pair_potential + ckf
    end if

    if( potentials%lj%enabled ) then
       lj = eval_lj(potentials,particle_i,particle_j,r_ij_length)
       pair_potential = pair_potential + lj
    end if

    if( potentials%wca%enabled ) then
       wca = eval_wca(potentials,particle_i,particle_j,r_ij_length)
       pair_potential = pair_potential + wca
    end if

    if( potentials%hs%enabled ) then
       hs = eval_hs(potentials,particle_i,particle_j,r_ij_length)
       pair_potential = pair_potential + hs
    end if

  end function pair_potential

  !============================================!
  !     driver for all the pair potentials     !
  !============================================!
  real(P) function trial_pair_potential(system,potentials,i,j)
    implicit none
    ! external variables
    type (system_struct),     intent(inout) :: system
    type (potentials_struct), intent(inout) :: potentials
    integer, intent(in) :: i, j
    ! internal variables
    real(P), dimension(3) :: r_ij
    real(P) ::r_ij_length, kf, ckf, lj, wca, hs

    ! get displacement vector and distance
    r_ij = displacement(system,system%trial%all_particles(i)%position,system%trial%all_particles(j)%position)
    r_ij_length = sqrt( sum( r_ij**2.0_P ) )

    trial_pair_potential = 0.0_P

    if( potentials%kf%enabled ) then
       kf = eval_kf(potentials,system%trial%all_particles(i),system%trial%all_particles(j),r_ij,r_ij_length)
       trial_pair_potential = trial_pair_potential + kf
    end if

    if( potentials%ckf%enabled ) then
       ckf = eval_ckf(potentials,system%trial%all_particles(i),system%trial%all_particles(j),r_ij,r_ij_length)
       trial_pair_potential = trial_pair_potential + ckf
    end if

    if( potentials%lj%enabled ) then
       lj = eval_lj(potentials,system%trial%all_particles(i),system%trial%all_particles(j),r_ij_length)
       trial_pair_potential = trial_pair_potential + lj
    end if

    if( potentials%wca%enabled ) then
       wca = eval_wca(potentials,system%trial%all_particles(i),system%trial%all_particles(j),r_ij_length)
       trial_pair_potential = trial_pair_potential + wca
    end if

    if( potentials%hs%enabled ) then
       hs = eval_hs(potentials,system%trial%all_particles(i),system%trial%all_particles(j),r_ij_length)
       trial_pair_potential = trial_pair_potential + hs
    end if

  end function trial_pair_potential

  !====================================================================!
  !     driver for all the pair potentials evaluated by the logger     !
  !====================================================================!
  subroutine logger_pair_potential(system,potentials,i,j)
    implicit none
    ! external variables
    type (system_struct),     intent(inout) :: system
    type (potentials_struct), intent(inout) :: potentials
    integer, intent(in) :: i, j
    ! internal variables
    real(P), dimension(3) :: r_ij
    real(P) :: r_ij_length

    ! get displacement vector and distance
    r_ij = displacement(system,system%particles%particle(i)%position,system%particles%particle(j)%position)
    r_ij_length = sqrt( sum( r_ij**2.0_P ) )

    if( potentials%kf%log ) then
       potentials%kf%tot_potential = &
            potentials%kf%tot_potential &
            + eval_kf(potentials,system%particles%particle(i),system%particles%particle(j),r_ij,r_ij_length)
    end if

    if( potentials%ckf%log ) then
       potentials%ckf%tot_potential = &
            potentials%ckf%tot_potential &
            + eval_ckf(potentials,system%particles%particle(i),system%particles%particle(j),r_ij,r_ij_length)
    end if

    if( potentials%lj%log ) then
       potentials%lj%tot_potential = &
            potentials%lj%tot_potential &
            + eval_lj(potentials,system%particles%particle(i),system%particles%particle(j),r_ij_length)
    end if

    if( potentials%wca%log ) then
       potentials%wca%tot_potential = &
            potentials%wca%tot_potential &
            + eval_wca(potentials,system%particles%particle(i),system%particles%particle(j),r_ij_length)
    end if

    if( potentials%hs%log ) then
       potentials%hs%tot_potential = &
            potentials%hs%tot_potential &
            + eval_hs(potentials,system%particles%particle(i),system%particles%particle(j),r_ij_length)
    end if

  end subroutine logger_pair_potential

  !==========================================!
  !     compute all restraint potentials     !
  !==========================================!
  subroutine logger_restraint_potential(system,potentials)
    implicit none
    ! external variables
    type (system_struct),     intent(inout) :: system
    type (potentials_struct), intent(inout) :: potentials
    ! internal variables
    integer :: i

    ! reference translation field
    if( potentials%restrain_tr%log ) then
       ! loop through reference positions
       do i = 1, system%num_particles
          potentials%restrain_tr%tot_potential = &
               potentials%restrain_tr%tot_potential &
               + eval_restrain_tr(potentials,system%particles%particle(i)%position, &
               potentials%restrain_tr%ref(:,i))
       end do
    end if

    ! reference orientation field
    if( potentials%restrain_or%log ) then
       ! loop through reference orientations
       do i = 1, system%num_particles
          potentials%restrain_or%tot_potential = &
               potentials%restrain_or%tot_potential &
               + eval_restrain_or(potentials,system%particles%particle(i)%orientation, &
               potentials%restrain_or%ref(:,:,i))
       end do
    end if

  end subroutine logger_restraint_potential

end module compute
