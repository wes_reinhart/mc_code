module potential_pair_hs
  use globals
  use datatypes_potentials
  use datatypes
  implicit none

contains

  !=============================================!
  !     computes hard sphere pair potential     !
  !=============================================!
  real(P) function eval_hs(potentials,particle_i,particle_j,r_ij_length)
    implicit none
    ! external variables
    type (potentials_struct), intent(inout) :: potentials
    type (particle_data_struct), intent(inout) :: particle_i, particle_j
    real(P), intent(in) :: r_ij_length
    ! internal variables
    real(P) :: d_i, d_j
    real(P) :: cutoff

    d_i = particle_i%diameter
    d_j = particle_j%diameter

    ! calculate the radial cutoff from the two particle diameters
    cutoff = ( d_i + d_j ) / 2.0_P
    
    ! assume no interaction
    eval_hs = 0.0_P

    ! Weeks-Chandler-Andersen potential (huh?)
    ! is radial_cutoff the diameter?
    if( r_ij_length .le. cutoff ) then
       eval_hs = infinity
    end if

  end function eval_hs

end module potential_pair_hs
