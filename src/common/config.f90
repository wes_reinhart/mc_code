module config
  use globals
  use datatypes
  use util_mc
  use util_rand
  use util_math
  implicit none

  !==========================================================================!
  !     set up a data structure for the initial configuration generation     !
  !==========================================================================!
  type :: lattice_struct
     integer :: num_sites                 ! total number of lattice sites
     integer :: num_available_sites       ! number of currently available sites
     integer :: num_stencil               ! size of the stencil
     real(P), dimension(3) :: delta       ! lattice spacing in (x,y,z)
     integer, dimension(3) :: dimensions  ! number of lattice sites in (x,y,z)
     integer, dimension(3) :: stencil_dim ! number of stencil sites in (x,y,z)
     integer, dimension(:),   allocatable :: map       ! list of sites with availability
     integer, dimension(:,:), allocatable :: stencil   ! which surrounding sites are occupied by a particle, in 3D
     integer, dimension(:),   allocatable :: neighbors ! list of the site id's for the stencil, in 1D
  end type lattice_struct

contains

  !================================!
  !     read a config from xml     !
  !================================!
  subroutine read_context_xml(context)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    ! internal variables
    integer :: fid, ierr
    character(len=1000) :: fline
    integer :: i, j, s, e, natoms, npatches, max_patches
    real(P) :: temp_float
    logical :: orientations, patches, diameters
    real(P), dimension(4) :: q

    ! assign a file ID that has not been used
    fid = last_fid
    last_fid = last_fid + 1

    ! open the file
    open(unit=fid,file=trim(context%parameters%config_file),action="read",status="old")

    ! find the configuration tag
    fline = ""
    ierr = 0
    do while( index(fline,"<configuration") .eq. 0 .and. ierr .eq. 0 )
       read(fid,'(A)',iostat=ierr) fline
    end do
    if( ierr .ne. 0 ) then
       write(*,'("Error (",I0,"): End of file while searching for <configuration> data")') line_number
       stop
    end if

    ! identify natoms
    s = index(fline,'natoms="') + 8
    e = index(fline(s:len(fline)),'"') + s - 2
    read(fline(s:e),*) temp_float
    natoms = int(temp_float)

    ! identify npatches ( per particle )
    s = index(fline,'npatches="')
    if( s .gt. 0 ) then
       s = s + 10
       e = index(fline(s:len(fline)),'"') + s - 2
       read(fline(s:e),*) temp_float
       npatches = int(temp_float)
    else
       npatches = 0
    end if

    write(*,'("natoms = ",I0,", npatches = ",I0)') natoms, npatches

    ! find the box tag
    fline = ""
    do while( index(fline,"<box") .eq. 0 .and. ierr .eq. 0 )
       read(fid,'(A)',iostat=ierr) fline
    end do
    if( ierr .ne. 0 ) then
       write(*,'("Error (",I0,"): End of file while searching for <box> data")') line_number
       stop
    end if

    ! identify box length
    context%system%box_length = 0.0_P
    s = 0
    e = -1
    do i = 1, 3
       s = e + 2
       s = scan(fline(s:len(fline)),'"') + s
       e = scan(fline(s:len(fline)),'"') - 2 + s
       read(fline(s:e),*) context%system%box_length(i)
    end do

    ! go to start of position tag
    fline = ""
    ierr = 0
    do while( index(fline,"<position") .eq. 0 .and. ierr .eq. 0 )
       read(fid,'(A)') fline
    end do
    if( ierr .ne. 0 ) then
       write(*,'("Error (",I0,"): End of file while searching for <position> data")') line_number
       stop
    end if

    context%system%num_particles = natoms / (npatches + 1)

    ! print information to command line
    write(*,'(A,A)') "Read the specified file: ", trim(context%parameters%config_file)
    write(*,'("N = ",I0,"; L = (",F10.5,",",F10.5,",",F10.5,")")') &
         context%system%num_particles, context%system%box_length

    ! allocate memory for the particle data within the context struct
    call initialize_system(context%system)

    ! read each particle's position
    do i = 1, context%system%num_particles
       read(fid,'(A)',iostat=ierr) fline
       read(fline,*) context%system%particles%particle(i)%position
       ! ignore VMD patch data
       do j = 1, npatches
          read(fid,'(A)',iostat=ierr) fline
       end do
    end do
    if( ierr .eq. 0 ) then
       write(*,'("Read ",I0," <position> entries")') context%system%num_particles
    else
       write(*,'("I/O error while reading positions!")')
       stop
    end if

    max_patches = 0
    do i = 1, context%system%num_particles
       if(context%system%particles%num_patches .gt. max_patches) &
            max_patches = context%system%particles%num_patches
    end do

    if( max_patches .gt. 0 ) then

       ! check whether patch facings are written explicitly
       rewind(fid)
       fline = ""
       ierr = 0
       do while( index(fline,"<patch") .eq. 0 .and. ierr .eq. 0 )
          read(fid,'(A)',iostat=ierr) fline
       end do
       if( ierr .ne. 0 ) then
          patches = .false.
       else
          patches = .true.
          write(*,'("Note (",I0,"): reading facings from <patch> tag")') line_number
       end if

       if( patches ) then

          ! patch facings always supercede quaternions
          !   read patch facings from file
          do i = 1, context%system%num_particles
             do j = 1, context%system%particles%num_patches
                read(fid,'(A)',iostat=ierr) fline
                read(fline,*) context%system%particles%particle(i)%orientation(:,j)
             end do
          end do

       else

          ! check whether orientations are provided in quaternion form
          rewind(fid)
          fline = ""
          ierr = 0
          do while( index(fline,"<orientation") .eq. 0 .and. ierr .eq. 0 )
             read(fid,'(A)',iostat=ierr) fline
          end do
          if( ierr .ne. 0 ) then
             orientations = .false.
          else
             orientations = .true.
             write(*,'("Note (",I0,"): generating facings from <orientation> tag")') line_number
          end if

          if( orientations ) then

             ! WFR: only works for up to 2 patches for now!
             if( max_patches .gt. 2 ) then
                write(*,'("Error (",I0,"): conversion of quaternions to more than &
                     two patches is not currently supported")') line_number
                stop
             end if

             ! read orientations from quaternions
             do i = 1, context%system%num_particles
                read(fid,'(A)',iostat=ierr) fline
                read(fline,*) q
                context%system%particles%particle(i)%orientation(:,1) = quat_to_vec(q)
                if( context%system%particles%num_patches .eq. 2 ) then
                   context%system%particles%particle(i)%orientation(:,2) = -quat_to_vec(q)
                end if
             end do

          else

             ! no orientation of any kind given,
             !   generate random patch facings
             write(*,'("Note (",I0,"): no orientation data found, generating randomly")') line_number
             do i = 1, context%system%num_particles
                call generate_patches(context,i)
             end do

          end if

       end if ! if( patches )

    end if ! if( num_patches .gt. 0 )

    ! read particle diameters, if provided
    rewind(fid)
    fline = ""
    ierr = 0
    do while( index(fline,"<diameter") .eq. 0 .and. ierr .eq. 0 )
       read(fid,'(A)',iostat=ierr) fline
    end do
    if( ierr .ne. 0 ) then
       write(*,'("<diameter> tag not found")')
       diameters = .false.
       ! generate them randomly
       ! calculate random diameter from a normal distribution for each particle
       do i = 1, context%system%num_particles
          context%system%particles%particle(i)%diameter = random_normal(context%system%particles%diameter_mean_var)
       end do
    else

       diameters = .true.

       if( context%system%particles%diameter_mean_var(1) .ne. 1.0_P &
            .or. context%system%particles%diameter_mean_var(2) .ne. 0.0_P ) then

          ! warn the user that their input is ignored
          print*,(context%system%particles%diameter_mean_var(1) .ne. 1.0_P),&
               (context%system%particles%diameter_mean_var(2) .ne. 0.0_P)
          print*,context%system%particles%diameter_mean_var(2)
          write(*,'("Found <diameter> tag in XML, but overwriting with new random diameters.")')
          ! calculate random diameter from a normal distribution for each particle
          do i = 1, context%system%num_particles
             context%system%particles%particle(i)%diameter = &
                  random_normal(context%system%particles%diameter_mean_var)
          end do

       else

          ! read them from file
          do i = 1, context%system%num_particles
             read(fid,'(A)',iostat=ierr) fline
             read(fline,*) context%system%particles%particle(i)%diameter
             ! ignore VMD patch data
             if( patches ) then
                do j = 1, npatches
                   read(fid,'(A)',iostat=ierr) fline
                end do
             end if
          end do

          if( ierr .eq. 0 ) then
             write(*,'("Read ",I0," <diameter> entries")') context%system%num_particles
          else
             write(*,'("I/O error while reading diameters!")')
             stop
          end if
       end if
    end if

    ! close the file
    close(fid)

  end subroutine read_context_xml

  !================================================!
  !     allocate memory for context definition     !
  !================================================!
  subroutine initialize_system(system)
    implicit none
    ! external variables
    type (system_struct), intent(inout) :: system
    ! internal variables
    integer :: npatches, nparticles, i, j, rand_value
    real(P) :: diameter_i

    ! reset the system (only deallocates if it was already allocated)
    call destroy_system(system)

    nparticles = system%num_particles

    ! set up context%system%particles
    allocate( system%particles%particle(nparticles) )
    do j = 1, nparticles
       npatches = system%particles%num_patches
       if( npatches .gt. 0 ) &
            allocate( system%particles%particle(j)%orientation(3,npatches) )
    end do
    ! calculate random diameter from a normal distribution for each particle
    do i = 1, nparticles
       diameter_i = random_normal(system%particles%diameter_mean_var)
       system%particles%particle(i)%diameter = diameter_i
    end do

  end subroutine initialize_system

  !===================================================!
  !     deallocate memory from context definition     !
  !===================================================!
  subroutine destroy_system(system)
    implicit none
    ! external variables
    type (system_struct), intent(inout) :: system
    ! internal variables
    integer :: i, j
    ! deallocate particles data
    ! not really sure how to safely deallocate the array of particle types...
    ! (do you need to deallocate derived types?)
    if( allocated(system%particles%particle) ) then
       do i = 1, size(system%particles%particle)
          if( allocated(system%particles%particle(i)%orientation) ) &
               deallocate(system%particles%particle(i)%orientation)
       end do
       deallocate(system%particles%particle)
    end if
    if( allocated(system%potentials%potential_pair) ) deallocate(system%potentials%potential_pair)
    if( allocated(system%potentials%potential_restrain_tr) ) deallocate(system%potentials%potential_restrain_tr)
    if( allocated(system%potentials%potential_restrain_or) ) deallocate(system%potentials%potential_restrain_or)
    ! deallocate cell list data
    if( allocated(system%cell_list%cell_map) )    deallocate(system%cell_list%cell_map)
    if( allocated(system%cell_list%cell_count) )  deallocate(system%cell_list%cell_count)
    if( allocated(system%cell_list%neighbors) )   deallocate(system%cell_list%neighbors)

    if( allocated(system%trial%particle) )  then
       do j = 1, size(system%trial%particle)
          if( allocated(system%trial%particle(j)%orientation) ) &
               deallocate(system%trial%particle(j)%orientation)
       end do
       deallocate(system%trial%particle)
    end if
  end subroutine destroy_system

  !==========================================!
  !     calculate the move probabilities     !
  !==========================================!
  ! need to change to reflect changed trial struct
  subroutine initialize_runtime(context)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    ! internal variables
    integer :: i, n, np, nt
    real(P) :: s

    n = context%system%num_particles
    nt = 0
    do i = 1, size(context%moves)
       print*,'initialize_runtime:',i,nt
       if(context%moves(i)%trial_particles .gt. nt) nt = context%moves(i)%trial_particles
    end do ! maximum number of trial particles
    print*,'nt = ',nt
    np = context%system%particles%num_patches

    print*,'allocating trial particle_ids'
    ! allocate trial data
    if( allocated(context%system%trial%particle_ids) ) deallocate(context%system%trial%particle_ids)
    allocate(context%system%trial%particle_ids(nt))
    print*,'allocating trial delta_ids'
    if( allocated(context%system%trial%delta_ids) ) deallocate(context%system%trial%delta_ids)
    allocate(context%system%trial%delta_ids(n,nt)) ! fast dimension is first dimension
    if( allocated(context%system%trial%num_delta) ) deallocate(context%system%trial%num_delta)
    allocate(context%system%trial%num_delta(nt))
    print*,'deallocating particles'
    ! deallocate/allocate particles
    if( allocated(context%system%trial%particle) ) deallocate(context%system%trial%particle)
    print*,'deallocated; now allocating',nt
    allocate(context%system%trial%particle(nt))
    print*,'allocating particle orientations'
    do i = 1, nt
       allocate(context%system%trial%particle(i)%orientation(3,np))
    end do
    print*,'allocating potential_pairs'
    if( allocated(context%system%trial%potential_pairs) ) deallocate(context%system%trial%potential_pairs)
    allocate(context%system%trial%potential_pairs(n,nt)) ! first dimension is faster
    if( allocated(context%system%trial%potential_restrain_tr) ) deallocate(context%system%trial%potential_restrain_tr)
    allocate(context%system%trial%potential_restrain_tr(nt))
    if( allocated(context%system%trial%potential_restrain_or) ) deallocate(context%system%trial%potential_restrain_or)
    allocate(context%system%trial%potential_restrain_or(nt))

    if( allocated(context%system%trial%all_particles) ) then
       do i = 1, size(context%system%trial%all_particles)
          if( allocated(context%system%trial%all_particles(i)%orientation) ) &
               deallocate(context%system%trial%all_particles(i)%orientation)
       end do
       deallocate(context%system%trial%all_particles)
    end if
    allocate(context%system%trial%all_particles(n))
    do i = 1, n
       allocate(context%system%trial%all_particles(i)%orientation(3,np))
    end do
    !if( allocated(context%system%trial%all_position) ) deallocate(context%system%trial%all_position)
    !allocate(context%system%trial%all_position(3,n))
    !if( allocated(context%system%trial%all_orientation) ) deallocate(context%system%trial%all_orientation)
    !allocate(context%system%trial%all_orientation(3,np,n))
    !if( allocated(context%system%trial%all_diameter) ) deallocate(context%system%trial%all_diameter)
    !allocate(context%system%trial%all_diameter(3,n))
    if( allocated(context%system%trial%all_potential_pair) ) deallocate(context%system%trial%all_potential_pair)
    allocate(context%system%trial%all_potential_pair(n,n))

    if( context%potentials%restrain_tr%enabled .or. context%potentials%restrain_tr%log ) then
       if( allocated(context%system%trial%all_potential_restrain_tr) ) deallocate(context%system%trial%all_potential_restrain_tr)
       allocate(context%system%trial%all_potential_restrain_tr(n))
    end if

    if( context%potentials%restrain_or%enabled .or. context%potentials%restrain_or%log ) then
       if( allocated(context%system%trial%all_potential_restrain_or) ) deallocate(context%system%trial%all_potential_restrain_or)
       allocate(context%system%trial%all_potential_restrain_or(n))
    end if

    if( allocated(context%system%potentials%potential_pair) ) deallocate(context%system%potentials%potential_pair)
    allocate(context%system%potentials%potential_pair(n,n))
    if( context%potentials%restrain_tr%enabled .or. context%potentials%restrain_tr%log ) then
       if( allocated(context%system%potentials%potential_restrain_tr) ) &
            deallocate(context%system%potentials%potential_restrain_tr)
       allocate(context%system%potentials%potential_restrain_tr(n))
       if( allocated(context%potentials%restrain_tr%ref) ) &
            deallocate(context%potentials%restrain_tr%ref)
       allocate(context%potentials%restrain_tr%ref(3,n))
       do i = 1, n
          context%potentials%restrain_tr%ref(:,i) = context%system%particles%particle(i)%position
       end do
    end if

    if( context%potentials%restrain_or%enabled .or. context%potentials%restrain_or%log ) then
       if( allocated(context%system%potentials%potential_restrain_or) ) &
            deallocate(context%system%potentials%potential_restrain_or)
       allocate(context%system%potentials%potential_restrain_or(n))
       if( allocated(context%potentials%restrain_or%ref) ) &
            deallocate(context%potentials%restrain_or%ref)
       allocate(context%potentials%restrain_or%ref(3,np,n))
       do i = 1, n
          context%potentials%restrain_or%ref(:,:,i) = context%system%particles%particle(i)%orientation
       end do
    end if

    if( allocated(context%moves) ) then
       if( allocated(context%system%move_prob) ) deallocate(context%system%move_prob)
       allocate(context%system%move_prob(size(context%moves)))
       s = sum(context%moves(:)%frequency)
       context%system%move_prob(1) = context%moves(1)%frequency / s
       do i = 2, size(context%moves)
          context%system%move_prob(i) = context%system%move_prob(i-1) &
               + context%moves(i)%frequency / s
       end do
    else
       write(*,'("Error (",I0,"): cannot run mc without any trial moves")') line_number
       stop
    end if

  end subroutine initialize_runtime

  !================================================================!
  !     generate random configuration with on-lattice approach     !
  !================================================================!
  subroutine generate_lattice_config(context)
    implicit none
    ! external variables
    type (context_struct),    intent(inout) :: context
    ! internal variables
    type (lattice_struct) :: lattice
    integer :: j
    real(P) :: a
    integer :: particle_id, site_id, map_entry, num_removed
    logical :: full_lattice
    integer :: site_counter, map_counter
    integer, dimension(3) :: coord
    integer :: map_min

    write(*,'("Generating random configuration with ",I0," particles in box of size ",3F10.5)') &
         context%system%num_particles, context%system%box_length

    ! create the context data
    call initialize_system(context%system)

    ! create the lattice
    call initialize_lattice(context,lattice)

    full_lattice = .false.
    particle_id = 1
    ! loop through all particles we want to place
    do while( particle_id .le. context%system%num_particles )

       ! get the shape of the stencil in 3D, centered on the origin
       call lattice_stencil(context%system%particles%particle(particle_id)%diameter,lattice)

       if( .not. full_lattice ) then
          ! pick from from available sites
          call random_number(a)
          map_entry = floor( a * lattice%num_available_sites ) + 1

          ! obtain the site id from the list of available ones
          site_counter = 0
          map_counter  = 0
          do while( site_counter .lt. map_entry )
             map_counter = map_counter + 1
             if( lattice%map(map_counter) .gt. 0 ) then
                site_counter = site_counter + 1
             end if
          end do

          ! take the site id we just located
          site_id = lattice%map(map_counter)
          ! remove from list of available sites
          lattice%map(map_counter) = 0
          num_removed = 1

       else ! not sure what the point of this is...

          map_min = minval(lattice%map)
          j = 1
          do while( j .le. lattice%num_sites .and. lattice%map(j) .ne. map_min )
             j = j + 1
          end do

          site_id = j
          map_counter = j

          lattice%map(map_counter) = 0

          num_removed = 0

       end if

       ! convert 1D site id to 3D coord
       coord = cell_id_to_coord(site_id,lattice%dimensions)

       ! convert the coordinates to system spec and assign them
       ! does coord start at 0? If so then need to think about this 0.5 addition, will
       ! have to change with dispersity in diameters perhaps not acutally just need to
       ! make sure that lattice_delta > diameter
       ! minus half the box length because origin is in the center of the box?
       context%system%particles%particle(particle_id)%position(:) = (real(coord,P)+0.5_P) * lattice%delta &
            - ( 0.5_P * context%system%box_length )

       ! assign patch geometry
       call generate_patches(context,particle_id)

       ! convert 3D stencil to 1D neighbor list
       call lattice_neighbors(lattice,coord)

       ! remove each element of the stencil from the map of available sites
       do j = 1, lattice%num_stencil ! lattice%num_available_sites
          if( lattice%map(lattice%neighbors(j)) .gt. 0 ) then
             lattice%map(lattice%neighbors(j)) = -lattice%stencil(4,j) ! negative dist from origin. huh?
             num_removed = num_removed + 1
          else if( lattice%map(lattice%neighbors(j)) .lt. -lattice%stencil(4,j) ) then
             lattice%map(lattice%neighbors(j)) =  -lattice%stencil(4,j)
          end if
       end do
       ! update number of available sites
       lattice%num_available_sites = lattice%num_available_sites - num_removed
       ! check that we are allowed to continue
       if( lattice%num_available_sites .le. 0 .and. .not. full_lattice ) then
          write(*,'("there are no more empty lattice sites after ",I0," were assigned")') particle_id-1
          write(*,'(I0," particles will be placed in least crowded sites")') &
               context%system%num_particles-particle_id+1
          ! quit the loop and guess positions for the last few particles
          full_lattice = .true.
       end if

       particle_id = particle_id + 1

    end do

  end subroutine generate_lattice_config

  !===============================================!
  !     initialize the lattice data structure     !
  !===============================================!
  subroutine initialize_lattice(context,lattice)
    implicit none
    ! external variables
    type (context_struct),   intent(inout) :: context
    type (lattice_struct),   intent(inout) :: lattice
    ! internal variables
    integer, dimension(3) :: stencil_dim
    integer :: i
    real(P) :: max_diameter
    real(P), dimension(:), allocatable :: diameters
    ! get integer number of sites to use for the lattice
    lattice%dimensions = nint( context%system%box_length / &
         context%system%nominal_lattice_spacing )
    ! get true lattice spacing
    lattice%delta = context%system%box_length / real(lattice%dimensions, P)
    ! assign number of sites
    lattice%num_sites = product( lattice%dimensions )
    lattice%num_available_sites = lattice%num_sites
    ! allocate memory for the map
    allocate( lattice%map( lattice%num_sites ) )
    ! intialize the map
    do i = 1, lattice%num_sites
       lattice%map(i) = i
    end do
    ! allocate memory for the 3D stencil
    ! allocate memory for the diameters
    allocate(diameters(size(context%system%particles%particle)))
    ! does this need to change if the diameter changes?
    do i = 1, context%system%num_particles
       diameters(i) = context%system%particles%particle(i)%diameter
    end do
    max_diameter = maxval(diameters)
    stencil_dim = ceiling( 2.0*max_diameter / lattice%delta )
    ! stencil dim is like maximum size, just need twice max diameter of system
    ! make sure there are an odd number so the origin is centered
    where( mod(stencil_dim,2) .eq. 0 )
       stencil_dim = stencil_dim + 1
    end where
    lattice%stencil_dim = stencil_dim
    lattice%num_stencil = product( stencil_dim )
    allocate( lattice%stencil(4,lattice%num_stencil) )
    ! allocate memory for the 1D neighbor list
    allocate( lattice%neighbors(lattice%num_stencil) )
  end subroutine initialize_lattice

  !=============================================================!
  !     compute which lattice sites are part of the stencil     !
  !=============================================================!
  subroutine lattice_stencil(diameter,lattice)
    implicit none
    ! external variables
    real(P), intent (in) :: diameter
    type (lattice_struct),   intent(inout) :: lattice
    ! internal variables
    real(P), dimension(3) :: vec_to_origin
    real(P) :: dist_to_origin, cutoff
    integer, dimension(3) :: halfdim
    integer :: i, j, k, stencil_count

    cutoff = diameter/2.0_P ! need to change to diameter

    stencil_count = 0
    halfdim = (lattice%stencil_dim-1)/2
    if( any( halfdim*2+1 .ne. lattice%stencil_dim ) ) then
       write(*,'("mesh dimensions are not odd: ",I0,",",I0,",",I0)') lattice%stencil_dim
    end if
    do i = -halfdim(1), halfdim(1)
       do j = -halfdim(2), halfdim(2)
          do k = -halfdim(3), halfdim(3)
             vec_to_origin = real((/i,j,k/),P) * lattice%delta
             dist_to_origin = sqrt( sum( vec_to_origin**2.0_P ) )
             if( dist_to_origin .le. cutoff ) then
                stencil_count = stencil_count + 1
                lattice%stencil(:,stencil_count) = (/i,j,k,(i**2+j**2+k**2)/)
             end if
          end do
       end do
    end do
    lattice%num_stencil = stencil_count

  end subroutine lattice_stencil

  !========================================================!
  !     find the site id's of the sites in the stencil     !
  !========================================================!
  subroutine lattice_neighbors(lattice,site_coord)
    implicit none
    ! external variables
    type (lattice_struct),   intent(inout) :: lattice
    integer, dimension(3),   intent(inout) :: site_coord
    ! internal variables
    integer, dimension(3) :: neighbor_coord
    integer :: i

    ! reset neighbor list
    lattice%neighbors = 0

    ! loop through sites included in the stencil
    do i = 1, lattice%num_stencil
       ! shift the origin of the stencil coord to the site coord
       neighbor_coord = lattice%stencil(1:3,i) + site_coord
       ! wrap the result ! (what does this mean?) (why woudld you do this if its not
       ! the edge of the box? doesnt do anything if not on the edge of a box
       neighbor_coord = wrap_cell_coord(neighbor_coord,lattice%dimensions)
       ! get the corresponding id and assign it to the neighbor list
       lattice%neighbors(i) = cell_coord_to_id(neighbor_coord,lattice%dimensions)
    end do

  end subroutine lattice_neighbors

  !=================================!
  !     generate patch geometry     !
  !=================================!
  subroutine generate_patches(context,particle_id)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    integer, intent(in) :: particle_id
    ! internal variables
    real(P) :: angle
    integer :: i
    real(P), dimension(3) :: direction
    real(P), dimension(3,3) :: r_mat
    ! pick random rotation direction and angle
    call random_sphere(direction)
    call random_number(angle)
    angle = ( angle - 0.5_P ) * 2.0 * pi
    r_mat = rotation_matrix(direction,angle)
    ! hard-coded particle geometries (could easily change this to an input)
    do i = 1, context%system%num_particles
       if( context%system%particles%num_patches .eq. 1 ) then
          ! single pole
          context%system%particles%particle(i)%orientation(:,1) = normalize( &
               matmul( (/0.0_P,1.0_P,0.0_P/), r_mat ) )
       else if( context%system%particles%num_patches .eq. 2 ) then
          ! opposite poles
          context%system%particles%particle(i)%orientation(:,1) = normalize( &
               matmul( (/0.0_P,1.0_P,0.0_P/), r_mat ) )
          context%system%particles%particle(i)%orientation(:,2) = normalize( &
               matmul( (/0.0_P,-1.0_P,0.0_P/), r_mat ) )
       else if( context%system%particles%num_patches .eq. 3 ) then
          ! trigonal
          context%system%particles%particle(i)%orientation(:,1) = normalize( &
               matmul( (/0.0_P,1.0_P,0.0_P/), r_mat ) )
          context%system%particles%particle(i)%orientation(:,2) = normalize( &
               matmul( (/sqrt(3.0_P)/2.0_P,-0.5_P,0.0_P/), r_mat ) )
          context%system%particles%particle(i)%orientation(:,3) = normalize( &
               matmul( (/-sqrt(3.0_P)/2.0_P,-0.5_P,0.0_P/), r_mat ) )
       else if( context%system%particles%num_patches .eq. 4 ) then
          ! tetrahedral
          context%system%particles%particle(i)%orientation(:,1) = normalize( &
               matmul( (/1.0_P,0.0_P,-1.0_P/sqrt(2.0_P)/), r_mat ) )
          context%system%particles%particle(i)%orientation(:,2) = normalize( &
               matmul( (/-1.0_P,0.0_P,-1.0_P/sqrt(2.0_P)/), r_mat ) )
          context%system%particles%particle(i)%orientation(:,3) = normalize( &
               matmul( (/0.0_P,1.0_P,1.0_P/sqrt(2.0_P)/), r_mat ) )
          context%system%particles%particle(i)%orientation(:,4) = normalize( &
               matmul( (/0.0_P,-1.0_P,1.0_P/sqrt(2.0_P)/), r_mat ) )
       end if
    end do
  end subroutine generate_patches

end module config
