module celllist
  use datatypes
  use util_mc
  implicit none

contains

  !==================================!
  !     initialize the cell list     !
  !==================================!
  subroutine initialize_cell_list(context,system)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    type (system_struct),  intent(inout) :: system
    ! internal variables
    real(P) :: cell_volume
    integer :: max_occupancy, num_cells, cell_id

    ! get minimum cell size
    call update_min_cell_size(context,system)
    
    ! calculate cell size and number
    system%cell_list%cell_dim = &
         floor( system%box_length / system%cell_list%min_cell_size  )
    ! so can have more than dimension of 3

    where( system%cell_list%cell_dim .lt. 3 )
       system%cell_list%cell_dim = 3
    end where

    where( system%cell_list%cell_dim .gt. max_cells )
       system%cell_list%cell_dim = max_cells
    end where

    num_cells   = product( system%cell_list%cell_dim )
    ! calculate cell size
    system%cell_list%side_length = system%box_length / system%cell_list%cell_dim
    cell_volume = product( system%cell_list%side_length )

    ! assume the density can be no higher than 10
    ! (this is a VERY safe assumption -- but only if we have the particle diameters!)
    max_occupancy = ceiling( cell_volume ) * 10
    system%cell_list%max_occupancy = max_occupancy

    ! print*,'system:',system%box_length,system%cell_list%min_cell_size
    ! print*,'new cell list:',system%cell_list%side_length,cell_volume,max_occupancy

    ! allocate memory for cell list
    allocate( system%cell_list%cell_count( num_cells ) )
    allocate( system%cell_list%cell_map( max_occupancy, num_cells ) )
    ! allocate memory for neighbors list
    allocate( system%cell_list%neighbors( 27, num_cells ) )
    ! calculate neighbors list
    do cell_id = 1, num_cells
       system%cell_list%neighbors(:,cell_id) = cell_neighbors(system,cell_id)
    end do

    if( any( system%cell_list%cell_dim .gt. 3 ) ) then
       system%trial%use_cell_list = .true.
       ! write(*,'("cell list WILL be used for pair potential computation")')
    else
       system%trial%use_cell_list = .false.
       ! write(*,'("cell list will NOT be used for pair potential computation")')
    end if

  end subroutine initialize_cell_list

  !===============================!
  !     destroy the cell list     !
  !===============================!
  subroutine cleanup_cell_list(system)
    implicit none
    ! external variables
    type (system_struct), intent(inout) :: system

    ! deallocate memory for cell list
    if( allocated(system%cell_list%cell_count) ) deallocate( system%cell_list%cell_count )
    if( allocated(system%cell_list%cell_map) )   deallocate( system%cell_list%cell_map )
    if( allocated(system%cell_list%neighbors) )  deallocate( system%cell_list%neighbors )
    
  end subroutine cleanup_cell_list

  !================================================================!
  !     update the minimum cell size from potentials and moves     !
  !================================================================!
  subroutine update_min_cell_size(context,system)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    type (system_struct),  intent(inout) :: system
    ! internal variables
    integer :: i
    real(P) :: displace_magnitude
    ! get displace magnitude
    do i = 1, size( context%moves )
       if( trim(context%moves(i)%name) .eq. "displace" ) then
          displace_magnitude = context%moves(i)%magnitude
       end if
    end do
    ! calculate cell size
    system%cell_list%min_cell_size = context%potentials%max_range + displace_magnitude
  end subroutine update_min_cell_size
  
  !=====================================================!
  !     query whether the cell list needs a rebuild     !
  !=====================================================!
  subroutine query_cell_list_rebuild(context,system)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    type (system_struct),  intent(inout) :: system
    ! internal variables
    integer, dimension(3) :: nominal_cells
    
    ! update cell size
    system%cell_list%side_length = system%box_length / system%cell_list%cell_dim

    ! get minimum cell size
    call update_min_cell_size(context,system)
    
    ! check if the cells are the wrong size
    nominal_cells = floor( system%box_length / system%cell_list%min_cell_size  )   

    where( nominal_cells .lt. 3 )
       nominal_cells = 3
    end where

    where( nominal_cells .gt. max_cells )
       nominal_cells = max_cells
    end where

    if( any( nominal_cells .lt. system%cell_list%cell_dim ) .or. &
         any( nominal_cells .gt. system%cell_list%cell_dim * 2 ) ) then
       call build_cell_list(context,system)
    end if
    
  end subroutine query_cell_list_rebuild
  
  !========================================!
  !     build or rebuild the cell list     !
  !========================================!
  subroutine build_cell_list(context,system)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    type (system_struct),  intent(inout) :: system
    ! internal variables
    integer :: particle_id, cell_id
    
    if( allocated(system%cell_list%cell_map) ) then
       ! destroy the old data structures
       call cleanup_cell_list(system)
       ! create the new data structures
       call initialize_cell_list(context,system)
    else
       ! create the new data structures
       call initialize_cell_list(context,system)
    end if
    
    ! zero the cell list
    system%cell_list%cell_map = 0
    system%cell_list%cell_count = 0

    ! place particles in the appropriate cells
    do particle_id = 1, system%num_particles
       ! get the cell id from the particle position
       cell_id = which_cell(system,system%particles%particle(particle_id)%position(:))
       ! store the information in the cell list
       system%cell_list%cell_count(cell_id) = system%cell_list%cell_count(cell_id) + 1
       system%cell_list%cell_map(system%cell_list%cell_count(cell_id),cell_id) = particle_id
    end do

  end subroutine build_cell_list

  !============================================================!
  !     update the cell list with new particle information     !
  !============================================================!
  subroutine update_cell_list(system)
    implicit none
    ! external variables
    type (system_struct),    intent(inout) :: system
    ! internal variables
    integer, dimension(:), allocatable :: particle_ids, old_cell_ids, new_cell_ids
    integer :: this_id, this_particle, nt, i, j
    integer :: old_cell_count, new_cell_count

    nt = system%trial%num_trial
    allocate(particle_ids(nt))
    allocate(old_cell_ids(nt))
    allocate(new_cell_ids(nt))
    
    particle_ids = system%trial%particle_ids(1:nt)

    ! find which cell the old particles belongs to
    do i = 1, nt
       old_cell_ids(i) = which_cell(system,system%particles%particle(particle_ids(i))%position)
    end do

    ! find which cell the new particles belongs to
    ! change to two positions
    do i = 1, nt
       new_cell_ids(i) = which_cell(system,system%trial%particle(i)%position)
    end do

    ! only continue if the particle has moved to a different cell
    do i = 1, nt
       if( old_cell_ids(i) .ne. new_cell_ids(i) ) then

          ! find the particle in the old map
          this_id = 0
          this_particle = 0
          do while( this_particle .ne. particle_ids(i) &
               .and. this_id .lt. system%num_particles )
             this_id = this_id + 1
             this_particle = system%cell_list%cell_map(this_id,old_cell_ids(i))
          end do
              
          ! remove one count from the old cell
          old_cell_count = system%cell_list%cell_count(old_cell_ids(i)) - 1
          system%cell_list%cell_count(old_cell_ids(i)) = old_cell_count

          ! shift other entries up in the old map to remove this one
          do j = this_id, old_cell_count
             system%cell_list%cell_map(j,old_cell_ids(i)) = &
                  system%cell_list%cell_map(j+1,old_cell_ids(i))
          end do

          ! add one count to the new cell
          new_cell_count = system%cell_list%cell_count(new_cell_ids(i)) + 1
          system%cell_list%cell_count(new_cell_ids(i)) = new_cell_count

          ! append the particle to the new map
          system%cell_list%cell_map(new_cell_count,new_cell_ids(i)) = particle_ids(i)

       end if
    end do

  end subroutine update_cell_list
  
  !========================================================!
  !     identify cell id based on particle coordinates     !
  !========================================================!
  function which_cell(system,particle_coord)
    implicit none
    integer :: which_cell
    ! external variables
    type (system_struct),   intent(inout) :: system
    real(P), dimension(3),  intent(inout) :: particle_coord
    ! internal variables
    integer :: dim
    integer, dimension(3) :: cell_coord
    
    ! find the coordinates
    ! NOTE: coordinates are shifted by L/2 so the indices start at zero
    cell_coord = floor( ( particle_coord + system%box_length*0.5_P ) / system%cell_list%side_length )

    do dim = 1, 3
       do while( cell_coord(dim) .lt. 0 )
          cell_coord(dim) = cell_coord(dim) + system%cell_list%cell_dim(dim)
       end do
    end do

    do dim = 1, 3
       do while( cell_coord(dim) .ge. system%cell_list%cell_dim(dim) )
          cell_coord(dim) = cell_coord(dim) - system%cell_list%cell_dim(dim)
       end do
    end do
    ! previous two do loops akin to preiodic boundary conditions

    ! convert the coordinates to id
    which_cell = cell_coord_to_id(cell_coord,system%cell_list%cell_dim)

    if( which_cell .gt. product(system%cell_list%cell_dim) &
         .or. which_cell .lt. 1 ) then
       print*,'problem in which_cell:',particle_coord,':',cell_coord,':',&
            system%cell_list%side_length,'->',which_cell
       stop
    end if
    
  end function which_cell
  
  !=========================================================!
  !     identify neighboring cells based on cell number     !
  !=========================================================!
  function cell_neighbors(system,cell_id)
    implicit none
    integer, dimension(27) :: cell_neighbors
    ! external variables
    type (system_struct), intent(inout) :: system
    integer, intent(in) :: cell_id
    ! internal variables
    integer, dimension(3) :: cell_coord, neighbor_coord
    integer :: i, j, k, count
    integer :: cell_id_copy

    cell_id_copy = cell_id
    
    ! start by getting a tuple of the coordinates
    cell_coord = cell_id_to_coord(cell_id_copy,system%cell_list%cell_dim)

    if( any( cell_coord .ge. system%cell_list%cell_dim ) ) then
       print*,'problem in cell_neighbors'
    end if

    count = 0
    ! loop through cell neighbors in 3D
    do i = cell_coord(1)-1, cell_coord(1)+1
       do j = cell_coord(2)-1, cell_coord(2)+1
          do k = cell_coord(3)-1, cell_coord(3)+1
             count = count + 1
             neighbor_coord = wrap_cell_coord((/i,j,k/),system%cell_list%cell_dim)
             cell_neighbors(count) = cell_coord_to_id(neighbor_coord,system%cell_list%cell_dim)
          end do
       end do
    end do
    
  end function cell_neighbors

end module celllist
