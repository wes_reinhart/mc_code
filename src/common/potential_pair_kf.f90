module potential_pair_kf
  use globals
  use datatypes_potentials
  use datatypes
  implicit none

contains
  
  !==============================================!
  !     computes kern-frenkel pair potential     !
  !==============================================!
  real(P) function eval_kf(potentials,particle_i,particle_j,r_ij,r_ij_length)
    implicit none
    ! external variables
    type (potentials_struct), intent(inout) :: potentials
    type (particle_data_struct), intent(inout) :: particle_i, particle_j
    real(P), dimension(3), intent(in) :: r_ij
    real(P), intent(in) :: r_ij_length
    ! internal variables
    real(P) :: d_i, d_j
    logical :: kf_aligned
    real(P) :: cutoff, potential_cutoff

    d_i = particle_i%diameter
    d_j = particle_j%diameter

    ! calculate radial and potential cutoff from the diameters of the two particles
    cutoff = (d_i + d_j)/2.0_P
    potential_cutoff = cutoff + potentials%kf%delta*2.0_P
    
    ! assume no interaction
    eval_kf = 0.0_P

    ! Kern-Frenkel potential

    if( r_ij_length .gt. cutoff .and. &
         r_ij_length .le. potential_cutoff ) then

       ! check alignment
       kf_aligned = kf_alignment(potentials,r_ij/r_ij_length,particle_i,particle_j)

       if( kf_aligned ) then
          eval_kf = -1.0_P
       end if

    end if

  end function eval_kf

  !============================================!
  !     return alignment status of i and j     !
  !============================================!
  logical function kf_alignment(potentials,r_ij,particle_i,particle_j)
    implicit none
    ! external variables
    type (potentials_struct), intent(inout) :: potentials 
    real(P), dimension(:),    intent(in) :: r_ij
    type (particle_data_struct), intent(inout) :: particle_i, particle_j
    ! internal variables
    real(P), dimension(:,:), allocatable :: i_orient, j_orient
    real(P) :: i_north, j_north
    integer :: i, np
    logical :: i_align, j_align

    i_orient = particle_i%orientation(:,:)
    j_orient = particle_j%orientation(:,:)
    
    np = size(i_orient,2)
    
    ! compute alignment of all patches on particle i
    i_align = .false.
    do i = 1, np
       i_north = dot_product( i_orient(:,i), r_ij )
       if( i_north .ge. potentials%kf%align_cutoff ) then
          i_align = .true.
       end if
    end do

    ! compute alignment of all patches on particle j
    j_align = .false.
    do i = 1, np
       j_north = dot_product( j_orient(:,i), -r_ij )
       if( j_north .ge. potentials%kf%align_cutoff ) then
          j_align = .true.
       end if
    end do

    kf_alignment = (i_align .and. j_align)
    
  end function kf_alignment

end module potential_pair_kf
