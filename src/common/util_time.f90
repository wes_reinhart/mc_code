module util_time
  use util_print
  use globals
  use datatypes

contains

  !======================================!
  !     check time since last report     !
  !======================================!
  subroutine timer_start(context)
    implicit none
    type (context_struct) :: context
    call cpu_time(context%parameters%start_time)
    context%parameters%last_time = context%parameters%start_time
    context%parameters%last_step = context%parameters%timestep
  end subroutine timer_start
  
  !======================================!
  !     check time since last report     !
  !======================================!
  subroutine timer_check(context)
    implicit none
    type (context_struct) :: context
    call cpu_time(context%parameters%current_time)
    ! print ETA every 10 seconds
    if( context%parameters%current_time &
         - context%parameters%last_time .gt. 10.0_P ) then
       call print_elapsed_and_eta(context)
    end if
  end subroutine timer_check

  !===========================================!
  !     print elapsed and eta to terminal     !
  !===========================================!
  subroutine print_elapsed_and_eta(context)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    ! internal variables
    real(P) :: elapsed_start, time_elapsed_last, eta, tps
    integer :: steps_elapsed_last
    ! compute differences
    elapsed_start      = context%parameters%current_time - context%parameters%start_time
    time_elapsed_last  = context%parameters%current_time - context%parameters%last_time
    steps_elapsed_last = context%parameters%timestep     - context%parameters%last_step
    ! estimate remaining time
    eta = elapsed_start * ( 1.0_P / (real(context%parameters%timestep,P) &
         / real(context%parameters%sweeps,P)) - 1.0_P )
    ! compute local sweeps per second
    !   ("TPS" is from HOOMD, timesteps per second)
    !   (this is probably bad nomenclature for MC simulations)
    tps = real(steps_elapsed_last,P) / time_elapsed_last
    ! write update to command line
    write(*,'("Time ",A," | Step ",I0," / ",I0)',advance='no') &
         sec_to_hms(elapsed_start), context%parameters%timestep, context%parameters%sweeps
    write(*,'(" | TPS ",A," | ETA ",A)') var_float(tps,2), sec_to_hms(eta)
    ! update last values
    context%parameters%last_time = context%parameters%current_time
    context%parameters%last_step = context%parameters%timestep
  end subroutine print_elapsed_and_eta

  !===========================================!
  !     print elapsed and eta to terminal     !
  !===========================================!
  subroutine print_final_time(context)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    ! internal variables
    real(P) :: elapsed_start, tps
    call cpu_time(context%parameters%current_time)
    ! compute differences
    elapsed_start      = context%parameters%current_time - context%parameters%start_time
    ! compute local sweeps per second
    !   ("TPS" is from HOOMD, timesteps per second)
    !   (this is probably bad nomenclature for MC simulations)
    tps = real(context%parameters%timestep,P) / elapsed_start
    ! write update to command line
    write(*,'("Time ",A," | Step ",I0," / ",I0)',advance='no') &
         sec_to_hms(elapsed_start), context%parameters%timestep, context%parameters%sweeps
    write(*,'(" | TPS ",A)') var_float(tps,2)
    ! update last values
    context%parameters%last_time = context%parameters%current_time
    context%parameters%last_step = context%parameters%timestep
  end subroutine print_final_time
  
  !==============================================================!
  !     given time in seconds, returns character of hh:mm:ss     !
  !==============================================================!
  function sec_to_hms(time)
    implicit none
    character(len=:), allocatable :: sec_to_hms
    ! external variables
    real(P), intent(in) :: time
    ! internal variables
    real(P) :: h, m, s
    integer :: tot_places
    character(len=10) :: h_char
    h = floor(time/3600.0_P)
    m = floor( (time-3600.0_P*h)/60.0_P )
    s = time - 3600.0_P*h - 60.0_P*m
    write(h_char,'(I0)') int(h)
    tot_places = len(trim(h_char)) + 6
    allocate( character(len=tot_places) :: sec_to_hms )
    write(sec_to_hms,'(A,":",A,":",A)') &
         trim(h_char), zero_pad_time(m), zero_pad_time(s)
  end function sec_to_hms

  !===================================!
  !     given t, returns tt or 0t     !
  !===================================!
  character(len=2) function zero_pad_time(time)
    implicit none
    ! external variables
    real(P), intent(in) :: time
    ! internal variables
    integer :: round_time
    round_time = nint(time)
    if( round_time .ge. 60 .or. round_time .lt. 0 ) then
       zero_pad_time = "**"
    else if( round_time .eq. 0 ) then
       zero_pad_time = "00"
    else if( round_time .lt. 10 ) then
       write(zero_pad_time,'("0",I0)') round_time
    else
       write(zero_pad_time,'(I0)') round_time
    end if
  end function zero_pad_time
  
end module util_time
