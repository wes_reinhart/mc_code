!=================================================================!
!     contains the derived data types used by all the modules     !
!=================================================================!
module datatypes
  use globals
  use datatypes_potentials
  implicit none

  !=====================================================================!
  !     holds parameters which are frequently referenced by modules     !
  !=====================================================================!
  type :: parameters_struct
     character(len=100) :: run_name     ! command line [1]
     character(len=100) :: run_dir      ! command line [2]
     integer :: rank                    ! MPI rank
     character(len=1000) :: config_file ! name of config file, if one exists
     integer :: timestep = 0            ! current timestep for logger, snapshots, etc
     integer :: sweeps                  ! each sweep attempts num_particles moves
     integer :: reset_period            ! sweeps between full energy calculation
     integer :: adjust_period           ! sweeps between movesize adjustment
     ! integer :: num_quadrature ! number of Gauss-Legendre quadrature points
     real(P) :: start_time
     real(P) :: last_time
     real(P) :: current_time
     integer :: last_step
     ! for thermodynamic integration
     real(P) :: lambda
  end type parameters_struct

  !========================================!
  !  holds the data for a single particle  !
  !========================================!
  type :: particle_data_struct
     real(P) :: diameter
     ! spatial information
     real(P), dimension(3) :: position ! xyz coords of the particle
     real(P), dimension(:,:), allocatable :: orientation ! normalized orientation of the particle
  end type particle_data_struct

  !=====================================!
  !     holds all the particle data     !
  !=====================================!
  type :: particles_data_struct
     type (particle_data_struct), dimension(:), allocatable :: particle ! all the particles in the system
     integer :: num_patches = 0 ! number of patches on each particle
     real(P), dimension(2) :: diameter_mean_var ! mean and variance of the diameter
  end type particles_data_struct

  !=======================================!
  !     holds all the potentials data     !
  !=======================================!
  type :: potentials_data_struct
     real(P) :: tot_potential             ! total potential between all terms
     real(P) :: tot_potential_pair        ! pairwise interactions for all particles (what is this?)
     real(P) :: tot_potential_restrain_tr ! potential from translational field (what is this?)
     real(P) :: tot_potential_restrain_or ! potential from orientational field (what is this?)
     real(P) :: tot_potential_bias        ! potential from biasing potential
     real(P), dimension(:,:), allocatable :: potential_pair
     real(P), dimension(:),   allocatable :: potential_restrain_tr
     real(P), dimension(:),   allocatable :: potential_restrain_or
  end type potentials_data_struct

  !========================================!
  !     holds cell list for the system     !
  !========================================!
  type :: cell_list_struct
     integer, dimension(3) :: cell_dim    ! number of cells in each dimension
     real(P), dimension(3) :: side_length ! dimensions of the cell (like xyz dimensions? each cell the same?)
     integer :: max_occupancy             ! memory allocated to the cell
     real(P) :: min_cell_size             ! largest r_cut plus largest displace move

     ! particle ids for cell j are stored in column j of cell_map
     ! column j of cell_map contains cell_count(j) entries (are you allowed to have a ragged array?
     integer, dimension(:,:), allocatable :: cell_map

     integer, dimension(:),   allocatable :: cell_count ! number of particles in each cell
     integer, dimension(:,:), allocatable :: neighbors  ! list of all the neighbors (particles?) for each cell
  end type cell_list_struct

  !======================================!
  !     holds temporary trial states     !
  !======================================!
  type :: trial_struct
     integer, dimension(:), allocatable :: particle_ids    ! particles that were moved
     integer :: move_type      ! move type applied
     logical :: full_recompute ! trigger a full recompute of the energy
     logical :: use_cell_list  ! for the compute module

     integer, dimension(:,:), allocatable :: delta_ids ! ids of other particles affected
     integer, dimension(:), allocatable :: num_delta                ! number of other particles affected
     integer :: num_trial ! number of trial particles being used in the move

     ! trial particle(s) spatial information
     type (particle_data_struct), dimension(:), allocatable :: particle ! particle information

     ! all-particle spatial information
     type (particle_data_struct), dimension(:), allocatable :: all_particles ! all particle information

     ! trial-particle thermodynamic information
     real(P), dimension(:,:), allocatable :: potential_pairs
     real(P), dimension(:), allocatable :: potential_restrain_tr
     real(P), dimension(:), allocatable :: potential_restrain_or

     ! all-particle thermodynamic information
     real(P), dimension(:,:), allocatable :: all_potential_pair
     real(P), dimension(:),   allocatable :: all_potential_restrain_tr
     real(P), dimension(:),   allocatable :: all_potential_restrain_or

     ! system thermodynamic information
     real(P) :: tot_potential             ! total potential between all terms
     real(P) :: tot_potential_pair        ! pairwise interactions for all particles
     real(P) :: tot_potential_restrain_tr ! potential from translational field
     real(P) :: tot_potential_restrain_or ! potential from orientational
     real(P) :: tot_potential_bias        ! potential from biasing potential
  end type trial_struct

  !==========================================!
  !     holds all the system information     !
  !==========================================!
  type :: system_struct
     integer :: num_particles = 0        ! number of particles in the system
     real(P), dimension(3) :: box_length ! Lx, Ly, Lz (so is the box always a rectangular prism?
     real(P) :: nominal_lattice_spacing  ! grid spacing for generating configuration
     character(len=3) :: ensemble        ! thermodynamic ensemble
     real(P), dimension(:), allocatable :: move_prob ! cumulative move probabilities
     real(P) :: beta      ! inverse temperature
     real(P) :: pressure  ! pressure
     real(P) :: potential ! total potential (equal to context%potentials%tot_potential?)
     type (particles_data_struct)  :: particles
     type (potentials_data_struct) :: potentials
     type (cell_list_struct)       :: cell_list
     type (trial_struct)           :: trial
  end type system_struct

  !=========================================!
  !     holds ratios in the stat struct     !
  !=========================================!
  type :: move_struct
     character(len=100) :: name   ! move name
     real(P) :: frequency = 0.0_P ! relative frequency to attempt this move

     integer :: trial_particles   ! number of trial particles in a move

     ! parameters for moves
     real(P) :: magnitude = 0.0_P
     real(P) :: maximum = 0.0_P
     real(P) :: minimum = 0.0_P

     ! specific to rescale
     logical :: uniform_rescale
     real(P), dimension(3) :: rescale_prob ! cumulative probability to rescale in each dimension

     ! cumulative stats on move acceptance rates
     integer :: total_success = 0
     integer :: total_attempt = 0
     integer :: local_success = 0
     integer :: local_attempt = 0

     real(P) :: nominal_acc   ! nominal acceptance rate for the move
     real(P) :: adjust_factor ! magnitude of movesize adjustment
  end type move_struct

  !=========================================!
  !     holds parameters for the logger     !
  !=========================================!
  type :: log_struct
     logical :: enabled = .false.     ! default off
     character(len=1000) :: prefix    ! log prefix
     character(len=1000) :: fmt       ! log format string
     integer :: fid                   ! file id for writing to
     integer :: period                ! log period
     integer :: precision             ! decimal precision
     integer :: min_size              ! minimum number of places to use in logger
     integer, dimension(17) :: order  ! sequence to write logged quantities
     character(len=100), dimension(17) :: names
     logical :: potential             ! total potential
     logical :: potential_pair        !
     logical :: potential_restrain_tr !
     logical :: potential_restrain_or !
     logical :: num_particles         ! for uVT simulations
     logical :: temperature           !
     logical :: pressure              !
     logical :: volume                !
     logical :: lx                    !
     logical :: ly                    !
     logical :: lz                    !
     logical :: potential_pair_kf     !
     logical :: potential_pair_ckf    !
     logical :: potential_pair_lj     !
     logical :: potential_pair_wca    !
     logical :: potential_pair_hs     !
     logical :: swap_acceptance       !
     ! do we need to include diameters here?
  end type log_struct

  !===========================================!
  !     holds parameters for the snapshot     !
  !===========================================!
  type :: snapshot_struct
     logical :: enabled   = .false.   ! default off
     logical :: overwrite = .false.   ! should we overwrite the previous snapshot each time
     logical :: potential = .false.   ! should we write the particle potentials for each snapshot
     character(len=100) :: prefix     ! snapshot prefix
     integer :: fid                   ! file id for writing to
     integer :: period                ! snapshot period
  end type snapshot_struct

  !======================================================!
  !     holds the entirety of the system information     !
  !======================================================!
  type :: context_struct
     type (parameters_struct) :: parameters
     type (potentials_struct) :: potentials
     type (system_struct)     :: system, trial_system
     type (move_struct), dimension(:), allocatable :: moves
     type (log_struct)        :: log
     type (snapshot_struct), dimension(:), allocatable :: snapshots
  end type context_struct

contains

  !===================================================!
  !     check all rotational data for consistency     !
  !===================================================!
  subroutine check_rotation_consistency(context)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    ! internal variables
    logical :: no_patches, rotation_potentials, rotation_moves
    integer :: i

    no_patches = (context%system%particles%num_patches .eq. 0)
    rotation_potentials = context%potentials%kf%enabled &
         .or. context%potentials%ckf%enabled &
         .or. context%potentials%restrain_or%enabled
    if( rotation_potentials .and. no_patches ) then
       write(*,'("Error: there are anisotropic potentials enabled but no orientations!")')
       stop
    end if
    rotation_moves = any( context%moves(:)%name .eq. "rotate" )
    if( rotation_moves .and. no_patches ) then
       write(*,'("Error: there are rotation moves enabled but no orientations!")')
       stop
    end if
  end subroutine check_rotation_consistency

  !================================================!
  !     check context for ensemble consistency     !
  !================================================!
  subroutine check_ensemble_consistency(context)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    ! internal variables
    logical :: any_moves, rescale_moves, npt
    integer :: i
    real(P) :: s
    if( .not. allocated( context%moves ) ) then
       write(*,'("Error (",I0,"): cannot run mc without any trial moves")') line_number
       stop
    else
       s = 0.0_P
       do i = 1, size(context%moves)
          s = s + context%moves(i)%frequency
       end do
       if( s .le. 0.0_P ) then
          write(*,'("Error (",I0,"): cannot run mc without any trial moves")') line_number
          stop
       else
          any_moves = .true.
       end if
    end if

    npt = ( context%system%ensemble .eq. "npt" )
    rescale_moves = .false.
    do i = 1, size(context%moves)
       if( context%moves(i)%name .eq. "rescale" &
            .and. context%moves(i)%frequency .gt. 0.0_P ) then
          rescale_moves = .true.
       end if
    end do

    if( npt .and. .not. rescale_moves ) then
       write(*,'("Error (",I0,"): NPT ensemble must have a valid rescaling move")') line_number
       stop
    end if

  end subroutine check_ensemble_consistency

  !=================================!
  !     initialize logger names     !
  !=================================!
  subroutine init_log_names(context)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    ! write names
    context%log%names(1)  = "potential"
    context%log%names(2)  = "potential_pair"
    context%log%names(3)  = "potential_restrain_tr"
    context%log%names(4)  = "potential_restrain_or"
    context%log%names(5)  = "num_particles"
    context%log%names(6)  = "temperature"
    context%log%names(7)  = "pressure"
    context%log%names(8)  = "volume"
    context%log%names(9)  = "lx"
    context%log%names(10) = "ly"
    context%log%names(11) = "lz"
    context%log%names(12) = "potential_pair_kf"
    context%log%names(13) = "potential_pair_ckf"
    context%log%names(14) = "potential_pair_lj"
    context%log%names(15) = "potential_pair_wca"
    context%log%names(16) = "potential_pair_hs"
    context%log%names(17) = "swap_acceptance"
    ! if diameters are included in the logger struct also need to include them here
  end subroutine init_log_names

  !=============================!
  !     expand moves struct     !
  !=============================!
  !   i.e. adds a type of move  !
  !=============================!
  subroutine expand_moves(context)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    ! internal variables
    type (move_struct), dimension(:), allocatable :: temp_moves
    integer :: i
    ! copy moves from context to buffer
    if( size( context%moves ) .gt. 0 ) then
       allocate( temp_moves( size( context%moves ) ) )
       do i = 1, size( context%moves )
          temp_moves(i) = context%moves(i)
       end do
    end if
    ! resize moves instance inside context
    if( allocated(context%moves) ) deallocate(context%moves)
    allocate( context%moves( size(temp_moves)+1 ) )
    ! copy back from buffer to context
    do i = 1, size( temp_moves )
       context%moves(i) = temp_moves(i)
    end do
  end subroutine expand_moves

  !================================!
  !     expand snapshot struct     !
  !================================!
  subroutine expand_snapshots(context)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    ! internal variables
    type (snapshot_struct), dimension(:), allocatable :: temp_snapshot
    integer :: i
    ! copy snapshot from context to buffer
    if( size( context%snapshots ) .gt. 0 ) then
       allocate( temp_snapshot( size( context%snapshots ) ) )
       do i = 1, size( context%snapshots )
          temp_snapshot(i) = context%snapshots(i)
       end do
    end if
    ! resize snapshot instance inside context
    if( allocated(context%snapshots) ) deallocate(context%snapshots)
    allocate( context%snapshots( size(temp_snapshot)+1 ) ) ! would it be faster to use a resizing array?
    ! copy back from buffer to context
    do i = 1, size( temp_snapshot )
       context%snapshots(i) = temp_snapshot(i)
    end do
  end subroutine expand_snapshots

  !================================================!
  !     do a deep copy of the cell_list_struct     !
  !================================================!
  subroutine copy_cell_list(a,b)
    implicit none
    ! external variables
    type (system_struct), intent(inout) :: a, b
    a%cell_list%cell_dim      = b%cell_list%cell_dim
    a%cell_list%side_length   = b%cell_list%side_length
    a%cell_list%max_occupancy = b%cell_list%max_occupancy
    a%cell_list%min_cell_size = b%cell_list%min_cell_size
    a%cell_list%cell_map      = b%cell_list%cell_map
    a%cell_list%cell_count    = b%cell_list%cell_count
    a%cell_list%neighbors     = b%cell_list%neighbors
  end subroutine copy_cell_list

end module datatypes
