module util_rand
  use globals
  use datatypes
  implicit none

contains

  !==========================================!
  !     seed the random number generator     !
  !==========================================!
  subroutine initialize_rng()
    use iso_fortran_env, only: int64
    implicit none
    integer, allocatable :: seed(:)
    integer :: i, n, un, istat, dt(8), pid
    integer(int64) :: t

    call random_seed(size = n)
    allocate(seed(n))
    ! First try if the OS provides a random number generator
    open(newunit=un, file="/dev/urandom", access="stream", &
         form="unformatted", action="read", status="old", iostat=istat)
    if (istat == 0) then
       read(un) seed
       close(un)
    else
       ! Fallback to XOR:ing the current time and pid. The PID is
       ! useful in case one launches multiple instances of the same
       ! program in parallel.
       call system_clock(t)
       if (t == 0) then
          call date_and_time(values=dt)
          t = (dt(1) - 1970) * 365_int64 * 24 * 60 * 60 * 1000 &
               + dt(2) * 31_int64 * 24 * 60 * 60 * 1000 &
               + dt(3) * 24_int64 * 60 * 60 * 1000 &
               + dt(5) * 60 * 60 * 1000 &
               + dt(6) * 60 * 1000 + dt(7) * 1000 &
               + dt(8)
       end if
       pid = getpid()
       t = ieor(t, int(pid, kind(t)))
       do i = 1, n
          seed(i) = lcg(t)
       end do
    end if
    call random_seed(put=seed)
  contains
    ! This simple PRNG might not be good enough for real work, but is
    ! sufficient for seeding a better PRNG.
    function lcg(s)
      integer :: lcg
      integer(int64) :: s
      if (s == 0) then
         s = 104729
      else
         s = mod(s, 4294967296_int64)
      end if
      s = mod(s * 279470273_int64, 4294967291_int64)
      lcg = int(mod(s, int(huge(0), int64)), kind(0))
    end function lcg
  end subroutine initialize_rng

  !===============================================================!
  !     choose a random point on the surface of a unit sphere     !
  !===============================================================!
  subroutine random_sphere(direction)
    implicit none
    ! external variables
    real(P), dimension(3), intent(out) :: direction
    ! internal variables
    real(P) :: u, v
    call random_number(u)
    u = u * 2.0_P - 1.0_P
    call random_number(v)
    v = v * 2.0_P * pi
    direction(1) = sqrt(1-u**2.0)*cos(v)
    direction(2) = sqrt(1-u**2.0)*sin(v)
    direction(3) = u
  end subroutine random_sphere

  !===============================================================!
  !      choose a random number from a normal distribution        !
  !===============================================================!
  !     uses the Box-Muller Transform, could also implement       !
  !     with a probability integral transform using the           !
  !     inverse cumalitve normal distribution. that could be      !
  !     more accurate in some cases, but would be                 !
  !     computationally slower in all cases.                      !
  !===============================================================!
  real(P) function random_normal(mean_var)
    implicit none
    ! external variables
    real(P), dimension(:), intent(inout) :: mean_var
    ! internal variables
    real(P) :: u, v, x, y, coin_flip, rand_value
    logical :: not_pos

    not_pos = .true.
    
    do while (not_pos)
       call random_number(u)
       call random_number(v)
       call random_number(coin_flip)
       x = sqrt(-2.0*log(u))*cos(2.0*pi*v)
       y = sqrt(-2.0*log(u))*sin(2.0*pi*v)
       
       if (coin_flip .lt. 0.5) then
          rand_value = x
       else
          rand_value = y
       end if

       random_normal = rand_value * sqrt(mean_var(2)) + mean_var(1)

       if(random_normal .gt. 0.0_P) exit
    
       write(*,'("WARNING: Diameter is not a positive number. Picking another diameter.")')
    end do
    
  end function random_normal

end module util_rand
