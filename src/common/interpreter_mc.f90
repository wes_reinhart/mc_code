module interpreter_mc
  use globals
  use interpreter_util
  use datatypes
  use logger
  use snapshot
  use driver
  implicit none

contains

  !=======================================================!
  !     interpret a mc command and send to subroutine     !
  !=======================================================!
  subroutine interpret_mc_command(context,fline)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    character(len=*), intent(inout) :: fline
    ! internal variables
    character(len=100) :: cmd_key, cmd_type
    integer :: s, e

    ! extract move type from fline
    cmd_key = "mc."
    s = index(fline,trim(cmd_key)) + len(trim(cmd_key))
    e = index(fline(s:),"(") - 2 + s
    cmd_type = trim(fline(s:e))

    ! switch between different potential types
    if( trim(cmd_type) .eq. "log" ) then
       call interpret_mc_log(context,fline)
    else if( trim(cmd_type) .eq. "snapshot" ) then
       call interpret_mc_snapshot(context,fline)
    else if( trim(cmd_type) .eq. "nvt" ) then
       call interpret_mc_nvt(context,fline)
    else if( trim(cmd_type) .eq. "npt" ) then
       call interpret_mc_npt(context,fline)
    else if( trim(cmd_type) .eq. "run" ) then
       call interpret_mc_run(context,fline)
    else
       write(*,'("Error (",I0,"): mc command ",A," not recognized")') &
            line_number, trim(cmd_type)
       stop
    end if

  end subroutine interpret_mc_command

  !===================================!
  !     interpret mc.log commands     !
  !===================================!
  subroutine interpret_mc_log(context,fline)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    character(len=*),      intent(inout) :: fline
    ! internal variables
    logical :: enable_log
    write(*,'("mc.log:")')
    ! default to enabled if the command is used
    enable_log = interpret_generic_logical("enabled",fline,.false.,.true.)
    if( enable_log .and. context%log%enabled ) then
       write(*,'("Warning (",I0,"): Only one logger can be active at a time. Closing old logger (",A,".log).")') &
            line_number, trim(context%log%prefix)
       call close_log(context)
    end if
    context%log%enabled = enable_log
    ! only continue reading if logger is enabled
    if( context%log%enabled ) then
       call init_log_names(context)
       ! required
       context%log%prefix = interpret_generic_character("prefix",fline,.true.,"none")
       context%log%period = interpret_generic_integer("period",fline,.true.,0)
       ! optional
       context%log%precision = interpret_generic_integer("precision",fline,.false.,5)
       context%log%min_size  = context%log%precision + 7 ! leave room for exponential formatting
       ! logged quantities logicals
       context%log%potential             = &
            interpret_generic_logical(trim(context%log%names(1)),fline,.false.,.false.)
       context%log%potential_pair        = &
            interpret_generic_logical(trim(context%log%names(2)),fline,.false.,.false.)
       context%log%potential_restrain_tr = &
            interpret_generic_logical(trim(context%log%names(3)),fline,.false.,.false.)
       context%log%potential_restrain_or = &
            interpret_generic_logical(trim(context%log%names(4)),fline,.false.,.false.)
       context%log%num_particles         = &
            interpret_generic_logical(trim(context%log%names(5)),fline,.false.,.false.)
       context%log%temperature           = &
            interpret_generic_logical(trim(context%log%names(6)),fline,.false.,.false.)
       context%log%pressure              = &
            interpret_generic_logical(trim(context%log%names(7)),fline,.false.,.false.)
       context%log%volume                = &
            interpret_generic_logical(trim(context%log%names(8)),fline,.false.,.false.)
       context%log%lx                    = &
            interpret_generic_logical(trim(context%log%names(9)),fline,.false.,.false.)
       context%log%ly                    = &
            interpret_generic_logical(trim(context%log%names(10)),fline,.false.,.false.)
       context%log%lz                    = &
            interpret_generic_logical(trim(context%log%names(11)),fline,.false.,.false.)
       context%log%potential_pair_kf     = &
            interpret_generic_logical(trim(context%log%names(12)),fline,.false.,.false.)
       context%log%potential_pair_ckf    = &
            interpret_generic_logical(trim(context%log%names(13)),fline,.false.,.false.)
       context%log%potential_pair_lj     = &
            interpret_generic_logical(trim(context%log%names(14)),fline,.false.,.false.)
       context%log%potential_pair_wca    = &
            interpret_generic_logical(trim(context%log%names(15)),fline,.false.,.false.)
       context%log%potential_pair_hs    = &
            interpret_generic_logical(trim(context%log%names(16)),fline,.false.,.false.)
       context%log%swap_acceptance    = &
            interpret_generic_logical(trim(context%log%names(17)),fline,.false.,.false.)
       ! check that logged potentials are initialized
       if( context%log%potential_pair_kf .and. .not. context%potentials%kf%log ) then
          if( context%potentials%kf%enabled ) then
             ! set the log flag if the potential is enabled
             context%potentials%kf%log = .true.
          else
             ! warn the user that the potential cannot be computed
             write(*,'("Warning (",I0,"): KF potential cannot be computed if it has not been enabled")') &
                  line_number
          end if
       end if
       if( context%log%potential_pair_ckf .and. .not. context%potentials%ckf%log ) then
          if( context%potentials%ckf%enabled ) then
             ! set the log flag if the potential is enabled
             context%potentials%ckf%log = .true.
          else
             ! warn the user that the potential cannot be computed
             write(*,'("Warning (",I0,"): CKF potential cannot be computed if it has not been enabled")') &
                  line_number
          end if
       end if
       if( context%log%potential_pair_lj .and. .not. context%potentials%lj%log ) then
          if( context%potentials%lj%enabled ) then
             ! set the log flag if the potential is enabled
             context%potentials%lj%log = .true.
          else
             ! warn the user that the potential cannot be computed
             write(*,'("Warning (",I0,"): LJ potential cannot be computed if it has not been enabled")') &
                  line_number
          end if
       end if
       if( context%log%potential_pair_wca .and. .not. context%potentials%wca%log ) then
          if( context%potentials%wca%enabled ) then
             ! set the log flag if the potential is enabled
             context%potentials%wca%log = .true.
          else
             ! warn the user that the potential cannot be computed
             write(*,'("Warning (",I0,"): WCA potential cannot be computed if it has not been enabled")') &
                  line_number
          end if
       end if
       if( context%log%potential_pair_hs .and. .not. context%potentials%hs%log ) then
          if( context%potentials%hs%enabled ) then
             ! set the log flag if the potential is enabled
             context%potentials%hs%log = .true.
          else
             ! warn the user that the potential cannot be computed
             write(*,'("Warning (",I0,"): HS potential cannot be computed if it has not been enabled")') &
                  line_number
          end if
       end if
       if( context%log%potential_restrain_tr .and. .not. context%potentials%restrain_tr%log ) then
          if( context%potentials%restrain_tr%enabled ) then
             ! set the log flag if the potential is enabled
             context%potentials%restrain_tr%log = .true.
          else
             ! warn the user that the potential cannot be computed
             write(*,'("Warning (",I0,"): restrain_tr potential cannot be computed if it has not been enabled")') &
                  line_number
          end if
       end if
       if( context%log%potential_restrain_or .and. .not. context%potentials%restrain_or%log ) then
          if( context%potentials%restrain_or%enabled ) then
             ! set the log flag if the potential is enabled
             context%potentials%restrain_or%log = .true.
          else
             ! warn the user that the potential cannot be computed
             write(*,'("Warning (",I0,"): restrain_or potential cannot be computed if it has not been enabled")') &
                  line_number
          end if
       end if
       ! determine order to write logged quantities
       context%log%order = 0
       if( context%log%potential )             context%log%order(1)  = index(fline,trim(context%log%names(1))//"=")
       if( context%log%potential_pair )        context%log%order(2)  = index(fline,trim(context%log%names(2))//"=")
       if( context%log%potential_restrain_tr ) context%log%order(3)  = index(fline,trim(context%log%names(3))//"=")
       if( context%log%potential_restrain_or ) context%log%order(4)  = index(fline,trim(context%log%names(4))//"=")
       if( context%log%num_particles )         context%log%order(5)  = index(fline,trim(context%log%names(5))//"=")
       if( context%log%temperature )           context%log%order(6)  = index(fline,trim(context%log%names(6))//"=")
       if( context%log%pressure )              context%log%order(7)  = index(fline,trim(context%log%names(7))//"=")
       if( context%log%volume )                context%log%order(8)  = index(fline,trim(context%log%names(8))//"=")
       if( context%log%lx )                    context%log%order(9)  = index(fline,trim(context%log%names(9))//"=")
       if( context%log%ly )                    context%log%order(10) = index(fline,trim(context%log%names(10))//"=")
       if( context%log%lz )                    context%log%order(11) = index(fline,trim(context%log%names(11))//"=")
       if( context%log%potential_pair_kf )     context%log%order(12) = index(fline,trim(context%log%names(12))//"=")
       if( context%log%potential_pair_ckf )    context%log%order(13) = index(fline,trim(context%log%names(13))//"=")
       if( context%log%potential_pair_lj )     context%log%order(14) = index(fline,trim(context%log%names(14))//"=")
       if( context%log%potential_pair_wca )    context%log%order(15) = index(fline,trim(context%log%names(15))//"=")
       if( context%log%potential_pair_hs )     context%log%order(16) = index(fline,trim(context%log%names(16))//"=")
       ! open the log file
       call open_log(context)
    else
       ! close the now-disabled log file
       call close_log(context)
    end if
  end subroutine interpret_mc_log

  !========================================!
  !     interpret mc.snapshot commands     !
  !========================================!
  subroutine interpret_mc_snapshot(context,fline)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    character(len=*),      intent(inout) :: fline
    ! internal variables
    character(len=100) :: snapshot_prefix
    logical :: recog_flag
    integer :: snapshot_id
    
    write(*,'("mc.snapshot:")')

    ! find the prefix for this snapshot
    snapshot_prefix = interpret_generic_character("prefix",fline,.true.,"none")
    
    ! search for a previously-initialized snapshot
    if( allocated(context%snapshots) ) then
       ! check whether this move has already been initialized
       snapshot_id = 1
       recog_flag = .false.
       ! find matching snapshot to replace parameters
       do while( snapshot_id .le. size(context%snapshots) .and. .not. recog_flag)
          if( context%snapshots(snapshot_id)%prefix .eq. trim(snapshot_prefix) ) then
             recog_flag = .true.
          else
             snapshot_id = snapshot_id + 1
          end if
       end do
       if( snapshot_id .gt. size(context%snapshots) ) then
          write(*,'("expanding snapshots instance...")')
          call expand_snapshots(context)
          context%snapshots(snapshot_id)%prefix = trim(snapshot_prefix)
          call open_snapshot(context,snapshot_id)
       else
          write(*,'("overwriting snapshots instance...")')
          ! close old snapshot instance
          call close_snapshot(context,snapshot_id)
          ! open new snapshot instance
          call open_snapshot(context,snapshot_id)
       end if
    else
       allocate(context%snapshots(1))
       snapshot_id = 1
       context%snapshots(snapshot_id)%prefix = trim(snapshot_prefix)
       call open_snapshot(context,snapshot_id)
    end if

    write(*,'(A,":")') trim(context%snapshots(snapshot_id)%prefix)

    ! default to enabled if the command is used
    context%snapshots(snapshot_id)%enabled = interpret_generic_logical("enabled",fline,.false.,.true.)

    ! only continue reading if snapshot is enabled
    if( context%snapshots(snapshot_id)%enabled ) then
       ! required
       context%snapshots(snapshot_id)%period = interpret_generic_integer("period",fline,.false.,0)
       ! optional
       context%snapshots(snapshot_id)%overwrite = interpret_generic_logical("overwrite",fline,.false.,&
            (context%snapshots(snapshot_id)%period.eq.0)) ! if zero, this is one-time only
       ! also optional
       context%snapshots(snapshot_id)%potential = interpret_generic_logical("potential",fline,.false.,.false.)
       ! write snapshot if system is defined
       if( allocated(context%system%particles%particle) ) then
          call write_snapshot(context,snapshot_id)
          if( context%snapshots(snapshot_id)%period .eq. 0 ) call close_snapshot(context,snapshot_id)
       else if( context%snapshots(snapshot_id)%period .eq. 0 ) then
          write(*,'("Warning (",I0,"): single snapshot requested, but context not initialized")') line_number
       end if
    else
       ! close the disabled snapshot
       call close_snapshot(context,snapshot_id)
    end if
    
  end subroutine interpret_mc_snapshot

  !===================================!
  !     interpret mc.nvt commands     !
  !===================================!
  subroutine interpret_mc_nvt(context,fline)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    character(len=*),      intent(inout) :: fline
    write(*,'("mc.nvt:")')
    context%system%ensemble = "nvt"
    ! required
    context%system%beta = 1.0_P / interpret_generic_real("kT",fline,.true.,0.0_P)
  end subroutine interpret_mc_nvt

  !===================================!
  !     interpret mc.npt commands     !
  !===================================!
  subroutine interpret_mc_npt(context,fline)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    character(len=*),      intent(inout) :: fline
    write(*,'("mc.npt:")')
    context%system%ensemble = "npt"
    ! required
    context%system%beta     = 1.0_P / interpret_generic_real("kT",fline,.true.,0.0_P)
    context%system%pressure = interpret_generic_real("P",fline,.true.,0.0_P)
  end subroutine interpret_mc_npt
  
  !===================================!
  !     interpret mc.run commands     !
  !===================================!
  subroutine interpret_mc_run(context,fline)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    character(len=*),      intent(inout) :: fline
    write(*,'("mc.run:")')
    ! required
    context%parameters%sweeps = interpret_generic_integer("sweeps",fline,.true.,0)
    ! optional
    context%parameters%adjust_period = interpret_generic_integer("adjust",fline,.false.,0)
    context%parameters%reset_period  = interpret_generic_integer("reset",fline,.false.,0)
    ! run the mc driver
    call drive_mc(context)
    ! reset timestep
    context%parameters%timestep = 0
  end subroutine interpret_mc_run
  
end module interpreter_mc
