module util_index
  use globals
  use datatypes
  
contains

  !================================!
  !     convert 1D index to 2D     !
  !================================!
  subroutine lower_to_twodim(in_l,n,i,j)
    implicit none
    ! external variables
    integer, intent(in)  :: in_l, n
    integer, intent(out) :: i, j
    ! internal variables
    integer :: k
    ! start at (2,1)
    k = 1
    i = 2
    j = 1
    do while( k .lt. in_l )
       ! increment 1D
       k = k + 1
       ! increment column
       j = j + 1
       ! increment row if column exceeds row
       !     start next row below the diagonal
       if( j .gt. (i-1) ) then
          i = i + 1
          j = 1
       end if
    end do
  end subroutine lower_to_twodim

  !================================!
  !     convert 2D index to 1D     !
  !================================!
  subroutine twodim_to_lower(in_i,in_j,n,l)
    implicit none
    ! external variables
    integer, intent(in)  :: in_i, in_j, n
    integer, intent(out) :: l
    ! internal variables
    integer :: i, j, x, temp
    i = in_i
    j = in_j
    ! need to be below the diagonal
    if( i .eq. j ) then
       write(*,'("twodim_to_lower: cannot ask for diagonal elements!")')
       l = 0
       return
    elseif( i .lt. j ) then
       temp = i
       i = j
       j = temp
    end if
    l = 0
    ! reach the row before the final one
    x = 0
    do while( x .lt. (i-1) )
       ! increment the 1d index
       l = l + x
       x = x + 1
    end do
    ! add the column
    l = l + j
  end subroutine twodim_to_lower
  
end module util_index
