module interpreter_move
  use globals
  use interpreter_util
  use datatypes
  implicit none

contains

  !=====================================================!
  !     interpret a move command and set parameters     !
  !=====================================================!
  subroutine interpret_move_command(context,fline)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    character(len=*), intent(inout) :: fline
    ! internal variables
    character(len=100) :: cmd_type
    integer :: s, e

    ! extract move type from fline
    s = index(fline,"move.") + 5
    e = index(fline(s:),"(") - 2 + s
    cmd_type = trim(fline(s:e))

    ! switch between different move types
    if( trim(cmd_type) .eq. "displace" ) then
       ! displacement move
       call interpret_generic_move(context,fline)
    else if( trim(cmd_type) .eq. "rotate" ) then
       ! rotation move
       call interpret_generic_move(context,fline)
    else if( trim(cmd_type) .eq. "rescale" ) then
       ! volume rescaling move
       call interpret_generic_move(context,fline)
       call interpret_move_rescale(context,fline)
    else if( trim(cmd_type) .eq. "swap_diameters" ) then
       ! swap diameters of two particles
       call interpret_move_swap_diameters(context,fline)
    else
       write(*,'("Error (",I0,"): move type ",A," not recognized: ",A)') &
            line_number, trim(cmd_type)
       stop
    end if

  end subroutine interpret_move_command

  !========================================!
  !     interpret common move commands     !
  !========================================!
  subroutine interpret_generic_move(context,fline)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    character(len=*),      intent(inout) :: fline
    ! internal variables
    logical :: recog_flag
    character(len=100) :: move_type
    integer :: s, e, move_id

    ! extract move type from fline
    s = index(fline,"move.") + 5
    e = index(fline(s:),"(") - 2 + s
    move_type = trim(fline(s:e))

    if( allocated(context%moves) ) then
       ! check whether this move has already been initialized
       move_id = 1
       recog_flag = .false.
       ! find matching move to replace parameters
       do while( move_id .le. size(context%moves) .and. .not. recog_flag)
          if( context%moves(move_id)%name .eq. move_type ) then
             recog_flag = .true.
          else
             move_id = move_id + 1
          end if
       end do
       if( move_id .gt. size(context%moves) ) then
          write(*,'("expanding moves instance...")')
          call expand_moves(context)
          context%moves(move_id)%name = trim(move_type)
       else
          write(*,'("overwriting moves instance...")')
       end if
    else
       allocate(context%moves(1))
       move_id = 1
       context%moves(move_id)%name = trim(move_type)
    end if

    write(*,'(A,":")') trim(context%moves(move_id)%name)

    context%moves(move_id)%trial_particles = 1
    
    ! read parameters from fline
    context%moves(move_id)%frequency = interpret_generic_real("freq",fline,.true.,0.0_P)
    call generic_nonpositive_error("freq",context%moves(move_id)%frequency)
    
    context%moves(move_id)%magnitude = interpret_generic_real("mag",fline,.true.,0.0_P)
    call generic_nonpositive_error("mag",context%moves(move_id)%magnitude)
    
    context%moves(move_id)%maximum   = interpret_generic_real("max",fline,.false.,&
         context%moves(move_id)%magnitude*10.0_P)
    call generic_nonpositive_error("max",context%moves(move_id)%maximum)
    
    context%moves(move_id)%minimum   = interpret_generic_real("min",fline,.false.,&
         context%moves(move_id)%magnitude/10.0_P)
    call generic_nonpositive_error("min",context%moves(move_id)%minimum)
    
    context%moves(move_id)%nominal_acc = interpret_generic_real("acc",fline,.false.,0.40_P)
    call generic_nonpositive_error("acc",context%moves(move_id)%nominal_acc)
    call generic_gt_one_error("acc",context%moves(move_id)%nominal_acc)

    context%moves(move_id)%adjust_factor = interpret_generic_real("adjust",fline,.false.,0.95_P)
    call generic_nonpositive_error("adjust",context%moves(move_id)%adjust_factor)
    call generic_gt_one_error("adjust",context%moves(move_id)%adjust_factor)
    
  end subroutine interpret_generic_move

  !========================================!
  !     interpret common move commands     !
  !========================================!
  subroutine interpret_move_rescale(context,fline)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    character(len=*),      intent(inout) :: fline
    ! internal variables
    logical :: recog_flag
    character(len=100) :: rescale_char
    integer :: i, move_id
    real(P) :: s

    ! find previously initialized rescale move struct
    move_id = 1
    recog_flag = .false.
    ! find matching move to replace parameters
    do while( move_id .le. size(context%moves) .and. .not. recog_flag)
       if( context%moves(move_id)%name .eq. "rescale" ) then
          recog_flag = .true.
       else
          move_id = move_id + 1
       end if
    end do
    
    context%moves(move_id)%trial_particles = 1
    
    ! read parameters from fline
    rescale_char = interpret_generic_character("dims",fline,.true.,"none")

    ! translate CHARACTER to real(P) probabilities
    if( len(trim(rescale_char)) .gt. 0 .and. len(trim(rescale_char)) .le. 3 &
         .and. verify(trim(rescale_char),"alxyz") .eq. 0 ) then
       if( trim(rescale_char) .eq. "all" ) then
          context%moves(move_id)%uniform_rescale = .true.
       else
          context%moves(move_id)%uniform_rescale = .false.
          context%moves(move_id)%rescale_prob = 0.0_P
          if( scan(rescale_char,"x") .gt. 0 ) context%moves(move_id)%rescale_prob(1) = 1.0_P
          if( scan(rescale_char,"y") .gt. 0 ) context%moves(move_id)%rescale_prob(2) = 1.0_P
          if( scan(rescale_char,"z") .gt. 0 ) context%moves(move_id)%rescale_prob(3) = 1.0_P
          ! normalize
          s = sum( context%moves(move_id)%rescale_prob )
          context%moves(move_id)%rescale_prob = context%moves(move_id)%rescale_prob / s
          ! make cumulative
          do i = 2, 3
             context%moves(move_id)%rescale_prob(i) = context%moves(move_id)%rescale_prob(i) &
                  + context%moves(move_id)%rescale_prob(i-1)
          end do
       end if
    else
       write(*,'("Error (",I0,"): invalid value """,A,""" supplied for ""dims"" (allowed values are all or xyz)")') &
            line_number, trim(rescale_char)
       stop
    end if

  end subroutine interpret_move_rescale

  !========================================!
  !     interpret common move commands     !
  !========================================!
  subroutine interpret_move_swap_diameters(context,fline)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    character(len=*),      intent(inout) :: fline
    ! internal variables
    logical :: recog_flag
    character(len=100) :: move_type
    integer :: s, e, move_id

    ! extract move type from fline
    s = index(fline,"move.") + 5
    e = index(fline(s:),"(") - 2 + s
    move_type = trim(fline(s:e))

    if( allocated(context%moves) ) then
       ! check whether this move has already been initialized
       move_id = 1
       recog_flag = .false.
       ! find matching move to replace parameters
       do while( move_id .le. size(context%moves) .and. .not. recog_flag)
          if( context%moves(move_id)%name .eq. move_type ) then
             recog_flag = .true.
          else
             move_id = move_id + 1
          end if
       end do
       if( move_id .gt. size(context%moves) ) then
          write(*,'("expanding moves instance...")')
          call expand_moves(context)
          context%moves(move_id)%name = trim(move_type)
       else
          write(*,'("overwriting moves instance...")')
       end if
    else
       allocate(context%moves(1))
       move_id = 1
       context%moves(move_id)%name = trim(move_type)
    end if

    write(*,'(A,":")') trim(context%moves(move_id)%name)

    ! read parameters from fline
    context%moves(move_id)%frequency = interpret_generic_real("freq",fline,.true.,0.0_P)
    call generic_nonpositive_error("freq",context%moves(move_id)%frequency)
    context%moves(move_id)%trial_particles = 2
    
  end subroutine interpret_move_swap_diameters
  
end module interpreter_move
