module potential_bias
  use globals
  use datatypes_potentials
  use datatypes
  use snapshot
  implicit none

contains

  !==================================!
  !     return biasing potential     !
  !==================================!
  real(P) function eval_bias(system,potentials)
    implicit none
    ! external variables
    type (system_struct),     intent(inout) :: system
    type (potentials_struct), intent(inout) :: potentials
    ! internal variables
    real(P) :: v, u
    integer :: v_bin, u_bin
    ! evaluate energy of the biasing potential
    !   the potential comes from the histogram
    v = product(system%box_length)
    v_bin = minloc(potentials%bias%v_bins,1,v.lt.potentials%bias%v_bins)-1
    if( v_bin .lt. 1 ) v_bin = 1
    u = system%potentials%tot_potential - system%potentials%tot_potential_bias
    u_bin = minloc(potentials%bias%u_bins,1,u.lt.potentials%bias%u_bins)-1
    if( u_bin .lt. 1 ) u_bin = 1
    eval_bias = potentials%bias%bias(v_bin,u_bin)
  end function eval_bias

  !==================================!
  !     update biasing potential     !
  !==================================!
  subroutine update_bias(context)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    ! internal variables
    character(len=1000) :: filename
    real(P) :: v, u
    integer :: v_bin, u_bin
    ! update the bias and histogram
    v = product(context%system%box_length)
    v_bin = minloc(context%potentials%bias%v_bins,1,v.lt.context%potentials%bias%v_bins)-1
    if( v_bin .lt. 1 ) v_bin = 1
    u = context%system%potentials%tot_potential - context%system%potentials%tot_potential_bias
    u_bin = minloc(context%potentials%bias%u_bins,1,u.lt.context%potentials%bias%u_bins)-1
    if( u_bin .lt. 1 ) u_bin = 1
    context%potentials%bias%hist(v_bin,u_bin) = &
         context%potentials%bias%hist(v_bin,u_bin) + 1
    context%potentials%bias%bias(v_bin,u_bin) = &
         context%potentials%bias%bias(v_bin,u_bin) + context%potentials%bias%f
    if( context%potentials%bias%bias(v_bin,u_bin) &
         .eq. maxval(context%potentials%bias%bias) ) then
       ! keep snapshots at most stable states
       write(filename,'(A,"-",A,"-",A,".xml")') &
            trim(context%potentials%bias%prefix), &
            zero_pad_bin(v_bin), &
            zero_pad_bin(u_bin)
       call write_bias_snapshot(context,filename)
    elseif( context%potentials%bias%bias(v_bin,u_bin) &
         ! or at rare/new states
         .eq. minval(context%potentials%bias%bias, &
         context%potentials%bias%bias.gt.0.0_P) ) then
       write(filename,'(A,"-",A,"-",A,".xml")') &
            trim(context%potentials%bias%prefix), &
            zero_pad_bin(v_bin), &
            zero_pad_bin(u_bin)
       call write_bias_snapshot(context,filename)
    end if
  end subroutine update_bias

  !=========================!
  !     reset histogram     !
  !=========================!
  subroutine reset_hist(context)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    context%potentials%bias%hist = 0
  end subroutine reset_hist

  !========================================!
  !     dump biasing potential to file     !
  !========================================!
  subroutine write_bias(context)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    ! internal variables
    integer :: fid, i, j
    ! increment global fid tracker
    last_fid = last_fid + 1
    ! assign a fild ID that has not been used
    fid = last_fid
    ! open the file
    open(unit=fid,file=trim(context%potentials%bias%prefix)//"_potential.dat",action="write",status="replace")
    do i = 1, context%potentials%bias%n_bins(2)
       do j = 1, context%potentials%bias%n_bins(1)
          write(fid,'(F15.5)',advance='no') context%potentials%bias%bias(j,i)
       end do
       write(fid,*)
    end do
    close(fid)
    ! open the file
    open(unit=fid,file=trim(context%potentials%bias%prefix)//"_v_bins.dat",action="write",status="replace")
    do i = 1, context%potentials%bias%n_bins(1)
       write(fid,'(F15.5)') context%potentials%bias%v_bins(i)
    end do
    close(fid)
    ! open the file
    open(unit=fid,file=trim(context%potentials%bias%prefix)//"_u_bins.dat",action="write",status="replace")
    do j = 1, context%potentials%bias%n_bins(2)
       write(fid,'(F15.5)') context%potentials%bias%u_bins(j)
    end do
    close(fid)
    ! open the file
    open(unit=fid,file=trim(context%potentials%bias%prefix)//"_histogram.dat",action="write",status="replace")
    do i = 1, context%potentials%bias%n_bins(2)
       do j = 1, context%potentials%bias%n_bins(1)
          write(fid,'(I0," ")',advance='no') context%potentials%bias%hist(j,i)
       end do
       write(fid,*)
    end do
    close(fid)
  end subroutine write_bias

  !==========================================!
  !     load biasing potential from file     !
  !==========================================!
  subroutine load_bias(context)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    ! internal variables
    character(len=1000) :: fline, filename
    integer :: fid, i, j, ierr
    real(P) :: delta_v
    ! increment global fid tracker
    last_fid = last_fid + 1
    ! assign a fild ID that has not been used
    fid = last_fid

    ! open the volume bins file
    ierr = 0
    write(filename,'(A,A)') trim(context%potentials%bias%prefix), "_v_bins.dat"
    open(unit=fid,file=trim(filename),action="read",status="old")
    ! count number of lines
    i = 0
    do while( ierr .eq. 0 )
       i = i + 1
       read(fid,'(A)',iostat=ierr) fline
    end do
    rewind(fid)
    ! allocate memory
    context%potentials%bias%n_bins(1) = i - 1
    allocate(context%potentials%bias%v_bins(context%potentials%bias%n_bins(1)))
    context%potentials%bias%v_bins = 0.0_P
    ! loop back over lines and read data
    do i = 1, context%potentials%bias%n_bins(1)
       read(fid,'(F15.5)') context%potentials%bias%v_bins(i)
    end do
    close(fid)

    ! open the energy bins file
    ierr = 0
    write(filename,'(A,A)') trim(context%potentials%bias%prefix), "_u_bins.dat"
    open(unit=fid,file=trim(filename),action="read",status="old")
    ! count number of lines
    j = 0
    do while( ierr .eq. 0 )
       j = j + 1
       read(fid,'(A)',iostat=ierr) fline
    end do
    rewind(fid)
    ! allocate memory
    context%potentials%bias%n_bins(2) = j - 1
    allocate(context%potentials%bias%u_bins(context%potentials%bias%n_bins(2)))
    context%potentials%bias%u_bins = 0.0_P
    ! loop back over lines and read data
    do j = 1, context%potentials%bias%n_bins(2)
       read(fid,'(F15.5)') context%potentials%bias%u_bins(j)
    end do
    close(fid)

    ! allocate memory for potential and histogram
    allocate(context%potentials%bias%bias(context%potentials%bias%n_bins(1),context%potentials%bias%n_bins(2)))
    context%potentials%bias%bias = 0.0_P
    allocate(context%potentials%bias%hist(context%potentials%bias%n_bins(1),context%potentials%bias%n_bins(2)))
    context%potentials%bias%hist = 0

    ! open the biasing potential file
    open(unit=fid,file=trim(context%potentials%bias%prefix)//"_potential.dat",action="read",status="old")
    ! read biasing potential from file
    read(fid,*,iostat=ierr) context%potentials%bias%bias
    close(fid)

    if( ierr .eq. 0 ) then
       write(*,'("Read ",I0," x ",I0," entries from potential file")') &
            context%potentials%bias%n_bins(1), context%potentials%bias%n_bins(2)
    else
       write(*,'("Error (",I0,"): potential file could not be read")') line_number
    end if

  end subroutine load_bias

end module potential_bias
