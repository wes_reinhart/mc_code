module util_mc
  use globals
  use datatypes
  use util_print
  implicit none

contains

  !==========================================================!
  !     return wrapped displacement vector pos_i - pos_j     !
  !==========================================================!
  function displacement(system,r_i,r_j)
    implicit none
    real(P), dimension(3) :: displacement
    ! external variables
    type (system_struct),  intent(in) :: system
    real(P), dimension(:),  intent(in) :: r_i, r_j
    ! internal variables
    real(P), dimension(3) :: box_length
    ! compute wrapped displacement by referencing box size
    displacement = r_j - r_i
    box_length = system%box_length
    do while( any( abs( displacement ) .gt. box_length*0.5_P ) )
       where( abs( displacement ) .gt. box_length*0.5_P )
          displacement = displacement - sign( box_length, displacement )
       end where
    end do
  end function displacement

  !====================================================!
  !     identify the cell id from cell coordinates     !
  !====================================================!
  function cell_coord_to_id(cell_coord,cell_dim)
    implicit none
    integer :: cell_coord_to_id
    ! external variables
    integer, dimension(3), intent(inout) :: cell_coord
    integer, dimension(3), intent(inout) :: cell_dim
    ! internal variables
    integer :: x_stack, y_stack, z_stack
    ! make the math more transparent
    x_stack = cell_coord(1) * ( cell_dim(2)*cell_dim(3) )
    y_stack = cell_coord(2) * cell_dim(3)
    z_stack = cell_coord(3)
    ! convert coordinates to id
    cell_coord_to_id = x_stack + y_stack + z_stack + 1
    if( cell_coord_to_id .gt. product(cell_dim) .or. &
         cell_coord_to_id .lt. 1 ) then
       print*,'problem in cell_coord_to_id:',cell_coord,'->',cell_coord_to_id,&
            '//',cell_dim,'=',product(cell_dim)
    end if
  end function cell_coord_to_id

  !====================================================!
  !     identify the cell coordinates from cell id     !
  !====================================================!
  function cell_id_to_coord(cell_id,cell_dim)
    implicit none
    integer, dimension(3) :: cell_id_to_coord
    ! external variables
    integer, intent(inout) :: cell_id
    integer, dimension(3), intent(inout) :: cell_dim
    ! internal variables
    integer :: leftover
    ! useful counter for dealing with three dimensions
    leftover = cell_id - 1
    ! find how many 2D slices are behind this cell in the list
    ! NOTE: integer math is truncated, so this emulates FLOOR()
    cell_id_to_coord(1) = leftover / ( cell_dim(2)*cell_dim(3) )
    leftover = leftover - cell_id_to_coord(1) * ( cell_dim(2)*cell_dim(3) )
    ! find how many 1D slices are behind this cell in the list
    ! NOTE: integer math is truncated, so this emulates FLOOR()
    cell_id_to_coord(2) = leftover / cell_dim(3)
    leftover = leftover - cell_id_to_coord(2) * cell_dim(3)
    ! nothing left to do in the third dimension
    cell_id_to_coord(3) = leftover
    if( any( cell_id_to_coord .ge. cell_dim ) ) then
       print*,'problem in cell_id_to_coord:',cell_id,'->',cell_id_to_coord
    end if
  end function cell_id_to_coord

  !================================================================!
  !     apply periodic boundary conditions to cell coordinates     !
  !================================================================!
  function wrap_cell_coord(cell_coord,cell_dim)
    implicit none
    integer, dimension(3) :: wrap_cell_coord
    ! external variables
    integer, dimension(3), intent(in) :: cell_coord
    integer, dimension(3), intent(inout) :: cell_dim
    ! internal variables
    wrap_cell_coord = cell_coord
    where( cell_coord .ge. cell_dim )
       wrap_cell_coord = cell_coord - cell_dim
    end where
    where( cell_coord .lt. 0 )
       wrap_cell_coord = cell_dim + cell_coord
    end where
  end function wrap_cell_coord
  ! shouldnt there be a intent(out) for the wrap_cell_coord?

  !==========================================================!
  !      does a deep copy of the system to trial system      !
  !==========================================================!
  subroutine system_deep_copy(context)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    ! internal variables
    integer :: i

    context%trial_system%num_particles = context%system%num_particles
    context%trial_system%box_length = context%system%box_length
    context%trial_system%nominal_lattice_spacing =  context%system%nominal_lattice_spacing
    context%trial_system%ensemble = context%system%ensemble
    if( allocated(context%trial_system%move_prob) ) deallocate(context%trial_system%move_prob)
    allocate(context%trial_system%move_prob(size(context%system%move_prob)))
    context%trial_system%move_prob = context%system%move_prob
    context%trial_system%beta = context%system%beta
    context%trial_system%pressure = context%system%pressure
    context%trial_system%potential = context%system%potential

    if( allocated(context%trial_system%particles%particle) ) then
       do i = 1, size(context%trial_system%particles%particle)
          if( allocated(context%trial_system%particles%particle(i)%orientation) ) &
               deallocate(context%trial_system%particles%particle(i)%orientation)
       end do
       deallocate(context%trial_system%particles%particle)
    end if
    allocate(context%trial_system%particles%particle(size(context%system%particles%particle)))
    do i = 1, size(context%system%particles%particle)
       allocate(context%trial_system%particles%particle(i)%orientation(3,context%system%particles%num_patches))
    end do
    context%trial_system%particles%num_patches = context%system%particles%num_patches
    context%trial_system%particles%diameter_mean_var = context%system%particles%diameter_mean_var
    do i = 1, size(context%trial_system%particles%particle)
       context%trial_system%particles%particle(i)%diameter = context%system%particles%particle(i)%diameter
       context%trial_system%particles%particle(i)%position = context%system%particles%particle(i)%position
       context%trial_system%particles%particle(i)%orientation = context%system%particles%particle(i)%orientation
    end do

    if( allocated(context%trial_system%potentials%potential_pair) ) &
         deallocate(context%trial_system%potentials%potential_pair)
    allocate(context%trial_system%potentials%potential_pair(context%system%num_particles,context%system%num_particles))
    if( allocated(context%trial_system%potentials%potential_restrain_tr) ) &
         deallocate(context%trial_system%potentials%potential_restrain_tr)
    allocate(context%trial_system%potentials%potential_restrain_tr(context%system%num_particles))
    if( allocated(context%trial_system%potentials%potential_restrain_or) ) &
         deallocate(context%trial_system%potentials%potential_restrain_or)
    allocate(context%trial_system%potentials%potential_restrain_or(context%system%num_particles))
    context%trial_system%potentials%tot_potential = context%system%potentials%tot_potential
    context%trial_system%potentials%tot_potential_pair = context%system%potentials%tot_potential_pair
    context%trial_system%potentials%tot_potential_restrain_tr = context%system%potentials%tot_potential_restrain_tr
    context%trial_system%potentials%tot_potential_restrain_or = context%system%potentials%tot_potential_restrain_or
    context%trial_system%potentials%potential_pair = context%system%potentials%potential_pair
    if( context%potentials%restrain_tr%enabled .or. context%potentials%restrain_tr%log ) &
         context%trial_system%potentials%potential_restrain_tr = context%system%potentials%potential_restrain_tr
    if( context%potentials%restrain_or%enabled .or. context%potentials%restrain_or%log ) &
         context%trial_system%potentials%potential_restrain_or = context%system%potentials%potential_restrain_or

    if( allocated(context%trial_system%cell_list%cell_map) ) deallocate(context%trial_system%cell_list%cell_map)
    allocate(context%trial_system%cell_list%cell_map(size(context%system%cell_list%cell_map,1),&
         size(context%system%cell_list%cell_map,2)))
    if( allocated(context%trial_system%cell_list%cell_count) ) deallocate(context%trial_system%cell_list%cell_count)
    allocate(context%trial_system%cell_list%cell_count(size(context%system%cell_list%cell_count)))
    if( allocated(context%trial_system%cell_list%neighbors) ) deallocate(context%trial_system%cell_list%neighbors)
    allocate(context%trial_system%cell_list%neighbors(size(context%system%cell_list%neighbors,1),&
         size(context%system%cell_list%neighbors,2)))
    context%trial_system%cell_list%cell_dim = context%system%cell_list%cell_dim
    context%trial_system%cell_list%side_length = context%system%cell_list%side_length
    context%trial_system%cell_list%max_occupancy = context%system%cell_list%max_occupancy
    context%trial_system%cell_list%min_cell_size = context%system%cell_list%min_cell_size
    context%trial_system%cell_list%cell_map = context%system%cell_list%cell_map
    context%trial_system%cell_list%cell_count = context%system%cell_list%cell_count
    context%trial_system%cell_list%neighbors = context%system%cell_list%neighbors

    if( allocated(context%trial_system%trial%particle_ids) ) deallocate(context%trial_system%trial%particle_ids)
    allocate(context%trial_system%trial%particle_ids(size(context%system%trial%particle_ids)))
    if( allocated(context%trial_system%trial%delta_ids) ) deallocate(context%trial_system%trial%delta_ids)
    allocate(context%trial_system%trial%delta_ids(size(context%system%trial%delta_ids,1),size(context%system%trial%delta_ids,2)))
    if( allocated(context%trial_system%trial%num_delta) ) deallocate(context%trial_system%trial%num_delta)
    allocate(context%trial_system%trial%num_delta(size(context%system%trial%num_delta)))
    if( allocated(context%trial_system%trial%particle) ) then
       do i = 1, size(context%trial_system%trial%particle)
          if( allocated(context%trial_system%trial%particle(i)%orientation) ) &
               deallocate(context%trial_system%trial%particle(i)%orientation)
       end do
       deallocate(context%trial_system%trial%particle)
    end if
    allocate(context%trial_system%trial%particle(size(context%system%trial%particle)))
    do i = 1, size(context%system%trial%particle)
       allocate(context%trial_system%trial%particle(i)%orientation(3,context%system%particles%num_patches))
    end do
    if( allocated(context%trial_system%trial%all_particles) ) then
       do i = 1, size(context%trial_system%trial%all_particles)
          if( allocated(context%trial_system%trial%all_particles(i)%orientation) ) &
               deallocate(context%trial_system%trial%all_particles(i)%orientation)
       end do
       deallocate(context%trial_system%trial%all_particles)
    end if
    allocate(context%trial_system%trial%all_particles(size(context%system%trial%all_particles)))
    do i = 1, size(context%system%trial%all_particles)
       allocate(context%trial_system%trial%all_particles(i)%orientation(3,context%system%particles%num_patches))
    end do
    if( allocated(context%trial_system%trial%potential_pairs) ) deallocate(context%trial_system%trial%potential_pairs)
    allocate(context%trial_system%trial%potential_pairs(size(context%system%trial%potential_pairs,1),&
         size(context%system%trial%potential_pairs,1)))
    if( allocated(context%trial_system%trial%potential_restrain_tr) ) &
         deallocate(context%trial_system%trial%potential_restrain_tr)
    allocate(context%trial_system%trial%potential_restrain_tr(size(context%system%trial%potential_restrain_tr)))
    if( allocated(context%trial_system%trial%potential_restrain_or) ) &
         deallocate(context%trial_system%trial%potential_restrain_or)
    allocate(context%trial_system%trial%potential_restrain_or(size(context%system%trial%potential_restrain_or)))
    if( allocated(context%trial_system%trial%all_potential_pair) ) &
         deallocate(context%trial_system%trial%all_potential_pair)
    allocate(context%trial_system%trial%all_potential_pair(size(context%system%trial%all_potential_pair,1),&
         size(context%system%trial%all_potential_pair,2)))
    if( allocated(context%trial_system%trial%all_potential_restrain_tr) ) &
         deallocate(context%trial_system%trial%all_potential_restrain_tr)
    allocate(context%trial_system%trial%all_potential_restrain_tr(size(context%system%trial%all_potential_restrain_tr)))
    if( allocated(context%trial_system%trial%all_potential_restrain_or) ) &
         deallocate(context%trial_system%trial%all_potential_restrain_or)
    allocate(context%trial_system%trial%all_potential_restrain_or(size(context%system%trial%all_potential_restrain_or)))
    context%trial_system%trial%particle_ids = context%system%trial%particle_ids
    context%trial_system%trial%move_type = context%system%trial%move_type
    context%trial_system%trial%full_recompute = context%system%trial%full_recompute
    context%trial_system%trial%use_cell_list = context%system%trial%use_cell_list
    context%trial_system%trial%delta_ids = context%system%trial%delta_ids
    context%trial_system%trial%num_delta = context%system%trial%num_delta
    context%trial_system%trial%num_trial = context%system%trial%num_trial
    do i = 1, size(context%trial_system%trial%particle)
       context%trial_system%trial%particle(i)%diameter = context%system%trial%particle(i)%diameter
       context%trial_system%trial%particle(i)%position = context%system%trial%particle(i)%position
       context%trial_system%trial%particle(i)%orientation = context%system%trial%particle(i)%orientation
    end do
    do i = 1, size(context%trial_system%trial%all_particles)
       context%trial_system%trial%all_particles(i)%diameter = context%system%trial%all_particles(i)%diameter
       context%trial_system%trial%all_particles(i)%position = context%system%trial%all_particles(i)%position
       context%trial_system%trial%all_particles(i)%orientation = context%system%trial%all_particles(i)%orientation
    end do
    context%trial_system%trial%potential_pairs = context%system%trial%potential_pairs
    context%trial_system%trial%potential_restrain_tr = context%system%trial%potential_restrain_tr
    context%trial_system%trial%potential_restrain_or = context%system%trial%potential_restrain_or
    context%trial_system%trial%all_potential_pair = context%system%trial%all_potential_pair
    if( context%potentials%restrain_tr%enabled .or. context%potentials%restrain_tr%log ) &
         context%trial_system%trial%all_potential_restrain_tr = context%system%trial%all_potential_restrain_tr
    if( context%potentials%restrain_or%enabled .or. context%potentials%restrain_or%log ) &
         context%trial_system%trial%all_potential_restrain_or = context%system%trial%all_potential_restrain_or
    context%trial_system%trial%tot_potential = context%system%trial%tot_potential
    context%trial_system%trial%tot_potential_pair = context%system%trial%tot_potential_pair
    context%trial_system%trial%tot_potential_restrain_tr = context%system%trial%tot_potential_restrain_tr
    context%trial_system%trial%tot_potential_restrain_or = context%system%trial%tot_potential_restrain_or
    
  end subroutine system_deep_copy

end module util_mc
