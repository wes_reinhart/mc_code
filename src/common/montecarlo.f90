module montecarlo
  use globals
  use datatypes
  use compute
  use moves
  use celllist
  use util_moves
  use potential_bias
  implicit none

contains

  !=================================================!
  !     wrapper for monte carlo move acceptance     !
  !=================================================!
  subroutine assess_move(context)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    ! internal variables
    real(P) :: a

    context%system%trial%use_cell_list = .false.
    ! calculate the potential energy of the trial state
    call update_potential(context%trial_system,context%potentials)

    ! get a random number
    call random_number(a)

    ! apply acceptance criteria
    if( a .lt. accept_probability(context) ) then
       ! update acceptance stats with a success
       call increment_moves(context,1)
       call accept_move(context)
    else
       ! update acceptance stats with a failure
       call increment_moves(context,0)
       call reject_move(context)
    end if

  end subroutine assess_move

  !============================================================!
  !     calculate acceptance probability based on ensemble     !
  !============================================================!
  real(P) function accept_probability(context)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    ! internal variables
    real(P) :: U_trial, U_sys, V_trial, V_sys

    U_trial = context%trial_system%potentials%tot_potential
    U_sys   = context%system%potentials%tot_potential

    accept_probability = 0.0_P

    if( context%system%ensemble .eq. "nvt" ) then

       accept_probability = exp( -context%system%beta * ( U_trial - U_sys ) )

    else if( context%system%ensemble .eq. "npt" ) then

       V_trial = product(context%trial_system%box_length)
       V_sys   = product(context%system%box_length)

       ! SHOULD BE (N+1) FOR LOGARITHMIC SCALING MOVES
       ! I AM USING UNIFORM, SO (N) IS CORRECT
       accept_probability = exp( -context%system%beta * ( U_trial - U_sys ) &
            - context%system%beta * context%system%pressure * (V_trial - V_sys) &
            + real(context%system%num_particles,P) * log(V_trial/V_sys) )

    end if

  end function accept_probability

  !================================================!
  !     provides protocol for accepting a move     !
  !================================================!
  subroutine accept_move(context)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    ! internal variables
    integer :: neighbor_count,n, nt, i
    integer, dimension(:), allocatable :: particle_ids, neighbor_ids

    if( context%trial_system%trial%full_recompute ) then

       ! apply box rescale to system
       context%system%box_length         = context%trial_system%box_length
       do i = 1, context%system%num_particles
          context%system%particles%particle(i)%position = context%trial_system%particles%particle(i)%position
          context%system%particles%particle(i)%diameter = context%trial_system%particles%particle(i)%diameter
       end do

       ! update total potentials
       context%system%potentials%tot_potential = &
            context%trial_system%potentials%tot_potential
       context%system%potentials%tot_potential_pair = &
            context%trial_system%potentials%tot_potential_pair
       context%system%potentials%tot_potential_restrain_tr = &
            context%trial_system%potentials%tot_potential_restrain_tr
       context%system%potentials%tot_potential_restrain_or = &
            context%trial_system%potentials%tot_potential_restrain_or
       context%system%potentials%tot_potential_bias = &
            context%trial_system%potentials%tot_potential_bias

       ! update pair potentials
       if( context%potentials%kf%enabled &
            .or. context%potentials%ckf%enabled &
            .or. context%potentials%lj%enabled &
            .or. context%potentials%wca%enabled &
            .or. context%potentials%hs%enabled ) then
          context%system%potentials%potential_pair = &
               context%trial_system%potentials%potential_pair
       end if

       ! update restraint potentials
       if( context%potentials%restrain_tr%enabled ) &
            context%system%potentials%potential_restrain_tr = &
            context%trial_system%potentials%potential_restrain_tr
       if( context%potentials%restrain_or%enabled ) &
            context%system%potentials%potential_restrain_or = &
            context%trial_system%potentials%potential_restrain_or

       ! update cell list
       call copy_cell_list(context%system,context%trial_system)

    else

       nt = context%trial_system%trial%num_trial ! number of trial particles
       allocate(particle_ids(nt))
       allocate(neighbor_ids(nt))

       particle_ids = context%trial_system%trial%particle_ids(1:nt)
       context%system%trial%particle_ids(1:nt) = particle_ids
       context%system%trial%num_trial = nt
       do i = 1, nt
          context%system%trial%particle(i)%position = context%trial_system%trial%particle(i)%position
       end do

       ! push the new particle data to the cell list
       !   THIS MUST HAPPEN BEFORE PARTICLE DATA IS OVERWRITTEN
       call update_cell_list(context%system)
       call update_cell_list(context%trial_system)

       do i = 1, nt
          ! apply displacmement move to system
          context%system%particles%particle(particle_ids(i))%position = &
               context%trial_system%trial%particle(i)%position
          context%trial_system%particles%particle(particle_ids(i))%position = &
               context%trial_system%trial%particle(i)%position

          ! only update orientation for patchy particles
          if( context%system%particles%num_patches .gt. 0 ) then
             context%system%particles%particle(particle_ids(i))%orientation = &
                  context%trial_system%trial%particle(i)%orientation
             context%trial_system%particles%particle(particle_ids(i))%orientation = &
                  context%trial_system%trial%particle(i)%orientation
          end if

          ! update diameters
          context%system%particles%particle(particle_ids(i))%diameter = &
               context%trial_system%trial%particle(i)%diameter
          context%trial_system%particles%particle(particle_ids(i))%diameter = &
               context%trial_system%trial%particle(i)%diameter
       end do

       ! update total potentials
       context%system%potentials%tot_potential = &
            context%trial_system%potentials%tot_potential
       context%system%potentials%tot_potential_pair = &
            context%trial_system%potentials%tot_potential_pair
       context%system%potentials%tot_potential_restrain_tr = &
            context%trial_system%potentials%tot_potential_restrain_tr
       context%system%potentials%tot_potential_restrain_or = &
            context%trial_system%potentials%tot_potential_restrain_or
       context%system%potentials%tot_potential_bias = &
            context%trial_system%potentials%tot_potential_bias

       ! update pair potentials
       if( context%potentials%kf%enabled &
            .or. context%potentials%ckf%enabled &
            .or. context%potentials%lj%enabled &
            .or. context%potentials%wca%enabled &
            .or. context%potentials%hs%enabled ) then
          do i = 1, nt
             do neighbor_count = 1, context%trial_system%trial%num_delta(i)
                neighbor_ids(i) = context%trial_system%trial%delta_ids(neighbor_count,i)

                context%system%potentials%potential_pair(particle_ids(i),neighbor_ids(i)) = &
                     context%trial_system%trial%potential_pairs(neighbor_ids(i),i)
                context%system%potentials%potential_pair(neighbor_ids(i),particle_ids(i)) = &
                     context%trial_system%trial%potential_pairs(neighbor_ids(i),i)

                context%trial_system%potentials%potential_pair(particle_ids(i),neighbor_ids(i)) = &
                     context%trial_system%trial%potential_pairs(neighbor_ids(i),i)
                context%trial_system%potentials%potential_pair(neighbor_ids(i),particle_ids(i)) = &
                     context%trial_system%trial%potential_pairs(neighbor_ids(i),i)
             end do
          end do
       end if

       do i = 1, nt
          ! update restrain potentials
          if( context%potentials%restrain_tr%enabled ) then
             context%system%potentials%potential_restrain_tr(particle_ids(i)) = &
                  context%trial_system%trial%potential_restrain_tr(i)
             context%trial_system%potentials%potential_restrain_tr(particle_ids(i)) = &
                  context%trial_system%trial%potential_restrain_tr(i)
          end if

          if( context%potentials%restrain_or%enabled ) then
             context%system%potentials%potential_restrain_or(particle_ids(i)) = &
                  context%trial_system%trial%potential_restrain_or(i)
             context%trial_system%potentials%potential_restrain_or(particle_ids(i)) = &
                  context%trial_system%trial%potential_restrain_or(i)
          end if
       end do

    end if

  end subroutine accept_move

  !================================================!
  !     provides protocol for rejecting a move     !
  !================================================!
  subroutine reject_move(context)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    ! internal variables
    integer :: i

    if( context%trial_system%trial%full_recompute ) then

       ! reset box size and particle data
       context%trial_system%box_length         = context%system%box_length
       do i = 1, context%system%num_particles
          context%trial_system%particles%particle(i)%position = context%system%particles%particle(i)%position
          context%trial_system%particles%particle(i)%diameter = context%system%particles%particle(i)%diameter
       end do

       ! update total potentials
       context%trial_system%potentials%tot_potential = &
            context%system%potentials%tot_potential
       context%trial_system%potentials%tot_potential_pair = &
            context%system%potentials%tot_potential_pair
       context%trial_system%potentials%tot_potential_restrain_tr = &
            context%system%potentials%tot_potential_restrain_tr
       context%trial_system%potentials%tot_potential_restrain_or = &
            context%system%potentials%tot_potential_restrain_or
       context%trial_system%potentials%tot_potential_bias = &
            context%system%potentials%tot_potential_bias

       ! update pair potentials
       if( context%potentials%kf%enabled &
            .or. context%potentials%ckf%enabled &
            .or. context%potentials%lj%enabled &
            .or. context%potentials%wca%enabled &
            .or. context%potentials%hs%enabled ) then
          context%trial_system%potentials%potential_pair = &
               context%system%potentials%potential_pair
       end if

       ! update restraint potentials
       if( context%potentials%restrain_tr%enabled ) &
            context%trial_system%potentials%potential_restrain_tr = &
            context%system%potentials%potential_restrain_tr
       if( context%potentials%restrain_or%enabled ) &
            context%trial_system%potentials%potential_restrain_or = &
            context%system%potentials%potential_restrain_or

       ! update cell list
       call copy_cell_list(context%trial_system,context%system)

    else

       ! reset total potentials
       context%trial_system%potentials%tot_potential = &
            context%system%potentials%tot_potential
       context%trial_system%potentials%tot_potential_pair = &
            context%system%potentials%tot_potential_pair
       context%trial_system%potentials%tot_potential_restrain_tr = &
            context%system%potentials%tot_potential_restrain_tr
       context%trial_system%potentials%tot_potential_restrain_or = &
            context%system%potentials%tot_potential_restrain_or
       context%trial_system%potentials%tot_potential_bias = &
            context%system%potentials%tot_potential_bias

    end if

  end subroutine reject_move

end module montecarlo
