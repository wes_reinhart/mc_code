program main
  use datatypes
  use io
  use config
  use montecarlo
  use compute
  use celllist
  implicit none
  type (context_struct) :: context

  integer :: sweep_count, particle_count, current_particles
  real(P) :: quicksum, fullsum, oldsize
  integer :: i, j, k
  real(P) :: start_time, current_time, elapsed_time, estimated_time
  real(P) :: pacc_d, pacc_r, pacc_v
  character(len=200) :: logname
  integer :: f_log

  ! start the clock
  call cpu_time(start_time)

  ! setup the mc context
  call setup_mc(context)

  ! REPLACE THIS WITH A GENERIC LOGGER

  ! open the logs
  write(logname,'(3A,"-",I0,".log")') &
       trim(parameters%run_dir),"/",trim(parameters%run_name),parameters%rank
  f_log = 300 + parameters%rank
  
  open(unit=f_log,file=trim(logname),action="write",status="replace")
  write(f_log,'(A1,A14,10A15)') "%","Sweep","Potential",&
       "Pressure","Temperature","Volume","L_X","L_Y","L_Z",&
       "Pacc_d","Pacc_r","Pacc_v"

  ! write the initial configuration
  call write_xml(parameters,system,0)

  ! initial log
  write(f_log,'(I15,10E15.5)') 0,&
       system%thermo%total_potential,&
       system%thermo%pressure,&
       system%thermo%temperature,&
       product(system%box_length),&
       system%box_length,&
       0.0_P,&
       0.0_P,&
       0.0_P
  
  ! let the mc driver run the simulation
  call drive_mc(context)
  
  close(f_log)

  ! save final configuration
  call write_snapshot(parameters,system)
  
  ! validate the quicksum method
  quicksum = system%thermo%total_potential
  system%trial%particle_id = 0
  call update_potential(parameters,system)
  fullsum = system%thermo%total_potential
  write(*,'(I0," : Energy difference between calculation methods is ",E15.5)') &
       parameters%rank, quicksum-fullsum

  write(*,'("Equilibrated magnitude of moves is: ",E15.5," (d), ",E15.5," (r), ",E15.5," (v)")') &
       system%displacement%magnitude, system%rotation%magnitude, system%rescaling%magnitude

  pacc_d = real(system%displacement%total_success,P) &
       / real(system%displacement%total_attempt,P)
  pacc_r = real(system%rotation%total_success,P) &
       / real(system%rotation%total_attempt,P)
  pacc_r = real(system%rescaling%total_success,P) &
       / real(system%rescaling%total_attempt,P)
  write(*,'("Overall acceptance rates are: ",E15.5," (d), ",E15.5," (r), ",E15.5," (v)")') pacc_d, pacc_r, pacc_v
  
  ! write the elapsed time for the run
  call cpu_time(current_time)
  elapsed_time = current_time - start_time
  write(*,'(I0," : run took ",F0.5," s")') parameters%rank, elapsed_time

end program main
