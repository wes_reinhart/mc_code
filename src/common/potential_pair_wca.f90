module potential_pair_wca
  use globals
  use datatypes_potentials
  use datatypes
  implicit none

contains

  !=====================================!
  !     computes WCA pair potential     !
  !=====================================!
  real(P) function eval_wca(potentials,particle_i,particle_j,r_ij_length)
    implicit none
    ! external variables
    type (potentials_struct), intent(inout) :: potentials
    type (particle_data_struct), intent(inout) :: particle_i, particle_j
    real(P), intent(in) :: r_ij_length
    ! internal variables
    real(P) :: d_i, d_j
    real(P) :: r_ij_m6, d_av, sigma_6

    d_i = particle_i%diameter
    d_j = particle_j%diameter

    ! assume no interaction
    eval_wca = 0.0_P

    d_av = (d_i + d_j) * 0.5_P

    ! Weeks-Chandler-Andersen potential
    ! brings up question is radial cutoff measured from the center or surface (i.e. is
    ! the particle a hard sphere or a soft sphere?)
    if( r_ij_length .le. potentials%wca%radial_cutoff*d_av ) then
       r_ij_m6 = r_ij_length**(-6)
       sigma_6 = (potentials%wca%sigma*d_av)**(6)
       eval_wca = 4.0_P * ( sigma_6*sigma_6*r_ij_m6*r_ij_m6 - sigma_6*r_ij_m6 + 0.25_P )
    end if

  end function eval_wca

end module potential_pair_wca
