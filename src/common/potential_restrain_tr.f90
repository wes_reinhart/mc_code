module potential_restrain_tr
  use globals
  use datatypes_potentials
  use datatypes
  implicit none

contains
  
  !===================================================!
  !     return translation part of Einstein field     !
  !===================================================!
  real(P) function eval_restrain_tr(potentials,r_i,r_i_ref)
    implicit none
    ! external variables
    type (potentials_struct), intent(in) :: potentials
    real(P), dimension(:),    intent(in) :: r_i, r_i_ref
    ! internal variables
    real(P), dimension(3) :: r_disp

    ! get displacement from reference lattice
    !   we do not wrap the harmonic springs!
    r_disp = r_i_ref - r_i

    ! evaluate energy
    eval_restrain_tr = potentials%restrain_tr%lambda * sum( r_disp**2.0_P )

  end function eval_restrain_tr

end module potential_restrain_tr
