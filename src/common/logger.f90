module logger
  use globals
  use datatypes_potentials
  use datatypes
  use compute
  implicit none

contains

  !=============================================!
  !     open the log file and write headers     !
  !=============================================!
  subroutine open_log(context)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    ! internal variables
    character(len=100) :: fmt
    integer :: fid, i, next_loc, next_id, nchar
    ! initialize the logger names
    call init_log_names(context)
    ! increment global fid tracker
    last_fid = last_fid + 1
    ! assign a file ID that has not been used
    fid = last_fid    
    context%log%fid = last_fid
    ! open the file
    open(unit=fid,file=trim(context%log%prefix)//".log",action="write",status="replace")
    ! write headers
    write(fid,'("% timestep")',advance='no')
    next_loc = 0
    next_id  = 0
    do i = 1, count(context%log%order .gt. 0)
       next_id = minloc(context%log%order, 1, context%log%order .gt. next_loc)
       next_loc = context%log%order(next_id)
       nchar = len(trim(context%log%names(next_id)))
       nchar = max(nchar,context%log%min_size)
       write(fmt,'("(A",I0,")")') (nchar+1)
       write(fid,fmt,advance='no') trim(context%log%names(next_id))
    end do
    write(fid,*)
  end subroutine open_log

  !====================================!
  !     cleanly close the log file     !
  !====================================!
  subroutine close_log(context)
    ! external variables
    type (context_struct),intent(inout) :: context
    ! close the log
    close(context%log%fid)
  end subroutine close_log
  
  !===================!
  !     write_log     !
  !===================!
  subroutine write_log(context)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    ! internal variables
    character(len=100) :: fmt
    integer :: fid, i, next_loc, next_id, nchar
    ! call the potential compute if the values are requested
    if( any((/&
         context%potentials%kf%log, &
         context%potentials%ckf%log, &
         context%potentials%lj%log, &
         context%potentials%wca%log, &
         context%potentials%hs%log &
         /)) ) call eval_pair_potential(context)
    if( any((/&
         context%potentials%restrain_tr%log, &
         context%potentials%restrain_or%log &
         /)) ) call eval_restraint_potential(context)
    ! write values
    fid = context%log%fid
    write(fid,'("")',advance='no') ! always write timestep
    write(fid,'(I10)',advance='no') context%parameters%timestep
    next_loc = 0
    next_id  = 0
    do i = 1, count(context%log%order .gt. 0)
       next_id = minloc(context%log%order, 1, context%log%order .gt. next_loc)
       next_loc = context%log%order(next_id)
       nchar = len(trim(context%log%names(next_id)))
       nchar = max(nchar,context%log%min_size)
       write(fmt,'("(A",I0,")")') (nchar+1)
       write(fid,fmt,advance='no') &
            var_real_fmt(get_log_value(context,next_id),&
            context%log%precision,&
            (nchar+1))
    end do
    write(fid,*)
    ! flush output so it is visible
    call flush()
  end subroutine write_log

  !================================================!
  !     translate the logger number to a value     !
  !================================================!
  real(P) function get_log_value(context,id)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    integer, intent(in) :: id
    real(P) :: mone, nan
    integer :: move_id
    logical :: recog_flag
    if( id .eq. 1 ) then ! potential
       get_log_value = context%system%potentials%tot_potential
    else if( id .eq. 2 ) then ! potential_pair
       get_log_value = context%system%potentials%tot_potential_pair
    else if( id .eq. 3 ) then ! restrain_tr
       get_log_value = context%system%potentials%tot_potential_restrain_tr
    else if( id .eq. 4 ) then ! restrain_or
       get_log_value = context%system%potentials%tot_potential_restrain_or
    else if( id .eq. 5 ) then ! num_particles
       get_log_value = real(context%system%num_particles,P)
    else if( id .eq. 6 ) then ! temperature
       get_log_value = 1.0_P / context%system%beta
    else if( id .eq. 7 ) then ! pressure
       get_log_value = context%system%pressure
    else if( id .eq. 8 ) then ! volume
       get_log_value = product(context%system%box_length)
    else if( id .eq. 9 ) then ! lx
       get_log_value = context%system%box_length(1)
    else if( id .eq. 10 ) then ! ly
       get_log_value = context%system%box_length(2)
    else if( id .eq. 11 ) then ! lz
       get_log_value = context%system%box_length(3)
    else if( id .eq. 12 ) then ! potential_pair_kf
       get_log_value = context%potentials%kf%tot_potential
    else if( id .eq. 13 ) then ! potential_pair_ckf
       get_log_value = context%potentials%ckf%tot_potential
    else if( id .eq. 14 ) then ! potential_pair_lj
       get_log_value = context%potentials%lj%tot_potential
    else if( id .eq. 15 ) then ! potential_pair_wca
       get_log_value = context%potentials%wca%tot_potential
    else if( id .eq. 16 ) then ! potential_pair_hs
       get_log_value = context%potentials%hs%tot_potential        
    else if( id .eq. 17 ) then !swap_acceptance
       move_id = 1
       recog_flag = .false.
       do while( move_id .le. size(context%moves) .and. .not. recog_flag)
          if( context%moves(move_id)%name .eq. 'swap_diameters' ) then
             recog_flag = .true.
          else
             move_id = move_id + 1
          end if
       end do
       if( recog_flag ) then
          mone = -1.0_P
          nan = sqrt(mone)
          get_log_value = nan
       else
          get_log_value = real(context%moves(move_id)%total_success,P) / real(context%moves(move_id)%total_attempt,P)
       end if
    else
       mone = 1.0_P
       nan = sqrt(mone)
       get_log_value = nan
    end if
  end function get_log_value

  !===========================================================!
  !     compute all pair potentials without the cell list     !
  !===========================================================!
  subroutine eval_pair_potential(context)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    ! internal variables
    integer :: i, j

    context%potentials%kf%tot_potential  = 0.0_P
    context%potentials%ckf%tot_potential = 0.0_P
    context%potentials%lj%tot_potential  = 0.0_P
    context%potentials%wca%tot_potential = 0.0_P
    context%potentials%hs%tot_potential  = 0.0_P
        
    ! loop through particle pairs
    do i = 1, (context%system%num_particles-1)
       do j = i+1, context%system%num_particles
          ! compute pair potentials
          call logger_pair_potential(context%system,context%potentials,i,j)
       end do
    end do

  end subroutine eval_pair_potential

  !==========================================!
  !     compute all restraint potentials     !
  !==========================================!
  subroutine eval_restraint_potential(context)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context

    context%potentials%restrain_tr%tot_potential  = 0.0_P
    context%potentials%restrain_or%tot_potential = 0.0_P

    call logger_restraint_potential(context%system,context%potentials)
    
  end subroutine eval_restraint_potential
  
end module logger
