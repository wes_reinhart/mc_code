!=========================================================!
!     driver for the fundamental Monte Carlo routines     !
!=========================================================!
module driver
  use datatypes
  use util_time
  use util_moves
  use util_mc
  use celllist
  use compute
  use moves
  use montecarlo
  use logger
  use snapshot
  use config
  use potential_bias
  implicit none

contains

  !=====================================================================!
  !     run MC cycles until we are supposed to perform an operation     !
  !=====================================================================!
  subroutine drive_mc(context)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    ! internal variables
    real(P) :: max_diameter
    integer :: sweep_count, current_particles
    integer :: i, j

    call timer_start(context)

    print*,'initializing'
    ! setup the data structures for the run
    call initialize_runtime(context)

    print*,'checking ensembles'
    ! check that moves and ensemble agree
    call check_ensemble_consistency(context)

    print*,'checking rotations'
    ! check for orientations if there are rotations
    call check_rotation_consistency(context)

    print*,'updating range'
    ! update maximum range of all pair_potentials
    max_diameter = 0.0_P
    do i = 1, context%system%num_particles
       if( context%system%particles%particle(i)%diameter .gt. max_diameter) then
          max_diameter = context%system%particles%particle(i)%diameter
       end if
    end do

    call update_potential_pair_max_range(context%potentials,max_diameter)

    print*,'building cell list'

    ! build the first cell list
    call build_cell_list(context,context%system)

    ! calculate the initial energy
    context%system%trial%full_recompute = .true.
    call update_potential(context%system,context%potentials)

    ! set up the trial state
    !   THIS MIGHT ONLY COPY POINTERS, IT IS NOT CLEAR
    ! do a deep copy of the system
    print*,'starting system deep copy'
    call system_deep_copy(context)
    print*,'finished system deep copy'
    !context%trial_system = context%system

    ! write initial state to log
    if( context%log%enabled ) then
       context%parameters%timestep = 0
       call write_log(context)
    end if

    ! write initial snapshot(s)
    if( allocated( context%snapshots ) ) then
       do j = 1, size(context%snapshots)
          if( context%snapshots(j)%enabled .and. &
               context%snapshots(j)%period .gt. 0 ) then
             call write_snapshot(context,j)
          end if
       end do
    end if

    ! loop through simulation sweeps
    do sweep_count = 1, context%parameters%sweeps
       ! loop through particles during a sweep
       current_particles = context%system%num_particles
       do i = 1, current_particles
          ! trial is updated by a trial move
          call perform_move(context)
          ! context is either updated to match trial
          !   OR trial is reset to match context
          call assess_move(context)
       end do

       context%parameters%timestep = sweep_count

       ! print ETA every 10 seconds
       call timer_check(context)

       ! write log
       if( context%log%enabled .and. &
            context%log%period .gt. 0 .and. &
            mod( sweep_count, context%log%period ) .eq. 0 ) then
          call write_log(context)
       end if

       ! write snapshot(s)
       if( allocated( context%snapshots ) ) then
          do j = 1, size(context%snapshots)
             if( context%snapshots(j)%enabled .and. &
                  context%snapshots(j)%period .gt. 0 .and. &
                  mod( sweep_count, context%snapshots(j)%period ) .eq. 0 ) then
                call write_snapshot(context,j)
             end if
          end do
       end if

       ! adjust move magnitudes according to nominal acceptance rate
       if( context%parameters%adjust_period .gt. 0 .and. &
            mod( sweep_count, context%parameters%adjust_period ) .eq. 0 ) then
             call adjust_acceptance(context)
             ! update cell list if displacement magnitude changed dramatically
             call query_cell_list_rebuild(context,context%system)
       end if

       ! do a complete recalculation to avoid numerical problems
       if( context%parameters%reset_period .gt. 0 .and. &
            mod( sweep_count, context%parameters%reset_period ) .eq. 0 ) then
          call reset_potential(context)
       end if

       ! manage the biasing potential
       if( context%potentials%bias%enabled ) call update_bias(context)

       ! periodically write the biasing potential to file, if requested
       if( context%potentials%bias%period .gt. 0 .and. &
            mod( sweep_count, context%potentials%bias%period ) .eq. 0 .and. &
            context%potentials%bias%enabled ) call write_bias(context)

    end do

    ! always write the biasing potential to file at the end
    if( context%potentials%bias%enabled ) call write_bias(context)

    ! print elapsed time
    call print_final_time(context)

  end subroutine drive_mc

end module driver
