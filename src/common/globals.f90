!=================================================================!
!     contains the derived data types used by all the modules     !
!=================================================================!
module globals
  implicit none

  !==============================================!
  !     define the level of precision to use     !
  !==============================================!
  integer, parameter :: P = kind(1.d0)
  integer, parameter :: max_cells = 100

  !=======================================================!
  !     make these constants available to all modules     !
  !=======================================================!
  real(P), parameter :: pi = 4.0_P*atan(1.0_P)
  real(P), parameter :: infinity = real(1e25,P)
  real(P), parameter :: almost_zero = real(1e-10,P)

  !=======================================================!
  !     make these variables available to all modules     !
  !=======================================================!
  integer :: line_number    ! keep track of line number in interpreter
  integer :: last_fid = 100 ! do not overwrite file IDs
  
end module globals
