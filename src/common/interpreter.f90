module interpreter
  use globals
  use datatypes_potentials
  use datatypes
  use interpreter_util
  use interpreter_mc
  use interpreter_potential
  use interpreter_move
  use interpreter_config
  implicit none

contains

  !=====================================!
  !     interpret a generic command     !
  !=====================================!
  subroutine interpret_generic_command(context,fline)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    character(len=*), intent(inout) :: fline
    ! internal variables
    character(len=100) :: cmd_type
    integer :: e

    ! filter out comment lines
    if( scan(trim(fline),'#') .ne. 1 ) then

       ! extract command type from fline
       e = scan(fline,".(") - 1
       if( e .gt. 0 ) then
          cmd_type = trim(fline(1:e))

          ! send command to appropriate interpreter subroutine
          if( cmd_type .eq. "move" .or. cmd_type .eq. "moves" ) then
             call interpret_move_command(context,fline)
          else if( cmd_type .eq. "parameter" .or. cmd_type .eq. "parameters" ) then
             call interpret_parameters_command(context,fline)
          else if( cmd_type .eq. "potential" .or. cmd_type .eq. "potentials" ) then
             call interpret_potential_command(context,fline)
          else if( cmd_type .eq. "mc" ) then
             call interpret_mc_command(context,fline)
          else if( cmd_type .eq. "config" ) then
             call interpret_config_command(context,fline)
          else
             ! report error with line number to user
             if( len(trim(fline)) .gt. 0 ) then
                write(*,'("Error (",I0,"): command type """,A,""" not recognized")') &
                     line_number, trim(cmd_type)
                stop
             end if
          end if

       end if
    end if

  end subroutine interpret_generic_command

  !====================================================!
  !     interpret a set command and set parameters     !
  !====================================================!
  subroutine interpret_parameters_command(context,fline)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    character(len=*), intent(inout) :: fline
    ! internal variables
    character(len=100) :: key
    integer :: s, e

    ! extract set key from fline
    s = index(fline,"parameters.set(") + 15
    e = index(fline(s:),"=") - 2 + s
    key = trim(fline(s:e))

    if( key == "rank" ) then
       context%parameters%rank = interpret_generic_integer("rank",fline,.true.,0)
    else
       write(*,'("Error (",I0,"): parameter ",A," not recognized")') line_number, trim(key)
       stop
    end if

  end subroutine interpret_parameters_command

end module interpreter
