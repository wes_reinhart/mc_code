module util_moves
  use globals
  use datatypes
  use util_print
  implicit none

contains

  !=====================================!
  !     reset local acceptance data     !
  !=====================================!
  subroutine reset_local_moves(move)
    implicit none
    ! external variables
    type (move_struct), intent(inout) :: move
    move%local_success = 0
    move%local_attempt = 0
  end subroutine reset_local_moves

  !===================================!
  !     reset all acceptance data     !
  !===================================!
  subroutine reset_all_moves(move)
    implicit none
    ! external variables
    type (move_struct), intent(inout) :: move
    move%local_success = 0
    move%local_attempt = 0
    move%total_success = 0
    move%total_attempt = 0
  end subroutine reset_all_moves

  !=================================!
  !     update acceptance rates     !
  !=================================!
  subroutine increment_moves(context,outcome)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    integer, intent(in) :: outcome
    ! internal variables
    integer :: i
    i = context%trial_system%trial%move_type
    context%moves(i)%local_success = context%moves(i)%local_success + outcome
    context%moves(i)%total_success = context%moves(i)%total_success + outcome
    context%moves(i)%local_attempt = context%moves(i)%local_attempt + 1
    context%moves(i)%total_attempt = context%moves(i)%total_attempt + 1
  end subroutine increment_moves

  !========================================!
  !     dynamically adjusts step sizes     !
  !========================================!
  subroutine adjust_acceptance(context)
    implicit none
    ! external variables
    type (context_struct), intent(inout) :: context
    ! internal variables
    real(P) :: loc_disp_acc, prev_mag
    integer :: i
    logical :: too_high, too_low, above_min, below_max, adjusted
    ! loop over moves
    do i = 1, size(context%moves)
       adjusted = .false.
       ! get local acceptance ratio
       loc_disp_acc = real(context%moves(i)%local_success,P) / real(context%moves(i)%local_attempt,P)
       ! perform logic
       too_high  = (loc_disp_acc .gt. context%moves(i)%nominal_acc / context%moves(i)%adjust_factor)
       too_low   = (loc_disp_acc .lt. context%moves(i)%nominal_acc * context%moves(i)%adjust_factor)
       above_min = (context%moves(i)%magnitude * context%moves(i)%adjust_factor .gt. context%moves(i)%minimum)
       below_max = (context%moves(i)%magnitude / context%moves(i)%adjust_factor .lt. context%moves(i)%maximum)
       ! attempt to fix
       if( too_low .and. above_min ) then
          adjusted = .true.
          prev_mag = context%moves(i)%magnitude
          ! reduce move magnitude -> increase acceptance
          !   (adjust_factor < 1)
          context%moves(i)%magnitude = context%moves(i)%magnitude * context%moves(i)%adjust_factor
       else if( too_high .and. below_max ) then
          adjusted = .true.
          prev_mag = context%moves(i)%magnitude
          ! increase move magnitude -> reduce acceptance
          !   (adjust_factor < 1)
          context%moves(i)%magnitude = context%moves(i)%magnitude / context%moves(i)%adjust_factor
       end if
       ! reset local acceptance
       call reset_local_moves(context%moves(i))
       ! report to command line
!       if( adjusted ) then
!          write(*,'("ADJUST : ",A," : ",A," -> ",A," (",A,")")') &
!               trim(context%moves(i)%name), &
!               var_real_fmt(prev_mag,3,10), &
!               var_real_fmt(context%moves(i)%magnitude,3,10), &
!               var_real_fmt(loc_disp_acc,3,10)
!       end if
    end do
  end subroutine adjust_acceptance

end module util_moves
