from __future__ import print_function
import numpy as np
import os
import sys
import glob
import commands

#==================================================
# find parameters for the run
#--------------------------------------------------
logdata = np.loadtxt("eos_0.log",comments='%')
logs    = logdata.shape[0]

P = np.loadtxt("eos_P.dat")

cmd = "grep 'natoms=' reference.config.xml"
status, output = commands.getstatusoutput(cmd)
s = output.find("natoms=") + len("natoms=")
output = output[s:].replace('"','')
e = output.find(" ")
N = int(output[:e])

cmd = "grep '^kT = ' generate_eos_script.py"
status, output = commands.getstatusoutput(cmd)
kT = float( output.replace("kT","").replace("=","").replace(";","") )

#==================================================
# preprocess some parameters
#--------------------------------------------------
nblocks = 10
equil_cutoff = 1

beta = 1.0/kT
blocksize = int( (logs-equil_cutoff) / nblocks )

rho_arr = np.zeros((nblocks,len(P)))
Z_arr   = np.zeros((nblocks,len(P)))

#==================================================
# process eos
#--------------------------------------------------
for i in range(len(P)):
    logdata = np.loadtxt("eos_"+str(i)+".log",comments='%')
    V   = logdata[:,3] * logdata[:,4] * logdata[:,5]
    rho = N / V
    Z   = beta * P[i] / rho 
    # calculate block averages
    for j in range(nblocks):
        lo = equil_cutoff + j*blocksize
        hi = equil_cutoff + (j+1)*blocksize
        rho_arr[j,i] = np.mean(rho[lo:hi])
        Z_arr[j,i]   = np.mean(Z[lo:hi])
# write to file
np.savetxt("rho.dat",rho_arr,fmt='%25.15e',header="% block averages N/V")
np.savetxt("Z.dat",Z_arr,fmt='%25.15e',header="% block averages beta*P/rho")
