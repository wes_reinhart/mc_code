#!/bin/bash
#SBATCH -N 1
#SBATCH --ntasks-per-node=1
#SBATCH -t 1:00:00
#SBATCH -p cpu

# print diagnostic information
host=`hostname`
echo "running job ${run_name} on node ${host} (job ${SLURM_JOB_ID}):"
python analyze_eos.py
