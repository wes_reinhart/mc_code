from __future__ import print_function

# load python modules
import random
import numpy as np
import re
import os
import shutil

def log_glq(N):
    filename = 'N'+str(N)+'.glq'
    [x,w] = glq = np.polynomial.legendre.leggauss(int(N))

    # c = float(1e-6)
    c = float(1e-3)
    y = ( x + 1 ) / 2.0 * ( np.log(c+1.0) - np.log(c) ) + np.log(c)
    z = 1 - ( np.exp(y) - c )
    x = ( z * 2.0 ) - 1.0

    # this is the exponential part AND the change of interval part
    w2 = ( np.log(c+1.0) - np.log(c) ) / 2.0 * np.exp(y)

    fid = open(filename,'w+')
    print('%           weight                    point',file=fid)
    for i in range(int(N)):
        print('%.16f     %.16f     %.16f' % (w[i],x[i],w2[i]),file=fid)
    fid.close()
    print('generation of %s complete' % filename)

    # only need abscissa during this script
    #      weights are saved to file
    return x

def setup_run_dir(run_name):
    work_dir = os.getcwd() + "/"
    data_dir = "/home/wfr/Data/ckf/"
    label = -1
    for x in next(os.walk(data_dir))[1]:
        if x.find(run_name+"-") == 0:
            dash = x.rfind('-') + 1
            n = int( x[dash:len(x)] )
            if n > label:
                label = n
    print( "Last run was %d" % label )
    # make run directory and copy configuration file there
    if label < 0:
        # read parameters file from the parent directory
        # paramsFile = run_name + ".params"
        run_dir = data_dir + run_name + "-" + str(0) + "/"
        # append run number to keep new data separate
        os.mkdir( run_dir )
        # shutil.copy2( paramsFile, run_dir )
    else: # continuing an old run
        # figure out what the last run was
        label += 1
        numeral = label
        # append run number to keep new data separate
        run_dir = data_dir + run_name + "-" + str(label)   + "/"
        old_dir = data_dir + run_name + "-" + str(label-1) + "/"
        # read params file either from parent directory (changing params)
        # or from last run (keeping the same params)
        # paramsFile = work_dir + run_name + ".params"
        # if not os.path.isfile( paramsFile ):
        #     paramsFile = old_dir + run_name + ".params"
        # make the run directory
        os.mkdir( run_dir )
        # copy the necessary old data to the new run directory
        for f in os.listdir( old_dir ):
            if ".config.xml" in f:
                # n_start = f.find('.config.xml')
                # n_end = f.rfind('-') + 1
                # rep_num = int( f[n_end:n_start] )
                # if rep_num < num_ranks:
                print('copying %s to %s' % (f,run_dir))
                shutil.copy2( old_dir + f, run_dir )
        # shutil.copy2( paramsFile, run_dir )
    return run_dir

#====================================================================================================
#                                          MAIN PROGRAM
#====================================================================================================
np.random.seed()
mode_txt = "--mode=gpu"

# MUST LEAVE THE SPACES BEFORE AND AFTER EQUALS SIGN:
run_name = "sljm_ti_hcp3_d1.06_a0.55_T0.15"

P = 4.00

alpha = 0.55;
omega = 50.0;

# find interaction range
x = run_name.find("_d") + 2
if x < 0:
    print("No valid delta given")
    sys.exit(0)
else:
    y = run_name.find("_",x)
    if y < 0:
        y = len(run_name)
    delta = float(run_name[x:y])

# find patch fraction
x = run_name.find("_a") + 2
if x < 0:
    print("No valid alpha given")
    sys.exit(0)
else:
    y = run_name.find("_",x)
    if y < 0:
        y = len(run_name)
    alpha = float(run_name[x:y])

if delta == 1.20:
    rcut = 3.1
    # delta = 1.20
    Md = 2.213;
    Mr = 0.11;
    req = 0.93;
elif delta == 1.10:
    rcut = 2.1
    # delta = 1.10
    Md = 1.878;
    Mr = 0.0462;
    req = 0.9973;
elif delta == 1.06:
    rcut = 1.6
    # delta = 1.06
    Md  = 1.8341;
    Mr  = 0.0302;
    req = 1.0043;    
else:
    print("Error: No valid delta given in run name.")
    sys.exit(0)

print("delta = %f: Md = %f, Mr = %f, req = %f" % (delta, Md, Mr, req))

# find temperature
x = run_name.find("_T") + 2
y = run_name.find("_",x)
if y < 0:
    y = len(run_name)
T = float(run_name[x:y])

print("P = %f, T = %f" % (P, T))

nequil  = int(1e5)
nsteps  = int(1e5)

nwrites = int(1e4)
nreset  = int(1e1)

lambda_tr = float(5e4)
lambda_or = float(5e3)

lj_mode_txt = "no_shift"

num_A1    = 5
num_A2    = 15

nblocks = 100

mi = 0.1   # moment of inertia
dt_equil = 0.005 # timestep for equilibration
dt_int   = 0.001 # timestep for integration

# compute the abscissa and weights
x = log_glq(num_A2)

#=======================================#
#     EQUILIBRATE THE CONFIGURATION     #
#=======================================#
hoomd.context.initialize(mode_txt)

# for N=256, ncell=4; for N=1372, ncell=7
# system = hoomd.init.create_lattice(unitcell=hoomd.lattice.fcc(a=1.462,type_name='C'),n=7) # rho = 1.28
system = deprecated.init.read_xml(filename=configfile,time_step=0)
for p in system.particles:
    p.mass = 1.0
    p.moment_inertia = (mi, mi, mi) # 2/5 m r^2 = 2/20 m sigma^2
    p.image = (0,0,0)
thermalize(system,T)

snap = system.take_snapshot(all=True)
snap.particles.angmom[:] = 1
system.restore_snapshot(snap)

nl = md.nlist.cell()
nl.set_params() # (r_buff=r_buff)
all = hoomd.group.all()

# set up anisotropic pair potential
sljm = md.pair.sljm(r_cut=rcut,nlist=nl)
sljm.pair_coeff.set('C','C',epsilon=1.0,sigma=1.0,Md=Md,Mr=Mr,req=req,omega=omega,alpha=alpha)

# set up LJ potential
# lj = md.pair.lj(r_cut=rcut,nlist=nl)
# lj.pair_coeff.set('C','C',epsilon=1.0,sigma=1.0)
# lj.set_params(mode='shift')

integrator = md.integrate.mode_standard(dt=dt_equil)

# keep everything well behaved during equilibration
update_zm = md.update.zero_momentum(period=int(nequil/nreset),phase=-1);
update_rt = md.update.rescale_temp(kT=T,period=int(nequil/nreset),phase=-1);

if nequil > 0:
    # thermalize
    langevin = md.integrate.langevin(group=all, kT=T, seed=np.random.randint(1e8))
    hoomd.run(int(nequil))
    langevin.disable()
    del langevin

    # pressurize
    coupled_npt = md.integrate.npt(group=all, kT=T, tau=1.4, P=P, tauP=1.4, couple='xyz')
    hoomd.run(int(nequil))
    coupled_npt.disable()
    del coupled_npt

    # equilibrate
    uncoupled_npt = md.integrate.npt(group=all, kT=T, tau=1.4, P=P, tauP=1.4, couple='none')
    hoomd.run(int(nequil))
    uncoupled_npt.disable()
    del uncoupled_npt

# disable updaters
update_zm.disable()
update_rt.disable()
del update_zm, update_rt

# save equilibrated configuration
deprecated.dump.xml(group=all,filename="equil.config.xml", all=True)

#=================#
#     RUN dA1     #
#=================#
for i in range(num_A1):

    hoomd.context.initialize(mode_txt)
    system = deprecated.init.read_xml(filename="equil.config.xml",time_step=0)

    for p in system.particles:
        p.mass = 1.0
        p.moment_inertia = (mi, mi, mi) # 2/5 m r^2 = 2/20 m sigma^2
        p.image = (0,0,0)
    thermalize(system,T)

    nl = md.nlist.cell()
    nl.set_params() # (r_buff=r_buff)
    all = hoomd.group.all()

    # set up LJ potential
    # lj = md.pair.lj(r_cut=rcut,nlist=nl)
    # lj.pair_coeff.set('C','C',epsilon=1.0,sigma=1.0)
    # lj.set_params(mode=lj_mode_txt)
    # lj.disable(log=True)

    # set up anisotropic pair potential
    sljm = md.pair.sljm(r_cut=rcut,nlist=nl)
    sljm.pair_coeff.set('C','C',epsilon=1.0,sigma=1.0,Md=Md,Mr=Mr,req=req,omega=omega,alpha=alpha)
    sljm.disable(log=True)

    k_tr = 2.0 * lambda_tr
    res_tr = md.restrain.position(group=all,kx=k_tr,ky=k_tr,kz=k_tr)

    k_or = 2.0 * lambda_or    
    res_or = md.restrain.orientation(group=all,k=k_or)
    
    md.integrate.mode_standard(dt=dt_int)
    md.integrate.langevin(group=all, kT=T, seed=np.random.randint(1e8))

    # hoomd.analyze.log(filename="dA1_"+str(i)+".log", period=int(nsteps/nwrites), quantities=['potential_energy','kinetic_energy','pair_lj_energy','restraint_position_energy','restraint_orientation_energy'], overwrite=True, header_prefix='%')
    hoomd.analyze.log(filename="dA1_"+str(i)+".log", period=int(nsteps/nwrites), quantities=['potential_energy','kinetic_energy','aniso_pair_ckf_energy','restraint_position_energy','restraint_orientation_energy'], overwrite=True, header_prefix='%')

    # deprecated.dump.xml(group=all,filename="dA1_"+str(i), position=True, orientation=True, period=int(nsteps/nsnaps))

    hoomd.run(nsteps)
    
    deprecated.dump.xml(group=all,filename="dA1_"+str(i)+".config.xml", all=True)

#====================================#
#     preprocess some parameters     #
#====================================#
beta = 1.0/T
equil_cutoff = int( nwrites / 2 )
blocksize = int( np.floor( nwrites - equil_cutoff ) / nblocks )
U_solid = np.zeros((nwrites,num_A1))
tr_disp = np.zeros((nwrites,num_A2))
or_disp = np.zeros((nwrites,num_A2))
sol_arr = np.zeros((nblocks+1,num_A1)) * np.nan # need an extra row for U_lattice
tr_arr = np.zeros((nblocks,num_A2)) * np.nan
or_arr = np.zeros((nblocks,num_A2)) * np.nan

#=====================#
#     process dA1     #
#=====================#
for i in range(num_A1):
    # load ckf potential
    logdata = np.loadtxt("dA1_"+str(i)+".log",comments='%')
    U_solid[:,i] = logdata[:,3]
    U_lattice = U_solid[0,i]
    sol_arr[0,0] = U_lattice
    # calculate block averages
    for j in range(nblocks):
        lo = equil_cutoff + j*blocksize
        hi = equil_cutoff + (j+1)*blocksize
        sol_arr[j+1,i] = np.mean(np.exp(-beta*(U_solid[lo:hi,i]-U_lattice)))
# write to file
np.savetxt("sol.dat",sol_arr,fmt='%25.15e',header="% block averages <exp[-B*(U_solid-U_lattice)]>")

#=================#
#     RUN dA2     #
#=================#
for i in range(num_A2):

    # change interval to [0,1]
    local_lambda = (x[i] + 1.0) * 0.50
    
    hoomd.context.initialize(mode_txt)
    system = deprecated.init.read_xml(filename="equil.config.xml",time_step=0)

    for p in system.particles:
        p.mass = 1.0
        p.moment_inertia = (mi, mi, mi) # 2/5 m r^2 = 2/20 m sigma^2
        p.image = (0,0,0)
    thermalize(system,T)

    nl = md.nlist.cell()
    nl.set_params() # (r_buff=r_buff)
    all = hoomd.group.all()

    # set up LJ potential
    # lj = md.pair.lj(r_cut=rcut,nlist=nl)
    # lj.pair_coeff.set('C','C',epsilon=1.0,sigma=1.0)
    # lj.set_params(mode=lj_mode_txt)

    sljm = md.pair.sljm(r_cut=rcut,nlist=nl)
    sljm.pair_coeff.set('C','C',epsilon=1.0,sigma=1.0,Md=Md,Mr=Mr,req=req,omega=omega,alpha=alpha)
    
    k_tr = 2.0 * lambda_tr * (1 - local_lambda)
    res_tr = md.restrain.position(group=all,kx=k_tr,ky=k_tr,kz=k_tr)
    
    k_or = 2.0 * lambda_or * (1 - local_lambda)
    res_or = md.restrain.orientation(group=all,k=k_or)

    print('local_lambda = ',local_lambda,'; k_tr = ',k_tr,', k_or = ',k_or)
    
    md.integrate.mode_standard(dt=dt_int)
    md.integrate.langevin(group=all, kT=T, seed=np.random.randint(1e8))

    # hoomd.analyze.log(filename="dA2_"+str(i)+".log", period=int(nsteps/nwrites), quantities=['potential_energy','kinetic_energy','pair_lj_energy','restraint_position_energy','restraint_orientation_energy'], overwrite=True, header_prefix='%')
    hoomd.analyze.log(filename="dA2_"+str(i)+".log", period=int(nsteps/nwrites), quantities=['potential_energy','kinetic_energy','aniso_pair_ckf_energy','restraint_position_energy','restraint_orientation_energy'], overwrite=True, header_prefix='%')

    # deprecated.dump.xml(group=all,filename="dA2_"+str(i), position=True, orientation=True, period=int(nsteps/nsnaps))

    hoomd.run(nsteps)
    
    deprecated.dump.xml(group=all,filename="dA2_"+str(i)+".config.xml", all=True)

    #=====================#
    #     process dA2     #
    #=====================#
    # load ckf potential
    logdata = np.loadtxt("dA2_"+str(i)+".log",comments='%')
    tr_disp[:,i] = logdata[:,4] / (k_tr * 0.5)
    or_disp[:,i] = logdata[:,5] / (k_or * 0.5)
    # calculate block averages
    for j in range(nblocks):
        lo = equil_cutoff + j*blocksize
        hi = equil_cutoff + (j+1)*blocksize
        tr_arr[j,i] = np.mean(tr_disp[lo:hi,i])
        or_arr[j,i] = np.mean(or_disp[lo:hi,i])

# write to file
np.savetxt("tr.dat",tr_arr,fmt='%25.15e',header="% block averages <restraint_position_energy / lambda_tr>")
np.savetxt("or.dat",or_arr,fmt='%25.15e',header="% block averages <restraint_orientation_energy / lambda_or>")
