mol delete all

axes location off
display rendermode GLSL
display projection orthographic

set dir [concat "./bin/"]
set file_name "swaps"
set suff [concat ".xml"]

set first 0

set iter 0
set skip 100
set frame [format "%010d" [expr $first+$iter*$skip]]

set fileX "${dir}${file_name}.${frame}${suff}"
echo $fileX

mol new
mol rename top "${file_name}"
mol addfile $fileX type hoomd waitfor all
pbc wrap -center origin
pbc box -center origin

mol modstyle 0 top VDW 1.0 25
mol modselect 0 top "all"
mol modcolor 0 top colorID 6
mol modmaterial 0 top "AOChalky"

mol addrep top
mol modstyle 1 top VDW 1.0 25
mol modselect 1 top "type P"
mol modcolor 1 top colorID 23
mol modmaterial 1 top "Opaque"

display resetview

if { $skip > 0 } {
    incr iter
    set next_frame_exists 1
    while { $next_frame_exists == 1 } {
	set frame [format "%010d" [expr $first+$iter*$skip]]
	set fileX "${dir}${file_name}.${frame}${suff}"
	if { [file exists $fileX] == 1 } {
	    echo $fileX
	    mol addfile $fileX type hoomd waitfor all
	    pbc wrap -center origin
	    pbc box -center origin
	} else {
	    echo "Requested file ${fileX} does not exist, exiting..."
	    set next_frame_exists 0
	    continue
	}
	incr iter
    }
}
