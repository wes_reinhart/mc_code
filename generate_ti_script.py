from __future__ import print_function
import sys
import os
import shutil
import numpy as np
import commands

def log_glq(N):
    filename = 'N'+str(N)+'.glq'
    [x,w] = glq = np.polynomial.legendre.leggauss(int(N))

    # c = float(1e-6)
    c = float(1e-3)
    y = ( x + 1 ) / 2.0 * ( np.log(c+1.0) - np.log(c) ) + np.log(c)
    z = 1 - ( np.exp(y) - c )
    x = ( z * 2.0 ) - 1.0

    # this is the exponential part AND the change of interval part
    w2 = ( np.log(c+1.0) - np.log(c) ) / 2.0 * np.exp(y)

    fid = open(filename,'w+')
    print('%           weight                    point',file=fid)
    for i in range(int(N)):
        print('%.16f     %.16f     %.16f' % (w[i],x[i],w2[i]),file=fid)
    fid.close()
    print('generation of %s complete' % filename)

    # only need abscissa during this script
    #      weights are saved to file
    return x

#==================================================
# set parameters for the run
#--------------------------------------------------
run_name   = "ti_ct_d1.06_a0.45_T0.15"
configfile = "/home/wfr/Data/mc_ckf/relax_ct-0/relax.config.xml";

# system parameters
N = 192;
kT = 0.15;

# ckf potential parameters
rcut  = 1.6;
Md    = 1.8341;
Mr    = 0.0302;
req   = 1.0043;
omega = 50;
alpha = 0.45;

# spring constants
lambda_tr = float(5e4)
lambda_or = float(5e3)

# quadrature points
num_A1    = 5
num_A2    = 15

# run parameters
equil  = 1e5;
sweeps = 1e6;
reset  = 1e2;
adjust = 10;
logs = 1e3;
snapshots = 1e2;

#==================================================
# create a run directory
#--------------------------------------------------
data_dir = "/home/wfr/Data/mc_ckf/"

# assign an unused label
label = -1
for x in next(os.walk(data_dir))[1]:
    if x.find(run_name+"-") == 0:
        dash = x.rfind('-') + 1
        n = int( x[dash:len(x)] )
        if n > label:
            label = n
            
print( "Last run was %d" % label )

# make the directory
run_dir = data_dir + run_name + "-" + str(label+1) + "/"
os.mkdir(run_dir)

# copy the job script there
shutil.copy2("run_mc.sh",run_dir)

# copy this script there
shutil.copy2("generate_ti_script.py",run_dir)

# copy the analysis scripts there
shutil.copy2("analyze_ti.py",run_dir)
shutil.copy2("analyze_ti.sh",run_dir)

# copy the initial configuration there
shutil.copy2(configfile,run_dir+"reference.config.xml")

# compute the abscissa and weights
x = log_glq(num_A2)

# copy the quadrature file there
quad_name = "N%d.glq" % num_A2
shutil.copy2(quad_name,run_dir)

# move to the run directory so slurm.out is written there
os.chdir(run_dir)

slurm_job_ids = []

#==================================================
# dA1 calculations
#--------------------------------------------------
for i in range(num_A1):
    # open script    
    scriptname = "input_dA1_%d.mc" % i;
    script = open(run_dir+scriptname,'w+');
    # initial setup
    print("config.read(configfile=%s,npatch=2)" % configfile,file=script);
    print("mc.nvt(kT=%f)" % ( kT ),file=script);
    # first equilibrate with only the restraints
    print("potential.restrain_tr(lambda=%e,log=True)" % lambda_tr,file=script);
    print("potential.restrain_or(lambda=%e,log=True)" % lambda_or,file=script);
    # moves might need to be very small
    print("move.displace(freq=%d,mag=1e-1,max=1e0,min=1e-10" % N,file=script);
    print("move.rotate(freq=%d,mag=0.33,max=1.57,min=1e-10" % N,file=script);
    # need "lattice energy" from real potential
    print("potential.pair_ckf(enabled=False,log=True,rcut=%f,Md=%f,Mr=%f,req=%f,omega=%f,alpha=%f)" % (rcut,Md,Mr,req,omega,alpha),file=script);
    # log all the quantities
    print("mc.log(prefix=dA1_%d_equil,period=%d,potential=True,potential_pair_ckf=True,potential_restrain_tr=True,potential_restrain_or=True,lx=True,ly=True,lz=True)" % (i,equil) ,file=script);
    # equilibrate moves (and configuration)
    print("mc.run(sweeps=%d,reset=%d,adjust=%d)" % (equil, reset, adjust),file=script);
    # log all the quantities
    print("mc.log(prefix=dA1_%d_prod,period=%d,potential=True,potential_pair_ckf=True,potential_restrain_tr=True,potential_restrain_or=True,lx=True,ly=True,lz=True)" % (i,sweeps/logs) ,file=script);
    # set up some snapshots (for diagnostics)
    print("mc.snapshot(prefix=dA1_%d.config,period=%d,overwrite=True)" % (i,sweeps/snapshots) ,file=script);
    # production run
    print("mc.run(sweeps=%d,reset=%d)" % (sweeps, reset),file=script);
    script.close()

    #==================================================
    # queue the run
    #--------------------------------------------------
    # sbatch the job script (must be bash)
    this_proc_name = run_name + "-" + str(label+1) + ":dA1-" + str(i)
    cmd = "sbatch --job-name=%s run_mc.sh %s %s %s" % (this_proc_name,scriptname,run_name,run_dir)
    # print("submitting dA1 job with command: %s" % cmd)
    status, output = commands.getstatusoutput(cmd)
    if status != 0:
        print("Error! %s" % output)
    jobnum = output.replace("Submitted batch job ","")
    slurm_job_ids.append(jobnum)
    
#==================================================
# dA2 calculations
#--------------------------------------------------
for i in range(num_A2):
    # calculate spring constants
    local_lambda = (x[i] + 1.0) * 0.50
    local_lambda_tr = lambda_tr * (1 - local_lambda)
    local_lambda_or = lambda_or * (1 - local_lambda)
    # open script
    scriptname = "input_dA2_%d.mc" % i;
    script = open(run_dir+scriptname,'w+');
    # initial setup
    print("config.read(configfile=%s,npatch=2)" % configfile,file=script);
    print("mc.nvt(kT=%f)" % ( kT ),file=script);
    # equilibrate with all potentials now
    print("potential.pair_ckf(log=True,rcut=%f,Md=%f,Mr=%f,req=%f,omega=%f,alpha=%f)" % (rcut,Md,Mr,req,omega,alpha),file=script);
    print("potential.restrain_tr(lambda=%e,log=True)" % local_lambda_tr,file=script);
    print("potential.restrain_or(lambda=%e,log=True)" % local_lambda_or,file=script);
    # moves might need to be very small
    print("move.displace(freq=%d,mag=1e-1,max=1e0,min=1e-10" % N,file=script);
    print("move.rotate(freq=%d,mag=0.33,max=1.57,min=1e-10" % N,file=script);
    # equilibrate moves (and configuration)
    print("mc.run(sweeps=%d,reset=%d,adjust=%d)" % (equil, reset, adjust),file=script);
    # log all the quantities
    print("mc.log(prefix=dA2_%d,period=%d,potential=True,potential_pair_ckf=True,potential_restrain_tr=True,potential_restrain_or=True,lx=True,ly=True,lz=True)" % (i,sweeps/logs) ,file=script);
    # set up some snapshots (for diagnostics)
    print("mc.snapshot(prefix=dA2_%d.config,period=%d,overwrite=True)" % (i,sweeps/snapshots) ,file=script);
    # production run
    print("mc.run(sweeps=%d,reset=%d)" % (sweeps, reset),file=script);
    script.close()

    #==================================================
    # queue the run
    #--------------------------------------------------
    # sbatch the job script (must be bash)
    this_proc_name = run_name + "-" + str(label+1) + ":dA2-" + str(i)
    cmd = "sbatch --job-name=%s run_mc.sh %s %s %s" % (this_proc_name,scriptname,run_name,run_dir)
    # print("submitting dA2 job with command: %s" % cmd)
    status, output = commands.getstatusoutput(cmd)
    if status != 0:
        print("Error! %s" % output)
    jobnum = output.replace("Submitted batch job ","")
    slurm_job_ids.append(jobnum)

#==================================================
# calculate necessary values
#--------------------------------------------------
print("list of all jobs:")
print(slurm_job_ids)

cmd = "sbatch --depend=afterany"
for job in slurm_job_ids:
    cmd += ":" + job
cmd += " analyze_ti.sh"
print("submitting analysis job with command: %s" % cmd)
status, output = commands.getstatusoutput(cmd)
