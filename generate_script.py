from __future__ import print_function
import sys
import os
import shutil
import commands

#==================================================
# set parameters for the run
#--------------------------------------------------
configfile = "/home/wfr/Data/lattices/ckf_N192_hcp3_1.30.xml";

# system parameters
N = 192;
kT = 0.15;
P = 0.20;

# ckf potential parameters
rcut  = 1.6;
Md    = 1.8341;
Mr    = 0.0302;
req   = 1.0043;
omega = 50;
alpha = 0.45;

rescale_mode = "xyz"
# rescale_mode = "all"

# run parameters
equil  = 1e2;
adjust = 1;
snapshots = 1e2;
logs   = 1e2;

# rescale_mode = "all"; # box scales uniformly
rescale_mode = "xyz"; # box dimensions scale independently

#==================================================
# get command-line arguments
#--------------------------------------------------
try:
    run_name = sys.argv[1]
except:
    print("Error: must provide a run name")
    sys.exit(0)

#==================================================
# create a run directory
#--------------------------------------------------
data_dir = "/home/wfr/Data/mc_ckf/"

# assign an unused label
label = -1
for x in next(os.walk(data_dir))[1]:
    if x.find(run_name+"-") == 0:
        dash = x.rfind('-') + 1
        n = int( x[dash:len(x)] )
        if n > label:
            label = n
            
print( "Last run was %d" % label )

# make the directory
run_dir = data_dir + run_name + "-" + str(label+1) + "/"
os.mkdir(run_dir)

# copy the job script there
shutil.copy2("run_mc.sh",run_dir)

# copy the starting configuration
shutil.copy2(configfile,run_dir+"reference.config.xml")

os.chdir(run_dir)

#==================================================
# set up the .mc script
#--------------------------------------------------
# open script file
scriptname = "input.mc";
script = open(run_dir+scriptname,'w+');

# initial setup
print("config.read(configfile=%s,npatch=2)" % (run_dir+"reference.config.xml"),file=script);
print("mc.npt(kT=%f,P=%f)" % ( kT, P ),file=script);
# equilibrate with all potentials now
print("potential.pair_ckf(rcut=%f,Md=%f,Mr=%f,req=%f,omega=%f,alpha=%f)" % (rcut,Md,Mr,req,omega,alpha),file=script);
# moves are all rescale
print("move.rescale(freq=1,mag=0.5,max=10.0,min=1e-3,dims=%s)" % rescale_mode,file=script);
# set up some snapshots (for diagnostics)
print("mc.snapshot(prefix=relax.config,period=%d,overwrite=True)" % (equil/snapshots),file=script);
# log some quantities
print("mc.log(prefix=relax,period=%d,potential=True,lx=True,ly=True,lz=True)" % (equil/logs),file=script);
# equilibrate moves and box size simultaneously
print("mc.run(sweeps=%d,adjust=%d)" % (equil, adjust),file=script);
script.close()
#==================================================
# queue the run
#--------------------------------------------------
# sbatch the job script (must be bash)
this_proc_name = run_name + "-" + str(label+1)
cmd = "sbatch --job-name=%s run_mc.sh %s %s %s" % (this_proc_name,scriptname,run_name,run_dir)
print("submitting eos job with command: %s" % cmd)
status, output = commands.getstatusoutput(cmd)
if status != 0:
    print(output)
