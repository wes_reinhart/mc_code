# generate a configuration
config.generate(N=192,L=(7,7,7),npatch=2,grid=0.1,diameter=(1.0,0.001))

# set up trial move(s)
move.displace(freq=192,mag=0.1)
move.rotate(freq=192,mag=1.5,max=3.14)

# set up pair potential(s)
potential.pair_kf(alpha=0.7,delta=0.1)

# choose ensemble
mc.nvt(kT=0.20)

# set up logger
mc.log(prefix=equil,period=1e1,potential=True)

# equilibrate step size
mc.run(sweeps=1e3,reset=1e2,adjust=1e1)

# set up logger
mc.log(prefix=test,period=1e2,potential=True)

# set up snapshots
mc.snapshot(prefix=test,period=1e2,overwrite=False)
mc.snapshot(prefix=test.config,period=1e2,overwrite=True)

# production run
mc.run(sweeps=1e4,reset=1e2)
